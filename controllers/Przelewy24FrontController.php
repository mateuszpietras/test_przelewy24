<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24FrontController.
 */
class Przelewy24FrontController extends ModuleFrontController
{
    /**
     * Dies and echoes output value. Inherited (when possible) from PS1.6.
     *
     * @param string|null $value
     * @param string|null $controller
     * @param string|null $method
     */
    protected function ajaxDie($value = null, $controller = null, $method = null)
    {
        if (_PS_VERSION_ < 1.6) {
            die($value);
        }

        parent::ajaxDie($value, $controller, $method);
    }
}
