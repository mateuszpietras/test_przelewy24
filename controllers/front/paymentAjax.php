<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24PaymentAjaxModuleFrontController.
 *
 * Class is named using PrestaShop convention described here:
 * https://devdocs.prestashop.com/1.7/modules/concepts/controllers/front-controllers/
 * File name should begin with lower case. Class name should be equal to:
 * $moduleName . ucfirst($fileName) .'ModuleFrontController'
 * To see more details please access PrestaShop class
 * /class/Dispatcher.php
 * on line 295 (method "dispatch")
 */
class Przelewy24PaymentAjaxModuleFrontController extends Przelewy24FrontController
{
    /**
     * Parameter inherited from ModuleFrontController.
     * Should user be use ssl connection to access this view.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Parameter inherited from ModuleFrontController.
     * Indicates if user should be authenticated to access this view.
     *
     * @var bool
     */
    public $auth = false;

    /**
     * Executes CURL request.
     *
     * @param string $url
     *
     * @return string
     */
    public static function requestGet($url)
    {
        $isCurl = function_exists('curl_init')
            && function_exists('curl_setopt')
            && function_exists('curl_exec')
            && function_exists('curl_close')
        ;

        if ($isCurl) {
            $userAgent = 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)';
            $curlConnection = curl_init();
            curl_setopt($curlConnection, CURLOPT_URL, $url);
            curl_setopt($curlConnection, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curlConnection, CURLOPT_USERAGENT, $userAgent);
            curl_setopt($curlConnection, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlConnection, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curlConnection);
            curl_close($curlConnection);
            return $result;
        }
        return '';
    }

    /**
     * Dispatch actions.
     *
     * @throws Exception
     */
    public function initContent()
    {
        $actionName = Tools::getValue('action');
        switch ($actionName) {
            case 'setOrder':
                $this->setOrderAction();
                break;
            case 'runIVR':
                $this->runIVRAction();
                break;
            case 'cc_forget':
                $this->ccForgetAction();
                break;
            case 'calculatePart':
                $this->calculatePartAction();
                break;
            case 'cardRegisterQuery':
                $this->cardRegisterQueryAction();
                break;
            case 'makeOrder':
                $this->makeOrderAction();
                break;
            case 'trnRegister':
                $this->trnRegisterAction();
                break;
            case 'cardRegister':
                $this->cardRegisterAction();
                break;
            case 'forgetCart':
                $this->forgetCartAction();
                break;
            default:
                $this->ajaxDie();
        }
    }

    /**
     * Action for admin panel to get most used payment methods.
     */
    private function setOrderAction()
    {
        $przelewy24 = new Przelewy24();
        $availablePaymentMethods = $przelewy24->availablePaymentMethods();
        $allMethods = array();
        foreach ($availablePaymentMethods as $bank) {
            $allMethods[$bank->id] = $bank->name;
        }

        $order = array();
        $mostUsed = OrderDetailsModel::getMostUsedPaymentMethods();

        if ($mostUsed) {
            foreach ($mostUsed as $row) {
                if (isset($allMethods[$row['p24_method']])) {
                    $order[] = (int) $row['p24_method'];
                    unset($allMethods[$row['p24_method']]);
                }
            }
        }

        foreach (array_keys($allMethods) as $id) {
            $order[] = (int) $id;
        }

        $this->ajaxDie(Tools::jsonEncode($order));
    }

    /**
     * Action for additional payment via IVR.
     *
     * This method is dead for now.
     * It will be resurrected so the code is left in place.
     */
    private function runIVRAction()
    {
        $przelewy24 = new Przelewy24();
        $cart = Cart::getCartByOrderId((int) Tools::getValue('id_order'));

        $currency = $przelewy24->getCurrencyCode($cart->id_currency);
        $currencySuffix = ('PLN' === $currency || empty($currency)) ? '' : '_'.$currency;

        $order = new Order((int) Tools::getValue('id_order'));
        $customer = new Customer((int) ($cart->id_customer));
        $address = new Address((int) $order->id_address_delivery);

        $paymentData = new Przelewy24PaymentData($cart);
        /* This amount is without extracharge. */
        $amount = (int)$paymentData->formatAmount($cart->getOrderTotal(true, Cart::BOTH));

        $urlStatus = $this->context->link->getModuleLink(
            'przelewy24',
            'paymentStatus',
            array(),
            '1' === Configuration::get('PS_SSL_ENABLED')
        );

        // adding basic auth to url_status
        $basicAuth = Configuration::get(P24Configuration::P24_BASIC_AUTH.$currencySuffix);
        if (!empty($basicAuth)) {
            list($protocol, $url) = explode('://', $urlStatus, 2);
            $urlStatus = "{$protocol}://{$basicAuth}@{$url}";
        }

        $orderDetails = OrderDetailsModel::findByOrderId((int) $cart->id);
        if ($orderDetails) {
            $sSid = $orderDetails->s_sid;
            $orderDetails->i_amount = $amount;
            $orderDetails->save();
        } else {
            $sSid = md5(time());
            $orderDetails = new OrderDetailsModel();
            $orderDetails->s_sid = $sSid;
            $orderDetails->i_id_order = (int) $cart->id;
            $orderDetails->i_amount = (int) $amount;
            $orderDetails->add();
        }

        try {
            $p24Soap = new Przelewy24Soap($currencySuffix);
            if ($p24Soap->hasMethod('TransactionMotoCallBackRegister')) {
                $clientPhone = (!empty($address->phone_mobile) ? $address->phone_mobile : $address->phone);
                // removing country prefix
                if (0 === strpos($clientPhone, '+48')) {
                    $clientPhone = Tools::substr($clientPhone, 3);
                } elseif (0 === strpos($clientPhone, '0048')) {
                    $clientPhone = Tools::substr($clientPhone, 4);
                } elseif (0 === strpos($clientPhone, '48')) {
                    $clientPhone = Tools::substr($clientPhone, 2);
                }

                $res = $p24Soap->runIvr(
                    $currencySuffix,
                    $clientPhone,
                    $amount,
                    $currency,
                    $cart->id,
                    $sSid,
                    $customer->email,
                    $customer->firstname,
                    $customer->lastname,
                    $urlStatus
                );

                $msg = 'Uruchomiono IVR na nr '.$clientPhone;

                Przelewy24Logger::addLog(__METHOD__.' '.$msg, 1);
                if ($res->error->errorCode > 0) {
                    Przelewy24Logger::addLog(__METHOD__.' '.$res->error->errorMessage, 1);
                }
            }
        } catch (Exception $e) {
            Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);
        }
        $this->ajaxDie();
    }

    /**
     * Set config for user to save or not credit card credentials.
     */
    private function ccForgetAction()
    {
        $ccForget = (int) ('true' === Tools::getValue('value'));
        if (!$customerId = (int) Context::getContext()->customer->id) {
            $this->ajaxDie();
        }
        $customerSettings = new CustomerSettingsModel($customerId);
        $customerSettings->cc_forget = $ccForget;

        if ($customerSettings->customer_id) {
            $customerSettings->update();
        } else {
            $customerSettings->customer_id = $customerId;
            $customerSettings->add();
        }
        $this->ajaxDie();
    }

    /**
     * Return information about installments.
     *
     * It is used on product page and on payment page.
     */
    private function calculatePartAction()
    {
        $liveUrl = Przelewy24Class::getLiveHost();
        $result = self::requestGet($liveUrl.'kalkulator_raty.php?ammount='.(Tools::getValue('amount') * 100));
        list($installmentsCount, $installmentAmount) = explode('<br>', $result, 2);
        $data = array(
            'installmentsCount' => (int) $installmentsCount,
            'installmentAmount' => (int) $installmentAmount,
        );
        $this->ajaxDie(Tools::jsonEncode($data));
    }

    /**
     * Update card data in our database.
     *
     * The information are from Przelewy24 service.
     */
    private function cardRegisterQueryAction()
    {
        $receiptText = Tools::getValue('ReceiptText');
        $cardData = trim(Tools::substr($receiptText, 0, strpos($receiptText, '$')));
        $cardType = trim(Tools::substr($cardData, 0, strpos($cardData, 'xxxx')));
        $cardNumber = trim(chunk_split(trim(str_replace($cardType, '', $cardData)), 4, ' '));

        $recurringModel = new RecurringModel();
        $recurringModel->website_id = (int) Context::getContext()->shop->id;
        $recurringModel->customer_id = (int) Context::getContext()->customer->id;
        $recurringModel->reference_id = Tools::getValue('ccToken');
        $recurringModel->expires =  Tools::getValue('expires');
        $recurringModel->mask = $cardNumber;
        $recurringModel->card_type = $cardType;
        $recurringModel->timestamp = date('Y-m-d');
        $data = array('status' => $recurringModel->add());
        $this->ajaxDie(Tools::jsonEncode($data));
    }

    /**
     * Create order on request.
     *
     * The P24_VERIFYORDER should be 2 for this method to work.
     */
    private function makeOrderAction()
    {
        # TODO This code should be put into common place.
        /** @var CartCore $cart */
        $cart = Context::getContext()->cart;
        /** @var CustomerCore $customer */
        $customer = Context::getContext()->customer;

        $przelewy24 = new Przelewy24();

        if ($cart && '2' === Configuration::get(P24Configuration::P24_VERIFYORDER)) {
            $idCurrency = $cart->id_currency;
            $currency = $przelewy24->getCurrencyCode($idCurrency);
            $przelewy24->addDiscount($cart, $currency);
            $paymentData = new Przelewy24PaymentData($cart);

            if (!$paymentData->orderExists()) {
                /* This amount is without extracharge. */
                $amount = $cart->getOrderTotal(true, Cart::BOTH);
                $amount = $paymentData->formatAmount($amount);
                $orderBeginningState = Configuration::get(P24Configuration::P24_ORDER_STATE_1);
                $przelewy24->validateOrder(
                    $cart->id,
                    (int) $orderBeginningState,
                    (float) ($amount / 100),
                    'Przelewy24',
                    null,
                    array(),
                    null,
                    false,
                    $customer->secure_key
                );
            }

            if ('1' === Configuration::get(P24Configuration::P24_ORDER_TITLE_ID)) {
                $result = $paymentData->getOrderReference();
            } else {
                $result = $paymentData->getFirstOrderId();
            }

            $data = array('order_id' => $result, 'description' => $this->module->l('Order').': '.$result);
        } else {
            $data = array();
        }
        $this->ajaxDie(Tools::jsonEncode($data));
    }

    /**
     * Get variables for trnRegisterAction method.
     *
     * @return array
     */
    private function getPostDataForTrnRegister()
    {
        if (Tools::getValue('p24_session_id')) {
            // TODO partially doubled code with paymentConfirmation.php
            $orderId = (int)Tools::getValue('order_id');
            if ($orderId) {
                $cart = Cart::getCartByOrderId($orderId);

                if (!$cart || !$cart->id_customer
                    || (int) $cart->id_customer !== (int) Context::getContext()->customer->id
                ) {
                    $this->ajaxDie();
                }

                $paymentData = new Przelewy24PaymentData($cart);
                if ('1' === Configuration::get(P24Configuration::P24_ORDER_TITLE_ID)) {
                    $sDescr = $this->module->l('Order') . ' ' . $paymentData->getOrderReference();
                } else {
                    $sDescr = $this->module->l('Order') . ' ' . $paymentData->getFirstOrderId();
                }
            } else {
                $przelewy24 = new Przelewy24();
                $cart = $this->context->cart;
                $idCurrency = $cart->id_currency;
                $currency = $przelewy24->getCurrencyCode($idCurrency);
                $przelewy24->addDiscount($cart, $currency);
                $sDescr = $this->module->l('Cart').': '.$cart->id;
                $paymentData = new Przelewy24PaymentData($cart);
            }

            $amountFloat = $paymentData->getTotalAmountWithExtraCharge();
            $amount = $paymentData->formatAmount($amountFloat);
            $przelewy24 = new Przelewy24();
            $idCurrency = $cart->id_currency;
            $currency = $przelewy24->getCurrencyCode($idCurrency);
            $currencySufix = ('PLN' === $currency || empty($currency)) ? '' : '_'.$currency;

            $customer = new Customer((int)($cart->id_customer));
            // TODO doubled code with paymentConfirmation -> move to przelewy24.php
            $postData = $this->getPostData($customer, $cart, $currency, $amount, $sDescr);
        } else {
            throw new LogicException('This function should not be called if there is no session.');
        }

        return array($currencySufix, $postData);
    }

    /**
     * First steep to pay by credit card.
     *
     * Prepare transaction on remote server.
     *
     * @throws Exception
     */
    private function trnRegisterAction()
    {
        if (Tools::getValue('p24_session_id')) {
            list($currencySufix, $postData) = $this->getPostDataForTrnRegister();

            $P24C = new Przelewy24Class(
                Configuration::get(P24Configuration::P24_MERCHANT_ID.$currencySufix),
                Configuration::get(P24Configuration::P24_SHOP_ID.$currencySufix),
                Configuration::get(P24Configuration::P24_SALT.$currencySufix),
                Configuration::get(P24Configuration::P24_TEST_MODE.$currencySufix)
            );

            foreach ($postData as $k => $v) {
                $P24C->addValue($k, $v);
            }
            $token = $P24C->trnRegister();
            if (is_array($token) && array_key_exists('token', $token) && $token['token']) {
                $token = $token['token'];
                $data = array(
                    'p24jsURL' => $P24C->getHost().'inchtml/ajaxPayment/ajax.js?token='.$token,
                    'p24cssURL' => $P24C->getHost().'inchtml/ajaxPayment/ajax.css',
                );
                $this->ajaxDie(Tools::jsonEncode($data));
            }
        }
        $this->ajaxDie();
    }

    /**
     * The method is to save card, not to pay.
     *
     * Prepare transaction on remote server.
     *
     * @throws Exception
     */
    private function cardRegisterAction()
    {
        if (Tools::getValue('p24_session_id')) {
            if (!$this->context->cart || !$this->context->cart->id_customer) {
                $this->ajaxDie();
            }
            $cart = $this->context->cart;

            $sDescr = 'Rejestracja karty: '.Tools::getValue('p24_session_id');
            $przelewy24 = new Przelewy24();
            $currency = $przelewy24->getCurrencyCode($cart->id_currency);
            $currencySuffix = ('PLN' === $currency || empty($currency)) ? '' : '_'.$currency;

            $amount = 0;

            $customer = new Customer((int)($cart->id_customer));

            $P24C = new Przelewy24Class(
                Configuration::get(P24Configuration::P24_MERCHANT_ID.$currencySuffix),
                Configuration::get(P24Configuration::P24_SHOP_ID.$currencySuffix),
                Configuration::get(P24Configuration::P24_SALT.$currencySuffix),
                Configuration::get(P24Configuration::P24_TEST_MODE.$currencySuffix)
            );

            // TODO function to replace after refactoring
            $postData = $this->getPostData($customer, null, $cart, $currency, $amount, $sDescr);

            foreach ($postData as $k => $v) {
                $P24C->addValue($k, $v);
            }
            $token = $P24C->trnRegister();
            if (is_array($token)
                && array_key_exists('token', $token)
                && array_key_exists('sign', $token)
                && $token['token'] ) {
                $sign = $token['sign'];
                $token = $token['token'];
                $data = array(
                    'p24jsURL' => $P24C->getHost().'inchtml/card/register_card/ajax.js?token='.$token,
                    'p24cssURL' => $P24C->getHost().'inchtml/card/register_card/ajax.css',
                    'p24sign' => $sign,
                );
                $this->ajaxDie(Tools::jsonEncode($data));
            }
        }
        $this->ajaxDie();
    }

    /**
     * Action to forget current cart.
     *
     * The cart may be bind to an order.
     */
    private function forgetCartAction()
    {
        $this->context->cookie->id_cart = null;
        $this->ajaxDie();
    }

    /**
     * Get post data.
     *
     * @param Customer $customer
     * @param Cart     $cart
     * @param string   $currency
     * @param int      $amount
     * @param string   $description
     *
     * @return array
     */
    protected function getPostData($customer, $cart, $currency, $amount, $description)
    {
        $idAddressInvoice = (int)$cart->id_address_invoice;
        $address = new Address($idAddressInvoice);
        $sLang = new Country((int)($address->id_country));

        $currencySufix = ('PLN' === $currency || empty($currency)) ? '' : '_'.$currency;

        $urlStatus = $this->context->link->getModuleLink(
            'przelewy24',
            'paymentStatus',
            array(),
            '1' === Configuration::get('PS_SSL_ENABLED')
        );

        // adding basic auth to url_status
        $basicAuth = Configuration::get(P24Configuration::P24_BASIC_AUTH.$currencySufix);
        if (!empty($basicAuth)) {
            list($protocol, $url) = explode('://', $urlStatus, 2);
            $urlStatus = "{$protocol}://{$basicAuth}@{$url}";
        }

        $paymentData = new Przelewy24PaymentData($cart);
        $shipping = (int)(round($paymentData->getShippingCosts() * 100));
        $postData = array(
            'p24_merchant_id' => Configuration::get(P24Configuration::P24_MERCHANT_ID.$currencySufix),
            'p24_pos_id' => Configuration::get(P24Configuration::P24_SHOP_ID.$currencySufix),
            'p24_session_id' => Tools::getValue('p24_session_id'),
            'p24_amount' => (int)$amount,
            'p24_currency' => filter_var($currency, FILTER_SANITIZE_STRING),
            'p24_description' => filter_var($description, FILTER_SANITIZE_STRING),
            'p24_email' => filter_var($customer->email, FILTER_SANITIZE_EMAIL),
            'p24_client' => filter_var(
                $customer->firstname.' '.$customer->lastname,
                FILTER_SANITIZE_STRING
            ),
            'p24_address' => filter_var(
                $address->address1.' '.$address->address2,
                FILTER_SANITIZE_STRING
            ),
            'p24_zip' => filter_var($address->postcode, FILTER_SANITIZE_STRING),
            'p24_city' => filter_var($address->city, FILTER_SANITIZE_STRING),
            'p24_country' => $sLang->iso_code,
            'p24_language' => Tools::strtolower(Language::getIsoById($cart->id_lang)),
            'p24_url_return' => filter_var(
                $this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentReturn',
                    array(),
                    '1' === Configuration::get('PS_SSL_ENABLED')
                ),
                FILTER_SANITIZE_URL
            ),
            'p24_url_status' => filter_var($urlStatus, FILTER_SANITIZE_URL),
            'p24_api_version' => P24_VERSION,
            'p24_ecommerce' => 'prestashop_'._PS_VERSION_,
            'p24_ecommerce2' => Przelewy24::PLUGIN_VERSION,
            'p24_shipping' => $shipping,
        );

        $productsInfo = array_map(
            function ($product) {
                return array(
                    'name' => array_key_exists('name', $product) ? $product['name'] : '',
                    'description' => array_key_exists('description_short', $product) ?
                        $product['description_short'] : '',
                    'quantity' => (int)$product['quantity'],
                    'price' => (int)round($product['price'] * 100),
                    'number' => $product['id_product'],
                );
            },
            $paymentData->getProducts()
        );

        $translations = array(
            'virtual_product_name' => $this->module->l('Extra charge [VAT and discounts]'),
            'cart_as_product' => $this->module->l('Your order'),
        );
        $p24Product = new Przelewy24Product($translations);
        $p24ProductItems = $p24Product->prepareCartItems($amount, $productsInfo, $shipping);

        $postData = array_merge($postData, $p24ProductItems);
        return $postData;
    }
}
