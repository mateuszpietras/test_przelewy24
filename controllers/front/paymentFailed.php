<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24PaymentFailedModuleFrontController.
 *
 * Class is named using PrestaShop convention described here:
 * https://devdocs.prestashop.com/1.7/modules/concepts/controllers/front-controllers/
 * File name should begin with lower case. Class name should be equal to:
 * $moduleName . ucfirst($fileName) .'ModuleFrontController'
 * To see more details please access PrestaShop class
 * /class/Dispatcher.php
 * on line 295 (method "dispatch")
 */
class Przelewy24PaymentFailedModuleFrontController extends Przelewy24FrontController
{
    /**
     * Parameter inherited from ModuleFrontController.
     * Should left column be displayed in this view.
     *
     * @var bool
     */
    public $display_column_left = false;

    /**
     * Parameter inherited from ModuleFrontController.
     * Should right column be displayed in this view.
     *
     * @var bool
     */
    public $display_column_right = false;

    /**
     * Parameter inherited from ModuleFrontController.
     * Should user be use ssl connection to access this view.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Init content.
     */
    public function initContent()
    {
        $this->display_column_left = false;
        $this->display_column_right = false;
        parent::initContent();

        $przelewy24 = new Przelewy24();
        $currency = $przelewy24->getCurrencyCode($this->context->cart->id_currency);
        $currencySuffix = ('PLN' === $currency || empty($currency)) ? '' : '_'.$currency;

        $this->context->smarty->assign('p24_retryPaymentUrl', $this->context->link->getModuleLink(
            'przelewy24',
            'paymentConfirmation',
            array(),
            '1' === Configuration::get('PS_SSL_ENABLED')
        ));

        if (Configuration::get(P24Configuration::P24_VERIFYORDER.$currencySuffix) > 0) {
            // Order was executed, but there is no payment. Looks for last order of this customer
            $customerOrderList = Order::getCustomerOrders($this->context->customer->id);

            if (is_array($customerOrderList)) {
                foreach ($customerOrderList as $item) {
                    $customer = new Customer((int) ($item['id_customer']));
                    $this->context->smarty->assign(
                        'p24_retryPaymentUrl',
                        $this->context->link->getModuleLink(
                            'przelewy24',
                            'paymentConfirmation',
                            array(
                                'order_id' => $item['id_order'],
                                'token' => $customer->secure_key, ),
                            '1' === Configuration::get('PS_SSL_ENABLED')
                        )
                    );
                    break;
                }
            }
        }

        if (Tools::getValue('order_id')) {
            $orderId = (int) Tools::getValue('order_id');
            $order = new Order($orderId);
            $customer = new Customer((int) $order->id_customer);
            $this->context->smarty->assign(
                'p24_retryPaymentUrl',
                $this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentConfirmation',
                    array(
                        'order_id' => $orderId,
                        'token' => $customer->secure_key,
                    ),
                    '1' === Configuration::get('PS_SSL_ENABLED')
                )
            );
        }

        if ($sessionId = Tools::getValue('p24_session_id')) {
            $sessionDataArray = explode('|', $sessionId, 2);
            $saSid = 2 === count($sessionDataArray) ? $sessionDataArray[1] : null;
            $orderDetailsArray = OrderDetailsModel::findBySessionId($saSid);
            $orderId = (int) $orderDetailsArray[OrderDetailsModel::ORDER_ID];
            $order = new Order($orderId);
            $customer = new Customer((int) ($order->id_customer));

            $this->context->smarty->assign(
                'p24_retryPaymentUrl',
                $this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentConfirmation',
                    array(
                        'order_id' => $orderId,
                        'token' => $customer->secure_key,
                    ),
                    '1' === Configuration::get('PS_SSL_ENABLED')
                )
            );
        }
        $this->context->smarty->assign('url_history', $this->context->link->getPageLink('history', true));
        $this->setTemplate('paymentFailed.tpl');
    }
}
