<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24PaymentConfirmationModuleFrontController.
 *
 * Class is named using PrestaShop convention described here:
 * https://devdocs.prestashop.com/1.7/modules/concepts/controllers/front-controllers/
 * File name should begin with lower case. Class name should be equal to:
 * $moduleName . ucfirst($fileName) .'ModuleFrontController'
 * To see more details please access PrestaShop class
 * /class/Dispatcher.php
 * on line 295 (method "dispatch")
 */
class Przelewy24PaymentConfirmationModuleFrontController extends Przelewy24FrontController
{
    const NEW_ORDER_REDIRECT_URL = 'index.php?controller=order&step=1';

    /**
     * Parameter inherited from ModuleFrontController.
     * Should left column be displayed in this view.
     *
     * @var bool
     */
    public $display_column_left = false;

    /**
     * Parameter inherited from ModuleFrontController.
     * Should right column be displayed in this view.
     *
     * @var bool
     */
    public $display_column_right = false;

    /**
     * Parameter inherited from ModuleFrontController.
     * Should user be use ssl connection to access this view.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Przelewy24PaymentConfirmationModuleFrontController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->display_column_left = false;
        $this->display_column_right = false;
        RecurringModel::deleteExpiredCards();
    }

    /**
     * Get object with payment data.
     *
     * @return Przelewy24PaymentData
     */
    private function getPaymentData()
    {
        $orderId = (int)Tools::getValue('order_id', 0);
        if ($orderId) {
            $cart = Cart::getCartByOrderId($orderId);
        } else {
            $cart = $this->context->cart;
        }
        if (!$cart) {
            Tools::redirect(self::NEW_ORDER_REDIRECT_URL);

            return;
        }

        return new Przelewy24PaymentData($cart);
    }

    /**
     * Init content.
     */
    public function initContent()
    {
        parent::initContent();

        $this->display_column_left = false;
        $this->display_column_right = false;

        $paymentData = $this->getPaymentData();

        if (!$paymentData->isCartInDb()) {
            Tools::redirect(self::NEW_ORDER_REDIRECT_URL);

            return;
        }

        $cart = $paymentData->getCart();

        /* There may be few orders for one payment. Choose first. */
        $this->context->smarty->assign('get_order_id', $paymentData->getFirstOrderId());

        $token = (string)Tools::getValue('token', '');
        $customer = new Customer((int)($cart->id_customer));

        $customerId = (int)Context::getContext()->customer->id;
        if ($customerId && $customerId !== (int)$customer->id) {
            Tools::redirect(self::NEW_ORDER_REDIRECT_URL);

            return;
        }

        if (!$customerId && (!$token || $token !== $customer->secure_key)) {
            Tools::redirect(self::NEW_ORDER_REDIRECT_URL);

            return;
        }

        $przelewy24 = new Przelewy24();
        $currency = $przelewy24->getCurrencyCode((int)$cart->id_currency);
        $currencySuffix = ('PLN' === $currency || empty($currency)) ? '' : '_' . $currency;

        $address = new Address((int)$cart->id_address_invoice);
        $sLang = new Country((int)($address->id_country));

        if (!$paymentData->orderExists()) {
            $przelewy24->addDiscount($cart, $currency);
        }
        if (empty($currency)) {
            Tools::redirect(self::NEW_ORDER_REDIRECT_URL);

            return;
        }

        $amount = $paymentData->getTotalAmountWithExtraCharge();

        $classicAmount = Tools::displayPrice($amount);
        $amount = (int)$paymentData->formatAmount($amount);

        $this->context->smarty->assign('extracharge_amount', 0);

        if ($paymentData->orderExists()) {
            if ('1' === Configuration::get(P24Configuration::P24_ORDER_TITLE_ID)) {
                $sDescr = $this->module->l('Order') . ' ' . $paymentData->getOrderReference();
                $referenceToOrderId = $paymentData->getOrderReference();
            } else {
                /* There may be few orders for one payment. Choose first. */
                $sDescr = $this->module->l('Order') . ' ' . $paymentData->getFirstOrderId();
                $referenceToOrderId = $paymentData->getFirstOrderId();
            }

            $extracharge = $paymentData->getExtrachargeFromDatabase();
            $extrachargeAmount = $extracharge ? $extracharge->extracharge_amount * 100 : 0;
        } else {
            $sDescr = $this->module->l('Cart') . ': ' . $cart->id;
            $referenceToOrderId = '';

            $extrachargeAmount = $przelewy24->getExtracharge($amount, $currencySuffix);
        }

        if ($extrachargeAmount > 0) {
            $this->context->smarty->assign('extracharge_enabled', true);
            $this->context->smarty->assign(
                'extracharge_amount',
                Tools::displayPrice(round($extrachargeAmount / 100, 2))
            );
            $classicAmount = Tools::displayPrice(round($amount / 100, 2));
        } else {
            $this->context->smarty->assign('extracharge_enabled', false);
        }

        // extra discount
        $totalDiscounts = $paymentData->getDiscounts();
        if ($totalDiscounts > 0) {
            $this->context->smarty->assign('extradiscount_amount', Tools::displayPrice($totalDiscounts));
        }

        /* The name of method is due to historical reasons. */
        $orderDetails = OrderDetailsModel::findByOrderId((int)$cart->id);
        if ($orderDetails) {
            $sSid = $orderDetails->s_sid;
            $orderDetails->i_amount = $amount;
            $orderDetails->save();
        } else {
            $sSid = md5(time());
            $orderDetails = new OrderDetailsModel();
            $orderDetails->s_sid = $sSid;
            /* The name of variable is due to historical reasons. */
            $orderDetails->i_id_order = (int)$cart->id;
            $orderDetails->i_amount = $amount;
            $orderDetails->add();
        }

        $paymentMethod = (int)Tools::getValue('payment_method');
        $freeOrder = !$paymentData->orderExists() && (float)$amount === (float)0;

        // Creates order (if it has to be created)
        if (!$paymentData->orderExists()
            && '1' === Configuration::get(P24Configuration::P24_VERIFYORDER . $currencySuffix)
            || $freeOrder
        ) {
            $orderBeginingState = Configuration::get(P24Configuration::P24_ORDER_STATE_1);

            if ($freeOrder) {
                $orderBeginingState = Configuration::get(P24Configuration::P24_ORDER_STATE_2);
            }

            $przelewy24->validateOrder(
                $cart->id,
                (int)$orderBeginingState,
                (float)($amount / 100),
                'Przelewy24',
                null,
                array(),
                null,
                false,
                $customer->secure_key
            );

            $args = array();
            /* There may be few orders for one payment. Choose first. */
            $args['order_id'] = $paymentData->getFirstOrderId();
            $args['token'] = $customer->secure_key;
            if (!empty($paymentMethod)) {
                $args['payment_method'] = $paymentMethod;
            }

            Tools::redirect(
                $this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentConfirmation',
                    $args,
                    '1' === Configuration::get('PS_SSL_ENABLED')
                )
            );
        }

        if (!$amount) {
            $this->context->smarty->assign('p24FreeOrder', true);
            $this->context->smarty->assign('url_history', $this->context->link->getPageLink('history', true));
        }

        $P24C = new Przelewy24Class(
            Configuration::get(P24Configuration::P24_MERCHANT_ID . $currencySuffix),
            Configuration::get(P24Configuration::P24_SHOP_ID . $currencySuffix),
            Configuration::get(P24Configuration::P24_SALT . $currencySuffix),
            Configuration::get(P24Configuration::P24_TEST_MODE . $currencySuffix)
        );

        $urlStatus = $this->context->link->getModuleLink(
            'przelewy24',
            'paymentStatus',
            array(),
            '1' === Configuration::get('PS_SSL_ENABLED')
        );

        // adding basic auth to url_status
        $basic_auth = Configuration::get(P24Configuration::P24_BASIC_AUTH . $currencySuffix);
        if (!empty($basic_auth)) {
            list($protocol, $url) = explode('://', $urlStatus, 2);
            $urlStatus = "{$protocol}://{$basic_auth}@{$url}";
        }

        // TODO doubled code with paymentAjax.php -> move to przelewy24.php
        $shipping = (int)round($paymentData->getShippingCosts() * 100, 2);
        $postData = array(
            'productsNumber' => $cart->nbProducts(),
            'p24_classic_amount' => $classicAmount,
            'p24_id_zamowienia' => $referenceToOrderId,
            'p24_show_summary' => Configuration::get(P24Configuration::P24_SHOW_SUMMARY . $currencySuffix),
            'p24_url' => filter_var($P24C->trnDirectUrl(), FILTER_SANITIZE_URL),

            'p24_session_id' => $cart->id . '|' . $sSid,
            'p24_merchant_id' => Configuration::get(P24Configuration::P24_MERCHANT_ID . $currencySuffix),
            'p24_pos_id' => Configuration::get(P24Configuration::P24_SHOP_ID . $currencySuffix),
            'p24_email' => filter_var($customer->email, FILTER_SANITIZE_EMAIL),
            'p24_amount' => $amount,
            'p24_currency' => filter_var($currency, FILTER_SANITIZE_STRING),
            'p24_description' => filter_var($sDescr, FILTER_SANITIZE_STRING),
            'p24_language' => Tools::strtolower(Language::getIsoById($cart->id_lang)),
            'p24_client' => filter_var(
                $customer->firstname . ' ' . $customer->lastname,
                FILTER_SANITIZE_STRING
            ),
            'p24_address' => filter_var(
                $address->address1 . ' ' . $address->address2,
                FILTER_SANITIZE_STRING
            ),
            'p24_city' => filter_var($address->city, FILTER_SANITIZE_STRING),
            'p24_zip' => filter_var($address->postcode, FILTER_SANITIZE_STRING),
            'p24_country' => $sLang->iso_code,

            'p24_encoding' => 'UTF-8',
            'p24_method' => $paymentMethod,
            'p24_ajax_url' => filter_var(
                $this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentAjax',
                    array(),
                    '1' === Configuration::get('PS_SSL_ENABLED')
                ),
                FILTER_SANITIZE_URL
            ),

            'p24_url_status' => filter_var($urlStatus, FILTER_SANITIZE_URL),
            'p24_url_return' => filter_var(
                $this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentReturn',
                    array('cart_id' => $cart->id),
                    '1' === Configuration::get('PS_SSL_ENABLED')
                ),
                FILTER_SANITIZE_URL
            ),
            'p24_api_version' => P24_VERSION,
            'p24_ecommerce' => 'prestashop_' . _PS_VERSION_,
            'p24_ecommerce2' => Przelewy24::PLUGIN_VERSION,

            'p24_validationRequired' => Configuration::get(P24Configuration::P24_VERIFYORDER . $currencySuffix),
            'p24_wait_for_result' => Configuration::get(P24Configuration::P24_WAIT_FOR_RESULT),
            'p24_shipping' => $shipping,
        );

        $productsInfo = array_map(
            function ($product) {
                if (isset(
                    $product['name'],
                    $product['description_short'],
                    $product['quantity'],
                    $product['price'],
                    $product['id_product']
                )) {
                    return array(
                        'name' => $product['name'],
                        'description' => $product['description_short'],
                        'quantity' => (int)$product['quantity'],
                        'price' => (int)round($product['price'] * 100),
                        'number' => $product['id_product'],
                    );
                }
            },
            $paymentData->getProducts()
        );

        $translations = array(
            'virtual_product_name' => $this->module->l('Extra charge [VAT and discounts]'),
            'cart_as_product' => $this->module->l('Your order'),
        );
        $p24Product = new Przelewy24Product($translations);
        $p24ProductItems = $p24Product->prepareCartItems($amount, $productsInfo, $shipping);
        // TODO end

        $postData['p24_sign'] = $P24C->trnDirectSign($postData);
        $P24C->checkMandatoryFieldsForAction($postData, 'trnDirect');
        $postData['oneclick'] = false;
        $postData['p24_cclist'] = array();
        $postData['p24_cards'] = array();
        if (Configuration::get(P24Configuration::P24_ONECLICK_ENABLED . $currencySuffix) &&
            $this->context->customer->isLogged()) {
            $postData['oneclick'] = true;
            $postData['p24_recuring_url'] = $this->context->link->getModuleLink(
                'przelewy24',
                'paymentRecurring',
                array(),
                '1' === Configuration::get('PS_SSL_ENABLED')
            );
            $rememberCreditCards = new CustomerSettingsModel($cart->id_customer);
            $result = !$rememberCreditCards->shouldCardBeRemembered() ?
                array() : RecurringModel::getCustomerCards($customer->id);
            foreach ($result as $row) {
                $postData['p24_cards'][$row[RecurringModel::ID]] =
                    sprintf(
                        html_entity_decode(
                            $this->module->l('Use %s card with number %s', 'przelewy24')
                        ),
                        $row[RecurringModel::CARD_TYPE],
                        $row[RecurringModel::MASK]
                    );
                $postData['p24_cclist'][$row[RecurringModel::ID]] =
                    array('type' => $row[RecurringModel::CARD_TYPE], 'mask' => $row[RecurringModel::MASK]);
            }
        }

        $this->context->smarty->assign($postData);

        $this->context->smarty->assign('p24ProductItems', $p24ProductItems);
        $this->context->smarty->assign('accept_in_shop', false);
        $this->context->smarty->assign('accept_in_shop_accepted', false);

        $customerSettings = new CustomerSettingsModel($customer->id);

        // Is acceptation in shop enabled (checkbox data)
        if (Configuration::get(P24Configuration::P24_ACCEPTINSHOP_ENABLED . $currencySuffix)) {
            $this->context->smarty->assign('accept_in_shop', true);
            if ($this->context->customer->isLogged() && $customerSettings->accepted_terms) {
                $this->context->smarty->assign('accept_in_shop', false);
                $this->context->smarty->assign('accept_in_shop_accepted', true);
            }
        }

        $customTimelimit = Configuration::get(P24Configuration::P24_CUSTOM_TIMELIMIT . $currencySuffix);
        if (is_numeric($customTimelimit) && $customTimelimit >= 0 && $customTimelimit <= 99) {
            $this->context->smarty->assign('custom_timelimit', $customTimelimit);
        }

        $this->context->smarty->assign(
            'back_url',
            $this->context->link->getPageLink(Configuration::get('PS_ORDER_PROCESS_TYPE') ? 'order-opc' : 'order', true)
        );

        $this->context->smarty->assign(
            'payslow_enabled',
            '1' === Configuration::get(P24Configuration::P24_PAYSLOW_ENABLED . $currencySuffix)
        );
        $this->context->smarty->assign(
            'pay_in_shop',
            '1' === Configuration::get(P24Configuration::P24_PAYINSHOP_ENABLED . $currencySuffix)
        );
        $this->context->smarty->assign(
            'default_remember_card',
            '1' === Configuration::get(P24Configuration::P24_DEFAULT_REMEMBER_CARD . $currencySuffix)
        );
        $this->context->smarty->assign(
            'accept_in_shop_translation',
            $przelewy24->l('Yes, I have read and accept Przelewy24.pl terms.')
        );

        $this->context->smarty->assign('ccards_forget', !$customerSettings->shouldCardBeRemembered());
        $this->context->smarty->assign(
            'myStoredCardsUrl',
            $this->context->link->getModuleLink(
                'przelewy24',
                'myStoredCards',
                array(),
                '1' === Configuration::get('PS_SSL_ENABLED')
            )
        );
        if ($cart) {
            $this->context->smarty->assign('cartId', $cart->id);
        }

        $allMethods = array();
        $p24PaymethodFirst = array();
        $p24PaymethodSecond = array();
        // Other payment methods
        $p24PaymethodList = Configuration::get(P24Configuration::P24_PAYMETHOD_LIST . $currencySuffix);
        if ($p24PaymethodList) {
            $resAllMethods = $przelewy24->availablePaymentMethods(
                '1' !== Configuration::get(P24Configuration::P24_PAYSLOW_ENABLED . $currencySuffix),
                $currency
            );
            if (is_array($resAllMethods)) {
                $this->addCSS($przelewy24->p24_css);
                $allMethods = array();
                $thereIs218 = false;
                foreach ($resAllMethods as $bank) {
                    $allMethods[$bank->id] = $bank->name;
                    if (218 === (int)$bank->id) {
                        $thereIs218 = true;
                    }
                }
                if ($thereIs218) {
                    unset($allMethods[142], $allMethods[145]);
                }

                $p24PaymethodFirst = explode(
                    ',',
                    Configuration::get(P24Configuration::P24_PAYMETHOD_FIRST . $currencySuffix)
                );
                $p24PaymethodSecond = explode(
                    ',',
                    Configuration::get(P24Configuration::P24_PAYMETHOD_SECOND . $currencySuffix)
                );

                foreach ($p24PaymethodFirst as $key => $item) {
                    if (!isset($allMethods[$item])) {
                        unset($p24PaymethodFirst[$key]);
                    }
                }

                foreach ($p24PaymethodSecond as $key => $item) {
                    if (!isset($allMethods[$item])) {
                        unset($p24PaymethodSecond[$key]);
                    }
                }

                if (0 === sizeof($p24PaymethodFirst)) {
                    $p24PaymethodFirst = $p24PaymethodSecond;
                    $p24PaymethodSecond = array();
                }
            }
        }

        // Payment method used last time.
        $lastMethodModel = new LastMethodModel($customer->id);

        if ($lastMethodModel->p24_method) {
            if (array_search($lastMethodModel->p24_method, array_merge($p24PaymethodFirst, $p24PaymethodSecond))) {
                $this->context->smarty->assign('last_pay_method', (int)$lastMethodModel->p24_method);
            } else {
                $this->context->smarty->assign('last_pay_method', false);
            }
        } else {
            $this->context->smarty->assign('last_pay_method', false);
        }
        $amountFloat = $amount / 100;
        $data = array('part_count' => 0, 'part_cost' => 0, 'product_ammount' => 0, 'liveUrl' => '');
        $showInstallment = (int)Configuration::get(P24Configuration::P24_INSTALLMENT_SHOW);
        $minIstallmentValue = Przelewy24::getMinInstallmentValue();
        if ($showInstallment && $amountFloat >= $minIstallmentValue && 'PLN' === $currency) {
            $liveUrl = Przelewy24Class::getLiveHost();
            $result = Przelewy24::requestGet($liveUrl . 'kalkulator_raty.php?ammount=' . $amount);
            $resultTab = explode('<br>', $result);

            $data['liveUrl'] = $liveUrl;
            $data['part_count'] = $resultTab[0];
            $data['part_cost'] = $resultTab[1];
            $data['product_ammount'] = $amountFloat;
            $data['show_installment'] = $showInstallment;
        }

        $this->context->smarty->assign($data);
        $this->context->smarty->assign(
            array(
                'showInstallment' => $showInstallment,
                'minIstallmentValue' => $minIstallmentValue,
                'p24_paymethod_all' => $allMethods,
                'p24_paymethod_list' => $p24PaymethodList,
                'p24_paymethod_graphics' => Configuration::get(
                    P24Configuration::P24_PAYMETHOD_GRAPHICS . $currencySuffix
                ),
                'p24_paymethod_first' => $p24PaymethodFirst,
                'p24_paymethod_second' => $p24PaymethodSecond,
            )
        );

        $this->context->smarty->assign('p24_css', $przelewy24->p24_css);
        $protocol = Przelewy24::getHttpProtocol();
        $this->context->smarty->assign(
            'base_url',
            $protocol . htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8') . __PS_BASE_URI__
        );
        $this->setTemplate('paymentConfirmation.tpl');
    }
}
