<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24PaymentSuccessfulModuleFrontController.
 *
 * Class is named using PrestaShop convention described here:
 * https://devdocs.prestashop.com/1.7/modules/concepts/controllers/front-controllers/
 * File name should begin with lower case. Class name should be equal to:
 * $moduleName . ucfirst($fileName) .'ModuleFrontController'
 * To see more details please access PrestaShop class
 * /class/Dispatcher.php
 * on line 295 (method "dispatch")
 */
class Przelewy24PaymentSuccessfulModuleFrontController extends Przelewy24FrontController
{
    /**
     * Parameter inherited from ModuleFrontController.
     * Should left column be displayed in this view.
     *
     * @var bool
     */
    public $display_column_left = false;

    /**
     * Parameter inherited from ModuleFrontController.
     * Should right column be displayed in this view.
     *
     * @var bool
     */
    public $display_column_right = false;

    /**
     * Parameter inherited from ModuleFrontController.
     * Should user be use ssl connection to access this view.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Init content.
     */
    public function initContent()
    {
        $this->display_column_left = false;
        $this->display_column_right = false;
        parent::initContent();

        $this->context->smarty->assign(array(
            'HOOK_ORDER_CONFIRMATION' => '',
            'HOOK_PAYMENT_RETURN' => '',
        ));

        $cartId = (int) Tools::getValue('cart_id');
        if ($cartId > 0) {
            try {
                $cart = new Cart($cartId);
                $orderId = Order::getOrderByCartId((int) $cart->id);
                $this->context->smarty->assign(array(
                    'HOOK_ORDER_CONFIRMATION' => $this->displayOrderConfirmation($orderId),
                    'HOOK_PAYMENT_RETURN' => $this->displayPaymentReturn($orderId),
                ));
            } catch (Exception $e) {
                Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);
            }
        }
        $this->setTemplate('paymentSuccessful.tpl');
    }

    /**
     * Execute the hook displayPaymentReturn.
     *
     * @param int $orderId
     *
     * @return Smarty_Internal_Data|bool
     */
    public function displayPaymentReturn($orderId)
    {
        $moduleId = (int) (Tools::getValue('id_module', 0));
        if (Validate::isUnsignedId($orderId) && Validate::isUnsignedId($moduleId)) {
            $params = array();
            $order = new Order($orderId);
            $currency = new Currency($order->id_currency);
            if (Validate::isLoadedObject($order)) {
                $params['total_to_pay'] = $order->getOrdersTotalPaid();
                $params['currency'] = $currency->sign;
                $params['objOrder'] = $order;
                $params['currencyObj'] = $currency;

                return Hook::exec('displayPaymentReturn', $params, $moduleId);
            }
        }
        return false;
    }

    /**
     * Execute the hook displayOrderConfirmation.
     *
     * @param int $orderId
     *
     * @return Smarty_Internal_Data|bool
     */
    public function displayOrderConfirmation($orderId)
    {
        if (Validate::isUnsignedId($orderId)) {
            $params = array();
            $order = new Order($orderId);
            $currency = new Currency($order->id_currency);

            if (Validate::isLoadedObject($order)) {
                $params['total_to_pay'] = $order->getOrdersTotalPaid();
                $params['currency'] = $currency->sign;
                $params['objOrder'] = $order;
                $params['currencyObj'] = $currency;

                return Hook::exec('displayOrderConfirmation', $params);
            }
        }
        return false;
    }
}
