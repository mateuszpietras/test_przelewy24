<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24PaymentRecurringModuleFrontController.
 *
 * Class is named using PrestaShop convention described here:
 * https://devdocs.prestashop.com/1.7/modules/concepts/controllers/front-controllers/
 * File name should begin with lower case. Class name should be equal to:
 * $moduleName . ucfirst($fileName) .'ModuleFrontController'
 * To see more details please access PrestaShop class
 * /class/Dispatcher.php
 * on line 295 (method "dispatch")
 */
class Przelewy24PaymentRecurringModuleFrontController extends Przelewy24FrontController
{
    /**
     * Parameter inherited from ModuleFrontController.
     * Indicates if user should be authenticated to access this view.
     *
     * @var bool
     */
    public $auth = true;

    /**
     * Parameter inherited from ModuleFrontController.
     * Should user be use ssl connection to access this view.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Customer id.
     *
     * @var int
     */
    private $uid;

    /**
     * Init content.
     */
    public function initContent()
    {
        if (Context::getContext()->customer->isLogged()) {
            $this->uid = (int)Context::getContext()->customer->id;
            $cardRm = Tools::getValue('cardrm');
            if ($cardRm && is_numeric($cardRm) && $cardRm > 0) {
                $this->removeCard((int) $cardRm);
            } else {
                $this->chargeCard();
            }
        }
        $this->ajaxDie();
    }

    /**
     * Remove card.
     *
     * @param int $cc
     *
     * @throws PrestaShopException
     */
    private function removeCard($cc)
    {
        $cc = (int)$cc;
        if ($cc > 0) {
            $recurring = new RecurringModel($cc);
            if (1 === (int)$recurring->website_id && (int)$recurring->customer_id === $this->uid) {
                $recurring->delete();
            }
            Tools::redirect($_SERVER['HTTP_REFERER']);
        }
    }

    /**
     * Find id of error order state.
     *
     * @return int|null
     */
    private function findErrorOrderState()
    {
        foreach (OrderState::getOrderStates(1) as $state) {
            if ('payment_error' === $state['template']) {
                return $state['id_order_state'];
            }
        }

        return null;
    }

    /**
     * Charge card.
     *
     * @throws PrestaShopException
     */
    private function chargeCard()
    {
        if (($sessionId = Tools::getValue('p24_session_id')) && Tools::getValue('p24_cc')) {
            $sessionExploded = explode('|', $sessionId, 2);
            $saSid = 2 === count($sessionExploded) ? $sessionExploded[1] : null;
            $p24Amount = OrderDetailsModel::findBySessionId($saSid);

            if (!$p24Amount || !is_array($p24Amount)) {
                Przelewy24Logger::addLog(__METHOD__.' Can\'t find payment in session: '.$saSid, 1);
                Tools::redirect($this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentFailed',
                    array(),
                    '1' === Configuration::get('PS_SSL_ENABLED')
                ));
                return;
            }

            /* The id is for cart. The name of key is misleading. */
            $cart = new Cart((int)$p24Amount['i_id_order']);
            $przelewy24 = new Przelewy24();
            $currencies = $przelewy24->getCurrency((int)$cart->id_currency);
            $currency = '';
            foreach ($currencies as $c) {
                if ($c['id_currency'] === $cart->id_currency) {
                    $currency = $c['iso_code'];
                }
            }

            $currencySuffix = ('PLN' === $currency || empty($currency)) ? '' : '_'.$currency;

            $cc = (int)Tools::getValue('p24_cc');
            $rr = new RecurringModel($cc);
            $ref = (int)$rr->customer_id === $this->uid ? $rr->reference_id : null;
            try {
                $przelewy24SoapCardService = new Przelewy24SoapCardService();
                $res = $przelewy24SoapCardService->chargeCard($currencySuffix, $ref, $p24Amount['i_amount'], $currency);
            } catch (Exception $e) {
                error_log(__METHOD__.' '.$e->getMessage());
            }
            $paymentData = new Przelewy24PaymentData($cart);
            if (!$paymentData->orderExists()) {
                $customer = new Customer((int)($cart->id_customer));
                $przelewy24->validateOrder(
                    $cart->id,
                    Configuration::get(P24Configuration::P24_ORDER_STATE_1),
                    (float) ($p24Amount['i_amount'] / 100),
                    'Przelewy24',
                    null,
                    array(),
                    null,
                    false,
                    $customer->secure_key
                );
            } else {
                error_log(__METHOD__.' HAD BASKET ');
            }

            $errorCode = isset($res) ? (int)$res->error->errorCode : -1;
            if (0 === $errorCode) {
                $orderState = Configuration::get(P24Configuration::P24_ORDER_STATE_2);
            } else {
                $orderState = $this->findErrorOrderState();
            }

            $orders = $paymentData->getAllOrders();
            foreach ($orders as $orderToUpdate) {
                $history = new OrderHistory();
                $history->id_order = (int)$orderToUpdate->id;
                $history->changeIdOrderState($orderState, (int)$orderToUpdate->id);
                $history->addWithemail(true);
            }

            try {
                $payments = $paymentData->getPayments();
                if (count($payments) > 0) {
                    $payments[0]->transaction_id = Tools::getValue('p24_order_id');
                    $payments[0]->update();
                }
            } catch (Exception $e) {
                Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);
            }
            if (0 === $errorCode) {
                $customerId = (int)$cart->id_customer;
                $lastMethod = new LastMethodModel($customerId);
                $lastMethod->delete();

                foreach ($orders as $orderToConfirm) {
                    Hook::exec('actionPaymentConfirmation', array('id_order' => $orderToConfirm->id));
                }

                Tools::redirect($this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentSuccessful',
                    array(),
                    '1' === Configuration::get('PS_SSL_ENABLED')
                ));
            } else {
                Tools::redirect($this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentFailed',
                    array(),
                    '1' === Configuration::get('PS_SSL_ENABLED')
                ));
            }
        }
    }
}
