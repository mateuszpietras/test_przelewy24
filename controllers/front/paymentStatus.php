<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24PaymentStatusModuleFrontController.
 *
 * Class is named using PrestaShop convention described here:
 * https://devdocs.prestashop.com/1.7/modules/concepts/controllers/front-controllers/
 * File name should begin with lower case. Class name should be equal to:
 * $moduleName . ucfirst($fileName) .'ModuleFrontController'
 * To see more details please access PrestaShop class
 * /class/Dispatcher.php
 * on line 295 (method "dispatch")
 */
class Przelewy24PaymentStatusModuleFrontController extends Przelewy24FrontController
{
    /**
     * Parameter inherited from ModuleFrontController.
     * Should user be use ssl connection to access this view.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Amount of extra charge.
     *
     * @var float|int
     */
    private $extrachargeAmount = 0;

    /**
     * Init content.
     */
    public function initContent()
    {
        if (Tools::getValue('p24_session_id')) {
            list($cartID, $saSid) = explode('|', Tools::getValue('p24_session_id'), 2);
            $cartID = (int)$cartID;
            $orderDetailsModel = OrderDetailsModel::findBySessionId($saSid);
            if ($cartID < 1 || (int)$orderDetailsModel[OrderDetailsModel::ORDER_ID] !== $cartID) {
                $this->ajaxDie('Invalid request');
            }
            $przelewy24 = new Przelewy24();
            $cart = new Cart($cartID);
            $paymentData = new Przelewy24PaymentData($cart);

            if (!$paymentData->orderExists()) {
                $idCurrency = $cart->id_currency;
                $currency = $przelewy24->getCurrencyCode($idCurrency);
                $przelewy24->addDiscount($cart, $currency);
                $extrachargeAmount = $paymentData->computeExtrachargeAmount();
                $cartAmount = (($cart->getOrderTotal() * 100 + $extrachargeAmount) / 100);
                $customer = new Customer((int)($cart->id_customer));
                $przelewy24->validateOrder(
                    $cartID,
                    (int)Configuration::get(P24Configuration::P24_ORDER_STATE_1), // order beginning state
                    $cartAmount,
                    'Przelewy24',
                    null,
                    array(),
                    null,
                    false,
                    $customer->secure_key
                );
            }

            $orders = $paymentData->getAllOrders();

            $amountFloat = $paymentData->getTotalAmountWithExtraCharge();
            $amount = $paymentData->formatAmount($amountFloat);
            $idCurrency = (int)$cart->id_currency;
            Context::getContext()->currency = Currency::getCurrencyInstance($idCurrency);
            $currency = $przelewy24->getCurrencyCode($idCurrency);
            $currencySuffix = ('PLN' === $currency || empty($currency)) ? '' : '_' . $currency;
            $P24C = new Przelewy24Class(
                Configuration::get(P24Configuration::P24_MERCHANT_ID . $currencySuffix),
                Configuration::get(P24Configuration::P24_SHOP_ID . $currencySuffix),
                Configuration::get(P24Configuration::P24_SALT . $currencySuffix),
                Configuration::get(P24Configuration::P24_TEST_MODE . $currencySuffix)
            );

            $extracharge = $paymentData->getExtrachargeFromDatabase();
            $this->extrachargeAmount = $extracharge ? $extracharge->extracharge_amount * 100 : 0;

            if ('trnVerify' === Tools::getValue('action') && Tools::getValue('p24_order_id')) {
                $_POST['p24_merchant_id'] = Configuration::get(P24Configuration::P24_MERCHANT_ID . $currencySuffix);
                $_POST['p24_pos_id'] = Configuration::get(P24Configuration::P24_SHOP_ID . $currencySuffix);
                $_POST['p24_amount'] = $amount;
                $_POST['p24_currency'] = $currency;
                $_POST['p24_method'] = 142;
                $_POST['p24_sign'] = md5(
                    Tools::getValue('p24_session_id') . '|' . Tools::getValue('p24_order_id') . '|' .
                    $amount . '|' . $currency . '|' . Configuration::get(P24Configuration::P24_SALT . $currencySuffix)
                );
            }
            $validation = array('p24_amount' => $amount, 'p24_currency' => $currency);
            $result = $P24C->trnVerifyEx($validation);

            if (null === $result) {
                $this->ajaxDie("\n" . 'INVALID POST DATA');
            } elseif (true === $result) {
                if ($paymentData->orderExists()) {
                    // save accept terms
                    if (Configuration::get(P24Configuration::P24_ACCEPTINSHOP_ENABLED . $currencySuffix)) {
                        $customerSettings = new CustomerSettingsModel($cart->id_customer);
                        if ($customerSettings->customer_id) {
                            $customerSettings->update();
                        } else {
                            $customerSettings->customer_id = $cart->id_customer;
                            $customerSettings->add();
                        }
                    }

                    // checks if order wasn't already confirmed
                    foreach ($orders as $orderToCheck) {
                        if ($orderToCheck->current_state === Configuration::get(P24Configuration::P24_ORDER_STATE_2)) {
                            $this->ajaxDie("\n" . 'OK');
                        }
                    }

                    $this->setOrdersState(
                        $orders,
                        Configuration::get(P24Configuration::P24_ORDER_STATE_2)
                    );
                    $methodId = (int)Tools::getValue('p24_method');
                    $amountObj = OrderDetailsModel::constructBySid($saSid);
                    $amountObj->p24_method = $methodId;
                    $amountObj->save();
                    if ($cart->id_customer) {
                        $lastMethodModel = new LastMethodModel($cart->id_customer);
                        if (in_array($methodId, array(140, 142, 145, 218))) {
                            $this->addCard(
                                $cart->id_customer,
                                (int)Tools::getValue('p24_order_id'),
                                Tools::getValue('p24_currency')
                            );
                            $lastMethodModel->delete();
                        } elseif ($methodId > 0) {
                            $lastMethodModel->p24_method = $methodId;
                            $lastMethodModel->save();
                        }
                    }
                }
                foreach ($orders as $orderToNotify) {
                    Hook::exec('actionPaymentConfirmation', array('id_order' => $orderToNotify->id));
                }
                $this->ajaxDie("\n" . 'OK');
            } else {
                $this->setOrdersState($orders, Configuration::get('PS_OS_ERROR'));
                $this->ajaxDie("\n" . 'ERROR');
            }
        } else {
            $this->ajaxDie('INVALID_SESSION_ID');
        }
    }

    /**
     * Add card.
     *
     * @param int $uid
     * @param int $oid
     * @param string $currency
     */
    private function addCard($uid, $oid, $currency)
    {
        $currencySuffix = ('PLN' === $currency || empty($currency)) ? '' : '_' . $currency;
        $uid = (int)$uid;
        if ($uid > 0 && Configuration::get(P24Configuration::P24_ONECLICK_ENABLED . $currencySuffix)
            && 32 === Tools::strlen(Configuration::get(P24Configuration::P24_API_KEY . $currencySuffix))) {
            $customerSettings = new CustomerSettingsModel($uid);
            /* Don't save a guest card */
            $customer = new Customer($uid);
            if (1 === (int)$customerSettings->cc_forget || 1 === $customer->is_guest) {
                return;
            }
            try {
                $przelewy24SoapCardService = new Przelewy24SoapCardService();
                $res = $przelewy24SoapCardService->getTransactionReference($currencySuffix, $oid);
                if (0 === (int)$res->error->errorCode) {
                    if (!empty($res->result->refId)) {
                        $hasRecurency = $przelewy24SoapCardService->checkCard($currencySuffix, $res->result->refId);
                        if (0 === (int)$hasRecurency->error->errorCode && $hasRecurency->result) {
                            $expRaw = $res->result->cardExp;
                            $expForCard = Tools::substr($expRaw, 2, 2) . Tools::substr($expRaw, 0, 2);
                            if (date('ym') <= $expForCard) {
                                RecurringModel::constructByUnique(
                                    $res->result->mask,
                                    $res->result->cardType,
                                    $expForCard,
                                    $uid,
                                    $res->result->refId,
                                    1
                                );
                                Hook::exec('actionPaymentCCAdd');
                            } else {
                                $msg = __METHOD__ . ' '
                                    . $this->module->l('expiry date') . ' ' . var_export($expForCard, true);
                                Przelewy24Logger::addLog($msg, 1);
                            }
                        } else {
                            $msg = __METHOD__ . ' ' . $this->module->l('credit card has no recurring') . ' ' .
                                var_export($hasRecurency, true);
                            Przelewy24Logger::addLog($msg, 1);
                        }
                    }
                }
            } catch (Exception $e) {
                Przelewy24Logger::addLog(__METHOD__ . ' ' . $e->getMessage(), 1);
            }
        }
    }

    /**
     * Set orders state.
     *
     * @param PrestaShopCollection $orders
     * @param int $state
     */
    private function setOrdersState($orders, $state)
    {
        foreach ($orders as $order) {
            /* Getting current state and comparing it with new one */
            $oldOrderStatus = $order->getCurrentOrderState();
            if ((int)$oldOrderStatus->id !== (int)$state) {
                $history = new OrderHistory();
                $history->id_order = (int)($order->id);
                $history->changeIdOrderState($state, (int)($order->id));
                $history->addWithemail(true);
            }
        }

        /* Any Order of the same set is fine. */
        if (isset($order) && method_exists($order, 'getOrderPaymentCollection')) {
            $payments = $order->getOrderPaymentCollection();
            if (count($payments) > 0) {
                $payments[0]->transaction_id = (int)Tools::getValue('p24_order_id');
                $payments[0]->update();
            }
        }
    }
}
