<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24PaymentReturnModuleFrontController.
 *
 * Class is named using PrestaShop convention described here:
 * https://devdocs.prestashop.com/1.7/modules/concepts/controllers/front-controllers/
 * File name should begin with lower case. Class name should be equal to:
 * $moduleName . ucfirst($fileName) .'ModuleFrontController'
 * To see more details please access PrestaShop class
 * /class/Dispatcher.php
 * on line 295 (method "dispatch")
 */
class Przelewy24PaymentReturnModuleFrontController extends Przelewy24FrontController
{
    /**
     * Init content.
     */
    public function initContent()
    {
        $cartId = (int) Tools::getValue('cart_id');
        $cart = new Cart($cartId);
        $orderId = Order::getOrderByCartId((int) $cart->id);
        $ok = $this->checkIfOrderIsOk($orderId);

        if ($ok) {
            $rawUrl = $this->context->link->getModuleLink(
                'przelewy24',
                'paymentSuccessful',
                array('cart_id' => $cart->id),
                '1' === Configuration::get('PS_SSL_ENABLED')
            );
        } else {
            $rawUrl = $this->context->link->getModuleLink(
                'przelewy24',
                'paymentFailed',
                array('cart_id' => $cart->id),
                '1' === Configuration::get('PS_SSL_ENABLED')
            );
        }
        $url = filter_var($rawUrl, FILTER_SANITIZE_URL);
        Tools::redirect($url);
    }

    /**
     * Check if order is ok
     * We receive information only when operation is successful. If order has default status or status does not exist
     * we can assume that operation failed.
     *
     * @param int $orderId
     *
     * @return bool
     */
    private function checkIfOrderIsOk($orderId)
    {
        if ($orderId) {
            $state1 = (int) Configuration::get(P24Configuration::P24_ORDER_STATE_1);
            $order = new Order($orderId);
            $currentState = (int) $order->getCurrentState();
            $ok = $currentState !== $state1 || !$currentState;
        } else {
            $ok = false;
        }
        return $ok;
    }
}
