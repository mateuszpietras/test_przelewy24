<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24MyStoredCardsModuleFrontController.
 *
 * Class is named using PrestaShop convention described here:
 * https://devdocs.prestashop.com/1.7/modules/concepts/controllers/front-controllers/
 * File name should begin with lower case. Class name should be equal to:
 * $moduleName . ucfirst($fileName) .'ModuleFrontController'
 * To see more details please access PrestaShop class
 * /class/Dispatcher.php
 * on line 295 (method "dispatch")
 */
class Przelewy24MyStoredCardsModuleFrontController extends Przelewy24FrontController
{
    /**
     * Parameter inherited from ModuleFrontController.
     * Indicates if user should be authenticated to access this view.
     *
     * @var bool
     */
    public $auth = true;

    /**
     * Parameter inherited from ModuleFrontController.
     * Should user be use ssl connection to access this view.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Init
     * Accessing parent parameters used by PrestaShop in ModuleFrontController, that is why underscore is used here.
     */
    public function init()
    {
        $this->page_name = 'mystoredcards';
        $this->display_column_left = false;
        $this->display_column_right = false;
        parent::init();
    }

    /**
     * Init content.
     */
    public function initContent()
    {
        parent::initContent();

        $cart = $this->context->cart;
        $smarty = $this->context->smarty;
        $cartId = (int) $cart->id;

        if (!$cartId) {
            $cart->add();
            $cartId = (int) $cart->id;
            Context::getContext()->cookie->__set('id_cart', $cartId);
        }

        $p24SessionId = $cartId.'|'.md5(time());

        $customerId = (int) Context::getContext()->customer->id;
        $customerSettings = new CustomerSettingsModel($customerId);
        $recurringModel = RecurringModel::loadByCustomerId($customerId);

        // submit settings form
        if (Tools::isSubmit('submit') && $customerId) {
            $customerSettings->cc_forget = ((int) Tools::getValue('ccards_forget') ? 1 : 0);

            if ($customerSettings->customer_id) {
                $customerSettings->update();
            } else {
                $customerSettings->customer_id = $customerId;
                $customerSettings->add();
            }

            // remove cards
            if ((int) Tools::getValue('ccards_forget') && $recurringModel) {
                $recurringModel->delete();
            }
        }

        $smarty->assign('ccards_forget', !$customerSettings->shouldCardBeRemembered());

        if ('1' === Configuration::get(P24Configuration::P24_ONECLICK_ENABLED)) {
            $smarty->assign('ccards', RecurringModel::getCustomerCards($customerId));
            $smarty->assign('p24_session_id', $p24SessionId);
            $protocol = Przelewy24::getHttpProtocol();
            $smarty->assign(
                'base_url',
                $protocol.htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8')
                .__PS_BASE_URI__
            );
            $smarty->assign(
                'p24_ajax_url',
                $this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentAjax',
                    array(),
                    '1' === Configuration::get('PS_SSL_ENABLED')
                )
            );
            $smarty->assign(
                'p24_url_status',
                $this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentStatus',
                    array(),
                    '1' === Configuration::get('PS_SSL_ENABLED')
                )
            );
            $smarty->assign('clientId', Context::getContext()->customer->id);
            $smarty->assign('secure', '1' === Configuration::get('PS_SSL_ENABLED'));

            $this->setTemplate('myStoredCardsList.tpl');
        }
    }
}
