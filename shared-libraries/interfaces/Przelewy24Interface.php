<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Interface Przelewy24Interface.
 */
interface Przelewy24Interface
{
    /**
     * Set translations.
     *
     * @param array $translations
     */
    public function setTranslations(array $translations = array());
}
