<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Upgrade module method. Listed by
 * classess/modules/Module::loadUpgradeVersionList
 * from PrestaShop source code.
 *
 * @return bool
 */
function upgradeModule340()
{
    $results = array();
    $results[] = Db::getInstance()->execute(
        'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'p24_recuring` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `website_id` INT NOT NULL,
              `customer_id` INT NOT NULL,
              `reference_id` VARCHAR(35) NOT NULL,
              `expires` VARCHAR(4) NOT NULL,
              `mask` VARCHAR (32) NOT NULL,
              `card_type` VARCHAR (20) NOT NULL,
              `timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
              UNIQUE KEY `UNIQUE_FIELDS` (`mask`,`card_type`,`expires`,`customer_id`,`website_id`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;'
    );

    $results[] = Db::getInstance()->execute(
        'CREATE TABLE IF NOT EXISTS`'._DB_PREFIX_.'przelewy24_amount` (
                `i_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                `s_sid`	char(32) NOT NULL,
                `i_id_order` INT UNSIGNED NOT NULL,
                `i_amount` INT UNSIGNED NOT NULL,
                `p24_method` INT    
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;'
    );

    $results[] = Db::getInstance()->execute(
        'CREATE TABLE IF NOT EXISTS`'._DB_PREFIX_.'przelewy24_lastmethod` (
                `customer_id` INT NOT NULL PRIMARY KEY,
                `p24_method` INT NOT NULL
 	        ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;'
    );

    $results[] = Db::getInstance()->execute(
        'CREATE TABLE IF NOT EXISTS`'._DB_PREFIX_.'przelewy24_customersettings` (
                `customer_id` INT NOT NULL PRIMARY KEY,
                `cc_forget` INT DEFAULT 0
		    ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;'
    );

    $results[] = Db::getInstance()->execute(
        'CREATE TABLE IF NOT EXISTS`'._DB_PREFIX_.'przelewy24_extracharges` (
                `id_extracharge` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                `id_order` INT NOT NULL,
                `extracharge_amount` DECIMAL(20,6) NOT NULL
			 ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;'
    );

    // install result
    foreach ($results as $result) {
        if (!$result) {
            return false;
        }
    }

    return true;
}
