<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Upgrade module method. Listed by
 * classess/modules/Module::loadUpgradeVersionList
 * from PrestaShop source code.
 *
 * @return bool
 */
function upgradeModule3420()
{
    return Db::getInstance()->execute(
        'ALTER TABLE `'._DB_PREFIX_.'przelewy24_extracharges` ADD `extracharge_vat` DECIMAL(5,2) NOT NULL;'
    );
}
