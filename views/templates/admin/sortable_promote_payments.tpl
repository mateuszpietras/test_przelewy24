{*
* @author Przelewy24
* @copyright Przelewy24
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

<fieldset>
    <table>
        <tr>
            <td>
                <div id="paymethod_promote_list{$suffix|escape:'html'}" {if not $isPayMethodPromoteChecked} class="display_none" {/if}>
                    {if isset($paymentListToPromote) && $paymentListToPromote}
                    {foreach from=$paymentListToPromote item=paymentToPromote}
                        <label class="paylistprom_item paylistprom_item_{$paymentToPromote.id|escape:'html'}"
                               data-sufix="suf{$suffix|escape:'html'}"
                               for="paylistprom_{$paymentToPromote.id|escape:'html'}">
                            <span class="ui-icon ui-icon-grip-dotted-vertical cursor_inline"></span>
                            <input class="paylistprom" id="paylistprom_{$paymentToPromote.id|escape:'html'}{$suffix|escape:'html'}" type="checkbox" data-val="{$paymentToPromote.id|escape:'html'}" data-sufix="suf{$suffix|escape:'html'}" {if $paymentToPromote.checked}checked="checked"{/if}>
                            <span class="relative_top_2">{$paymentToPromote.name|escape:'html'}</span>
                        </label>
                    {/foreach}
                    {else}
                        -

                    {/if}
                </div>
            </td>
        </tr>
    </table>
</fieldset>
