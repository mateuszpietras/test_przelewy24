{*
* @author Przelewy24
* @copyright Przelewy24
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

<button class="btn btn-primary" type="button" onClick="setAutoOrder('{$currencySuffix|escape:'html'}')">
    {l s='Set the payment methods order of the most used' mod='przelewy24'}
</button>
