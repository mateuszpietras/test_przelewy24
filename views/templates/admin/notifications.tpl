{*
* @author Przelewy24
* @copyright Przelewy24
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

{nocache}
    {foreach from=$flashMessages item=flash}
        <div class="alert alert-{$flash.type|escape:'html'}"><button type="button" class="close" data-dismiss="alert">×</button>
            {if $flash.currency}
                ({$flash.currency|escape:'html'})
            {/if}
            {l s=$flash.text mod='przelewy24'}
        </div>
    {/foreach}
{/nocache}
