{*
* @author Przelewy24
* @copyright Przelewy24
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

<div data-type="p24subformconfig"
     data-min="{$minIndex|escape:'html'}"
     data-currency="{$currencyCode|escape:'html'}"
     data-suffix="{$currencySuffix|escape:'html'}"></div>
