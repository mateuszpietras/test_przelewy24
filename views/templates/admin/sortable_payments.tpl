{*
* @author Przelewy24
* @copyright Przelewy24
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

<fieldset>
    <div class="paymethod">
        <div class="margin-left-2em">
            <p class="top_1em">{l s='Payment methods visible right away:' mod='przelewy24'} ({l s='Drop here max. ' mod='przelewy24'}{$maxNumberOfPaymentMethods|escape:'html'} {l s=' payment methods' mod='przelewy24'})</p>
            <div class="sortable selected float_left" id="selected{$suffix|escape:'html'}" data-id="{$suffix|escape:'html'}" data-max="{$maxNumberOfPaymentMethods|escape:'html'}">
                {foreach from=$paymentMethodsSelected item=bank}
                    <a class="bank-box" id="bank_{$bank.id|escape:'html'}_{$suffix|escape:'html'}" data-id="{$bank.id|escape:'html'}" data-sufix="suf{$suffix|escape:'html'}"><div class="bank-logo bank-logo-{$bank.id|escape:'html'}"></div><div class="bank-name">{$bank.name|escape:'html'}</div></a>
                {/foreach}
            </div>
            <img src="../modules/przelewy24/views/img/arrow-left-icon.png" class="arrow_left_icon">
            <p class="clear_margins">{l s='Payment methods visible on more-button:' mod='przelewy24'}</p>
            <div class="sortable available min_height_50" id="available{$suffix|escape:'html'}" data-id="{$suffix|escape:'html'}">
                {foreach from=$paymentMethodsAvailable item=bank}
                    <a class="bank-box" id="bank_{$bank.id|escape:'html'}_{$suffix|escape:'html'}" data-id="{$bank.id|escape:'html'}" data-sufix="suf{$suffix|escape:'html'}"><div class="bank-logo bank-logo-{$bank.id|escape:'html'}"></div><div class="bank-name">{$bank.name|escape:'html'}</div></a>
                {/foreach}
                <div class="clear_both" id="clear"></div></div>
            <div class="tempHolder" data-id="suf{$suffix|escape:'html'}"></div>
        </div>
</fieldset>
