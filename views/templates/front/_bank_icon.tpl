{*
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

{$not_in=$not_in|default:array()}
{$cc_id=$cc_id|default:""}
{$onclick=$onclick|default:""}
{$class=$class|default:""}
{$text=$text|default:""}
{if !empty($bank_name) && !in_array($bank_id, $not_in)}
<a class="bank-box {$class|escape:'html'}" data-id="{$bank_id|escape:'html'}" data-cc="{$cc_id|escape:'html'}" onclick="{$onclick|escape:'html'}"><div class="bank-logo bank-logo-{$bank_id|escape:'html'}" >{if $text}<span>{$text|escape:'html'}</span>{/if}</div><div class="bank-name">{$bank_name|escape:'html'}</div></a>
{/if}
