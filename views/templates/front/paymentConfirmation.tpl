{*
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

<link rel="stylesheet" type="text/css" href="{$modules_dir|escape:'html'}przelewy24/views/css/paymentConfirmation.css"/>
<style>
    .moreStuff:before {
        content: "\f078    {l s='more payment methods' mod='przelewy24'}    \f078";
    }
    .lessStuff:before {
        content: "\f077     {l s='less payment methods' mod='przelewy24'}    \f077";
    }
</style>


{capture name=path}{l s='Pay with Przelewy24' mod='przelewy24'}{/capture}
{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

<div style="clear:both"></div>


{$dictionary = [
    "cardHolderLabel" => "{l s='Cardholder name' mod='przelewy24'}",
    "cardNumberLabel" => "{l s='Card number' mod='przelewy24'}",
    "cvvLabel" => "{l s='cvv' mod='przelewy24'}",
    "expDateLabel" => "{l s='Expiry date' mod='przelewy24'}",
    "payButtonCaption" => "{l s='Confirm' mod='przelewy24'}",
    "threeDSAuthMessage" => "{l s='Click here to continue shopping' mod='przelewy24'}"
]|json_encode}

<div
    data-type="p24PaymentConfirmation"
    data-validationrequired="{$p24_validationRequired|escape:'html'}"
    data-ajaxurl="{$p24_ajax_url|escape:'html'}"
    data-orderid="{$get_order_id|escape:'html'}"
    data-cartid="{$cartId|escape:'html'}"
    data-acceptinshop="{$accept_in_shop|escape:'html'}"
    data-urlstatus="{$p24_url_status|escape:'html'}"
    data-sessionid="{$p24_session_id|escape:'html'}"
    data-urlreturn="{$p24_url_return|escape:'html'}"
    data-failureinfo="{l s='Script loading failure. Try again or choose another payment method.' mod='przelewy24'}"
    data-p24css="{$p24_css|escape:'html'}"
    data-dictionary='{$dictionary|escape:'html'}'
    data-sign="{$p24_sign|escape:'html'}"

></div>

<div class="content box">
    <div class="content_in content_in_padding_top">
        <h2><a href="http://przelewy24.pl" target="_blank"><img src="{$modules_dir|escape:'html'}przelewy24/views/img/logo.png"
                                                                alt="{l s='Payment confirmation' mod='przelewy24'}"/></a>&nbsp;{l s='Pay with Przelewy24' mod='przelewy24'}
        </h2>
        <hr/>
            <h4>{l s='Transaction data:' mod='przelewy24'}</h4>
            <table>
                <tr>
                    <th>{l s='Order name' mod='przelewy24'}&nbsp;</th>
                    <td id="summary_opis">{$p24_description|escape:'html'}</td>
                </tr>
                <tr>
                    <th>{l s='Customer' mod='przelewy24'}</th>
                    <td>{$p24_client|escape:'html'}</td>
                </tr>
                <tr>
                    <th>{l s='E-mail' mod='przelewy24'}</th>
                    <td>{$p24_email|escape:'html'}</td>
                </tr>
                <tr>
                    <th>{l s='Amount' mod='przelewy24'}</th>
                    <td id="p24-summary-amount">
                        {$p24_classic_amount|escape:'html'}

                        {if !empty($extracharge_amount) }
                            ({l s='Added extra charge to order' mod='przelewy24'}:
                            {$extracharge_amount|escape:'html'})
                        {/if}

                        {if !empty($extradiscount_amount) }
                            ({l s='Added extra discount to order' mod='przelewy24'}:
                            {$extradiscount_amount|escape:'html'})
                        {/if}
                    </td>
                </tr>
                {if isset($p24FreeOrder) and $p24FreeOrder}
                    <tr>
                        <td colspan="2">
                            <strong>
                                {l s='Created order' mod='przelewy24'}
                            </strong>
                        </td>
                    </tr>
                {/if}
            </table>
            <hr/>
        <div class="product_attributes clearfix p24-raty-calc" id="left_payment_parts">
            {if $showInstallment && $product_ammount >= {$minIstallmentValue}}
                <a target="_blank" style="font-size:14px;" href="{$liveUrl|escape:'html'}kalkulator_raty/index.html?ammount={$product_ammount|escape:'html'}">
                    <img width="70" style="margin-right: 5px;" src="{$liveUrl|escape:'html'}kalkulator_raty/img/logoAR.jpg">{$part_count|escape:'html'} rat x ~{$part_cost|escape:'html'} zł </a> <br>
                <a target="_blank" style="font-size:14px;" href="http://www.mbank.net.pl/mraty_1/index.html?kwota={$product_ammount|escape:'html'}"> <img width="110" style="margin-right: 5px;" src="{$modules_dir|escape:'html'}przelewy24/views/img/logo_136.gif"></a><hr/>
            {/if}
        </div>
        {if empty($p24FreeOrder) or !$p24FreeOrder}

            {if isset($productsNumber) && $productsNumber <= 0}
                <p style="font-size: 16px; line-height: 20px;">{l s='Your shopping cart is empty.' mod='przelewy24'}</p>
            {else}

                {if $accept_in_shop}
                    <p>
                        <p id="P24_regulation_accept_text" style="color: red;">{l s='Please accept the Przelewy24 Terms' mod='przelewy24'}</p>
                        <label for="p24_regulation_accept" style="font-weight: normal;">
                            <input type="checkbox" name="p24_regulation_accept" id="p24_regulation_accept"
                                   style="display: inline-block;" onchange="p24_regulation_accept_checked();"/>
                            {$accept_in_shop_translation|escape:"html"}
                        </label>
                    </p>
                    <hr/>
                {/if}

                {if isset($oneclick) && isset($p24_method) && isset ($p24_ajax_url) && $oneclick && empty($p24_method)}
                    <p>
                        {l s='In case of choosing credit/debit card as pyment method, it\'s reference number will be saved for further payments.' mod='przelewy24'}
                    <div class="checkbox">
                        <label class="control-label">
                            {l s='Do not memorize payment cards, which I pay' mod='przelewy24'}
                            <input type="checkbox" name="ccards_forget"
                                   {if $ccards_forget}checked="checked"{/if}
                                   onclick="$.ajax({ url: '{$p24_ajax_url|escape:'html'}', method: 'POST', type: 'POST', data: { action: 'cc_forget', value: $(this).is(':checked') } })"/>
                        </label>
                    </div>
                    </p>
                {/if}
                {if isset($accept_in_shop_accepted) && $accept_in_shop_accepted}
                    {$accept_in_shop_accepted_val = 1}
                {else}
                    {$accept_in_shop_accepted_val = 0}
                {/if}
                <form action="{$p24_url|escape:'html'}" method="post" id="przelewy24Form" name="przelewy24Form"
                      accept-charset="utf-8">
                    <input type="hidden" name="p24_merchant_id" value="{$p24_merchant_id|escape:'html'}"/>
                    <input type="hidden" name="p24_session_id" value="{$p24_session_id|escape:'html'}"/>
                    <input type="hidden" name="p24_pos_id" value="{$p24_pos_id|escape:'html'}"/>
                    <input type="hidden" name="p24_amount" value="{$p24_amount|escape:'html'}"/>
                    <input type="hidden" name="p24_currency" value="{$p24_currency|escape:'html'}"/>

                    <input type="hidden" name="p24_description" value="{$p24_description|escape:'html'}" id="orderDescription"/>
                    <input type="hidden" name="p24_email" value="{$p24_email|escape:'html'}"/>
                    <input type="hidden" name="p24_client" value="{$p24_client|escape:'html'}"/>
                    <input type="hidden" name="p24_address" value="{$p24_address|escape:'html'}"/>
                    <input type="hidden" name="p24_zip" value="{$p24_zip|escape:'html'}"/>
                    <input type="hidden" name="p24_city" value="{$p24_city|escape:'html'}"/>
                    <input type="hidden" name="p24_country" value="{$p24_country|escape:'html'}"/>
                    <input type="hidden" name="p24_language" value="{$p24_language|escape:'html'}"/>

                    <input type="hidden" name="p24_method" value="{$p24_method|escape:'html'}"/>
                    {if $payslow_enabled}<input type="hidden" name="p24_channel" value="16"/>{/if}
                    {if isset($custom_timelimit)}<input type="hidden" name="p24_time_limit"
                                                        value="{$custom_timelimit|escape:'html'}"/>{/if}

                    <input type="hidden" name="p24_encoding" value="{$p24_encoding|escape:'html'}"/>
                    <input type="hidden" name="p24_url_status" value="{$p24_url_status|escape:'html'}"/>
                    <input type="hidden" name="p24_url_return" value="{$p24_url_return|escape:'html'}"/>
                    <input type="hidden" name="p24_api_version" value="{$p24_api_version|escape:'html'}"/>
                    <input type="hidden" name="p24_ecommerce" value="{$p24_ecommerce|escape:'html'}"/>
                    <input type="hidden" name="p24_ecommerce2" value="{$p24_ecommerce2|escape:'html'}"/>
                    <input type="hidden" name="p24_sign" value="{$p24_sign|escape:'html'}"/>
                    <input type="hidden" name="p24_regulation_accept" value="{$accept_in_shop_accepted_val|escape:'html'}"/>
                    <input type="hidden" name="p24_wait_for_result" value="{$p24_wait_for_result|escape:'html'}"/>
                    <input type="hidden" name="p24_shipping" value="{$p24_shipping|escape:'html'}"/>

                    {foreach $p24ProductItems as $name => $value}
                        <input type="hidden" name="{$name|escape:'html'}" value="{$value|escape:'html'}"/>
                    {/foreach}

                    <p style="color: red;" id="ajaxOutput"></p>
                </form>
                {if (empty($p24_cards) && !$pay_in_shop && !$last_pay_method) || $p24_method}
                    {* nie ma recurringu, zapisanej ostatniej metody, nie ma kart JS, nie ma metod płatności w sklepie - lub po prostu zawczasu ustalono metodę płatności *}
                    {if $p24_paymethod_list && !$p24_method}
                        <p>{l s='Choose payment method, then press "Confirm" to complete the payment process.' mod='przelewy24'}</p>
                    {elseif array_key_exists($p24_method, $p24_paymethod_all) }
                        <p><strong>{l s='Your payment method:' mod='przelewy24'}</strong></p>
                        {include file="./_bank_element.tpl" bank_id=$p24_method bank_name=$p24_paymethod_all[$p24_method]}
                        <p>{l s='Press "Confirm" to confirm your order and be redirected to przelewy24.pl, where you can complete the payment process.' mod='przelewy24'}</p>
                    {/if}
                    {if !$p24_method}
                        {$hidePayments=false}
                    {/if}
                    {if ($oneclick && empty($p24_method) && !$ccards_forget) || $extracharge_enabled}
                        <hr>
                    {/if}
                {else}
                    {if ($oneclick && empty($p24_method) && !$ccards_forget) || $extracharge_enabled}
                        <hr>
                    {/if}
                    {if isset($p24_recuring_url)}
                    <form action="{$p24_recuring_url|escape:'html'}" method="post" id="przelewy24FormRecuring"
                          name="przelewy24FormRecuring" accept-charset="utf-8">
                        <input type="hidden" name="p24_session_id" value="{$p24_session_id|escape:'html'}"/>
                        <input type="hidden" name="p24_amount" value="{$p24_amount|escape:'html'}"/>
                        <input type="hidden" name="p24_currency" value="{$p24_currency|escape:'html'}"/>
                        <input type="hidden" name="p24_description" value="{$p24_description|escape:'html'}" id="orderDescription"/>
                        <input type="hidden" name="p24_email" value="{$p24_email|escape:'html'}"/>
                        <input type="hidden" name="p24_client" value="{$p24_client|escape:'html'}"/>
                        <input type="hidden" name="p24_regulation_accept" value="{$accept_in_shop_accepted_val|escape:'html'}"/>
                        <input type="hidden" name="p24_cc"/>
                    </form>
                    {/if}
                {/if}
                {if $p24_paymethod_list && !$p24_method}
                    {* pokazuj listę banków w sklepie *}
                    {if $p24_paymethod_graphics}
                        {* lista graficzna *}
                        <div id="P24FormAreaHolder" onclick="hidePayJsPopup();" style="display: none">
                            <div onclick="arguments[0].stopPropagation();" id="P24FormArea" class="popup"></div>
                        </div>
                        <div class="payMethodList">
                            {$ignoreArr=array()}
                            {if $last_pay_method}
                                {include file="./_bank_icon.tpl" bank_id=$last_pay_method bank_name="Ostatnio używane"}
                                {$ignoreArr[]=$last_pay_method}
                            {/if}

                            {foreach from=$p24_cclist key=id item=card}
                                {include file="./_bank_icon.tpl" bank_id=$card.type|md5 bank_name=$card.type text=$card.mask|substr:-9 class="recurring" cc_id=$id}
                            {/foreach}

                            {if $pay_in_shop}
                                {$ignoreArr[]=140}
                                {$ignoreArr[]=142}
                                {$ignoreArr[]=145}
                                {$ignoreArr[]=218}
                                {include file="./_bank_icon.tpl" bank_id=218 bank_name=$p24_paymethod_all.218 onclick="showPayJsPopup()"}
                                {include file="./_bank_icon.tpl" bank_id=145 bank_name=$p24_paymethod_all.145 onclick="showPayJsPopup()"}
                            {/if}

                            {foreach $p24_paymethod_first as $bank_id}
                                {include file="./_bank_icon.tpl" bank_id=$bank_id bank_name=$p24_paymethod_all.$bank_id not_in=$ignoreArr onclick=""}
                            {/foreach}
                            <div style="clear:both"></div>
                            {if $p24_paymethod_second|sizeof > 0}
                                <div class="morePayMethods" style="display: none">
                                    {foreach $p24_paymethod_second as $bank_id}
                                        {include file="./_bank_icon.tpl" bank_id=$bank_id bank_name=$p24_paymethod_all.$bank_id not_in=$ignoreArr}
                                    {/foreach}
                                    {foreach $p24_paymethod_all as $bank_id => $bank_name}
                                        {if !in_array($bank_id, $p24_paymethod_first) && !in_array($bank_id, $p24_paymethod_second)}
                                            {include file="./_bank_icon.tpl" bank_id=$bank_id bank_name=$bank_name not_in=$ignoreArr}
                                        {/if}
                                    {/foreach}
                                    <div style="clear:both"></div>
                                </div>
                                <div class="moreStuff"
                                     onclick="$(this).fadeOut(100);$('.lessStuff').fadeIn(); $('.morePayMethods').slideDown()"
                                     title="Pokaż więcej metod płatności"></div>
                                <div class="lessStuff"
                                     onclick="$(this).fadeOut(100);$('.moreStuff').fadeIn(); $('.morePayMethods').slideUp()"
                                     title="Pokaż mniej metod płatności" style="display:none"></div>
                            {/if}
                        </div>
                    {else}
                        {* lista tekstowa *}
                        <ul>
                            {$ignoreArr=array()}
                            {if $last_pay_method}
                                {$ignoreArr[]=$last_pay_method}
                                <li>
                                    <input type="radio" id="przelewy24lastmethod" name="p24_cc" value="last_method"
                                           data-method="{$last_pay_method|escape:'html'}"/>
                                    <label for="przelewy24lastmethod"
                                           style="font-weight:normal;position:relative; top:-3px;">
                                        {l s='Choose last used payment method' mod='przelewy24'}
                                        <div class="bank-logo bank-logo-{$last_pay_method|escape:'html'}"
                                             id="przelewy24lastmethod_img"
                                             style="display: inline-block; margin-bottom: -22px;">
                                    </label>
                                </li>
                            {/if}

                            {foreach from=$p24_cards key=id item=desc name=foo}
                                <li>
                                    <input type="radio" name="p24_cc" id="p24_cc_{$id|escape:'html'}" value="{$id|escape:'html'}"
                                           {if $smarty.foreach.foo.first}checked="checked"{/if} />
                                    <label for="p24_cc_{$id|escape:'html'}"
                                           style="font-weight:normal;position:relative; top:-3px;">{$desc|escape:'html'}
                                        <a href="{$p24_recuring_url|escape:'html'}?cardrm={$id|escape:'html'}">{l s='remove' mod='przelewy24'}</a>
                                    </label>
                                </li>
                            {/foreach}

                            {if $pay_in_shop}
                                {$ignoreArr[]=140}
                                {$ignoreArr[]=142}
                                {$ignoreArr[]=145}
                                <li class="przelewy24AjaxCardBox">
                                    <input type="radio" id="przelewy24ajaxcard" name="p24_cc" value="ajax_card"/>
                                    <label for="przelewy24ajaxcard"
                                           style="font-weight:normal;position:relative; top:-3px;">
                                        {l s='Choose credit/debit card as payment method' mod='przelewy24'}
                                    </label>
                                    <div id="P24FormArea" style="display: none"></div>
                                </li>
                            {/if}

                            <li>
                                <input type="radio" id="przelewy24standard" name="p24_cc" value="0"/>
                                <label for="przelewy24standard" style="font-weight:normal;position:relative; top:-3px;">
                                    {l s='Choose another payment method.' mod='przelewy24'}
                                </label>
                            </li>
                        </ul>
                        <div style="margin: 0 3em; {if $hidePayments|default:true}display: none;{/if}"
                             class="payMethodList">
                            <ul>
                                {if count($p24_paymethod_first) > 0}
                                        {foreach $p24_paymethod_first as $bank_id}
                                            {include file="./_bank_item.tpl" bank_id=$bank_id bank_name=$p24_paymethod_all.$bank_id not_in=$ignoreArr}
                                        {/foreach}
                                {/if}
                                {if sizeof($p24_paymethod_second) > 0}
                                    <div class="morePayMethods" style="display: none">
                                        {foreach $p24_paymethod_second as $bank_id}
                                            {include file="./_bank_item.tpl" bank_id=$bank_id bank_name=$p24_paymethod_all.$bank_id not_in=$ignoreArr}
                                        {/foreach}
                                        {foreach $p24_paymethod_all as $bank_id => $bank_name}
                                            {if !in_array($bank_id, $p24_paymethod_first) && !in_array($bank_id, $p24_paymethod_second)}
                                                {include file="./_bank_item.tpl" bank_id=$bank_id bank_name=$bank_name not_in=$ignoreArr}
                                            {/if}
                                        {/foreach}
                                    </div>
                                    <div class="moreStuff"
                                         onclick="$(this).fadeOut(100);$('.morePayMethods').slideDown()"
                                         title="{l s='Show more payment methods' mod='przelewy24'}"></div>
                                {/if}
                            </ul>
                        </div>
                    {/if} {* /if tekstowo/graficznie *}
                {/if} {* /if pokazuj listę banków w sklepie *}
            {/if} {* /if empty cart *}
        {/if}
    </div>

    {if empty($p24FreeOrder) or  !$p24FreeOrder}
        <p class="cart_navigation cart_navigation_next"
           style="display: block !important; overflow: hidden; margin: 15px 0;">
            {if isset($productsNumber) && $productsNumber <= 0}
                <a href="{$base_dir_ssl|escape:'html'}index.php" class="button-exclusive btn btn-default">
                    <i class="icon-chevron-left"></i>{l s='Return to shop' mod='przelewy24'}
                </a>
            {else}
                {if !$get_order_id}
                    <a href="{$back_url|escape:'html'}"
                       class="{if $smarty.const._PS_VERSION_ >= 1.6 }button-exclusive btn btn-default{else}button_large{/if}">
                        <i class="icon-chevron-left"></i>{l s='Other payment methods' mod='przelewy24'}
                    </a>
                {/if}
                <a class="{if $smarty.const._PS_VERSION_ >= 1.6 }button btn btn-default button-medium{else}exclusive_large{/if}"
                   href="javascript:{if !$p24_validationRequired}if(!($('#przelewy24FormRecuring input[name=p24_cc]').val() > 0)) simpleFormSubmit(); else {/if}proceedPayment()">

				<span id="proceedPaymentLink">
					{l s='Confirm' mod='przelewy24'}&nbsp;<img src="{$modules_dir|escape:'html'}przelewy24/views/img/ajax.gif"
                                                               style="display: none; width: 20px; height: 20px;"/>
				</span>
                </a>
            {/if}
        </p>
        <p style="text-align: right">
            {l s='Confirm order required to pay' mod='przelewy24'}
        </p>
    {else}
        <p class="cart_navigation cart_navigation_next"
           style="display: block !important; overflow: hidden; margin: 15px 0;">
            <a class="{if $smarty.const._PS_VERSION_ >= 1.6 }button btn btn-default button-medium{else}exclusive_large{/if}"
               href="{$url_history|escape:'html'}">
               <span>
                    {l s='Order history' mod='przelewy24'}
               </span>
            </a>
        </p>
    {/if}
</div>

<script src="{$modules_dir|escape:'html'}przelewy24/views/js/paymentConfirmation.js"></script>