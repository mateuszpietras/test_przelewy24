{*
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

{$not_in=$not_in|default:array()}
{if !empty($bank_name) && !in_array($bank_id, $not_in)}
    <div class="bank-element">
        <div class="bank-logo bank-logo-{$bank_id|escape:'html'}" >
            {if isset($text) && $text}<span>{$text|escape:'html'}</span>{/if}
        </div>
        <strong>{$bank_name|escape:'html'}</strong>
    </div>
{/if}