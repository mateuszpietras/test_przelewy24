{*
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

{$not_in=$not_in|default:array()}
{if !empty($bank_name) && !in_array($bank_id, $not_in)}
	<li>
		<input type="radio" class="bank-box" data-id="{$bank_id|escape:'html'}" id="paymethod-bank-id-{$bank_id|escape:'html'}" name="paymethod-bank">
		<label for="paymethod-bank-id-{$bank_id|escape:'html'}" style="font-weight:normal;position:relative; top:-3px;">
            {$bank_name|escape:'html'}
		</label>
	</li>
{/if}
