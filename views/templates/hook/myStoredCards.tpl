{*
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

<li class="lnk_wishlist">
    <a href="{$link->getModuleLink('przelewy24', 'myStoredCards')|escape:'htmlall':'UTF-8'}" title="{l s='My stored cards' mod='przelewy24'}">
        <i class="icon-credit-card"></i>
        <span>{l s='My stored cards' mod='przelewy24'}</span>
    </a>
</li>

<style>
    @font-face {
        font-family: 'FontAwesome';
        src: url("../modules/przelewy24/views/fonts/fontawesome-webfont.eot?v=4.1.0");
        src: url("../modules/przelewy24/views/fonts/fontawesome-webfont.eot?#iefix&v=4.1.0") format("embedded-opentype"),
        url("../modules/przelewy24/views/fonts/fontawesome-webfont.woff?v=4.1.0") format("woff"),
        url("../modules/przelewy24/views/fonts/fontawesome-webfont.ttf?v=4.1.0") format("truetype"),
        url("../modules/przelewy24/views/fonts/fontawesome-webfont.svg?v=4.1.0#fontawesomeregular") format("svg");
        font-weight: normal;
        font-style: normal
    }
</style>