{*
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

<script>
    $(document).ready(function() {
        $('.totalprice.item').before(
            '<tr class="item" id="extracharge">' +
            '<td colspan="1"><strong>{$extracharge_text|escape:'html'}</strong></td>' +
            '<td colspan="4">{$extracharge|escape:'html'}</td>' +
            '</tr>');
    });
</script>
