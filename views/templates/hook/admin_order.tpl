{*
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

<ul id="przelewy24-admin-order-data" style="display: none;">
    <li class="extracharge_text">
        {$przelewy24_extracharge_text|escape:'htmlall':'UTF-8'}
    </li>
    <li class="extracharge_amount">
        {displayPrice currency=$przelewy24_extracharge_currency_id price=$przelewy24_extracharge_amount}
    </li>
</ul>
