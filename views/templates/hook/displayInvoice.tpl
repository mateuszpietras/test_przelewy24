{*
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

<ul
        data-type="p24invoiceconfiguration"
        data-url="{$ajaxUrl|escape:'html'}"
        data-order="{$orderId|escape:'html'}"
        data-phone="{$clientPhone|escape:'html'}"
        data-confirm="{l s='Do you want to start payment by IVR and make call back to the customer?' mod='przelewy24'}"

        style="display: none"
>

    <li>
        <a id="page-header-p24-ivr" class="toolbar_btn" href="#" title="IVR">
            <i class="process-icon-phone icon-phone"></i><div>{l s='Pay with IVR' mod='przelewy24'}</div>
        </a>
    </li>

</ul>

<script src="{$modules_dir|escape:'html'}przelewy24/views/js/displayInvoiceScripts.js"></script>