{*
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

<div class="tab-content panel">
    <div class="panel-heading">
        {l s='Refunds to Przelewy24' mod='przelewy24'}

    </div>

    {if $refundError}

        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>
            {foreach from=$refundError item=foo}
                <p>{l s=$foo mod='przelewy24'}</p>
            {/foreach}
        </div>

    {/if}
    {if !$error}
        <p>
            {l s='The function of generating returns from the store requires additional configuration of Przelewy24 account. To activate this functionality, please contact the Customer Service Department at the address serwis@przelewy24.pl.' mod='przelewy24'}
        </p>
    {else}
        {if $amount > 0}
            {assign var="amountToRefund" value=$amount/100}
            <p>
                {l s='Here you can send a refund to the customer. The amount of the refund may not exceed the value of the transaction and the amount of funds available in your account.' mod='przelewy24'}
            </p>
            <p>{l s='Amount to refund' mod='przelewy24'}: {$amountToRefund|escape:'html'} {$currency|escape:'html'}</p>
            <form class="form-horizontal hidden-print" method="post">
                <div class="form-group">
                    <div class="col-lg-2">
                        <label for="amountToRefund">{l s='Amount' mod='przelewy24'}</label>
                        <input class="form-control" id="amountToRefund" type="number" name="amountToRefund"
                               value="{$amountToRefund|escape:'html'}" step="0.01"/>
                        <input onclick="return confirm('{l s='This will generate outgoing transfer. Can you confirm the operation?' mod='przelewy24'}')"
                               class="btn btn-primary pull-right" type="submit" name="submitRefund" value="{l s='Send' mod='przelewy24'}">
                    </div>
                </div>
            </form>
        {elseif $refunds}
            <p>{l s='The payment has already been fully refunded - no funds to make further returns.' mod='przelewy24'}</p>
        {/if}

        {if $refunds && $getRefundInfo}
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <span class="title_box">
                                {l s='Amount refunded' mod='przelewy24'}
                            </span>
                        </th>
                        <th>
                            <span class="title_box">
                                {l s='Date of refund' mod='przelewy24'}
                            </span>
                        </th>
                        <th>
                            <span class="title_box">
                                Status
                            </span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$refunds item=refund}
                        <tr>
                            <td>
                                {$refund['amount_refunded']/100|escape:'html'} {$currency|escape:'html'}
                            </td>
                            <td>
                                {$refund['created']|escape:'html'}
                            </td>
                            <td>
                                {$refund['status']|escape:'html'}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        {/if}
        {if !$getRefundInfo || (!$amount && !$refunds)}
            <p>{l s='To display refunds balance, please contact the Przelewy24 Customer Service' mod='przelewy24'}</p>
        {/if}
    {/if}
</div>

