{*
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

<div class="box" style="overflow: auto;">
	<h2><a href="http://przelewy24.pl" target="_blank"><img src="{$modules_dir|escape:'html'}przelewy24/views/img/logo.png" alt="{l s='Pay with Przelewy24' mod='przelewy24'}"/></a>&nbsp;{l s='Pay with Przelewy24' mod='przelewy24'}</h2>
	<p>{l s='Your payment was not confirmed by Przelewy24. If you want to retry press button "Retry".' mod='przelewy24'}</p>
	<p class="cart_navigation">
		<a class="exclusive_large" href="{$p24_retryPaymentUrl|escape:'html'}">
			<span id="proceedPaymentLink">
				{l s='Retry' mod='przelewy24'}
			</span>
		</a>
	</p>
</div>

{if $extracharge_text && $extracharge}
	<script>
        $(document).ready(function() {
            $('.totalprice.item').before(
                '<tr class="item" id="extracharge">' +
                '<td colspan="1"><strong>{$extracharge_text|escape:'html'}</strong></td>' +
                '<td colspan="4">{$extracharge|escape:'html'}</td>' +
                '</tr>');
        });
	</script>
{/if}
