{*
* @license https://www.gnu.org/licenses/lgpl-3.0.en.html
*}

{if $p24_message}
	<div style="display: none" id="{$p24_message_id|escape:'html'}">{$p24_message|escape:'html'}</div>
	<script type="text/javascript">
        $(document).ready(function(){
            $('#content').prepend($("#{$p24_message_id|escape:'html'}").show());
        });
	</script>
{/if}