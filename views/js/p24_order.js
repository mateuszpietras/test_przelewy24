/**
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */
$(function(){
    var set = $('.p24-payment-module a');
    var idRx = /payment_method=(\d+)/;
    var urlRx = /url\(['"]([^'"]+)['"]\)/;

    set.each(function (idx, elm) {
        var $elm = $(elm);
        var href = $elm.attr('href');
        var rxExecuted = idRx.exec(href);
        if (rxExecuted) {
            var id = rxExecuted[1];
            var $img = $elm.find('img');
            if ($img.length) {
                /* Extract URL of image from style. */
                $img.addClass('bank-logo-' + id);
                var longSrc = $img.css('background-image');
                rxExecuted = urlRx.exec(longSrc);
                if (rxExecuted) {
                    $img.attr('src', rxExecuted[1]);
                }
                $img.removeClass('bank-logo-' + id);
            } else {
                /* Set class with background image. */
                $elm.addClass('bank-logo-' + id);
            }
        }
    })
});
