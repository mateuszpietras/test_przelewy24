/**
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/* This code may be run before jQuery is loaded. */
addEventListener('DOMContentLoaded', function(e) {

    /* Display extracharge in order in back office. */

    /* The data is in dedicated box. */
    var $box = $('#przelewy24-admin-order-data');
    if ($box.length) {
        var $totalAmount = $('#total_order');
        var $templates = $totalAmount.siblings().first().find('td');
        var $tr = $('<tr>');

        var makeElm = function ($elm, $template, text) {
            if ($template.find('b').length) {
                var $bold = $('<b>');
                $bold.text(text);
                $bold.appendTo($elm);
            } else {
                $elm.text(text);
            }
            $elm.addClass($template.attr('class'));
            $elm.attr('align', $template.attr('align'));
            $elm.appendTo($tr);
        };

        var $leftTd = $('<td>');
        var leftText = $box.find('.extracharge_text').text();
        makeElm($leftTd, $($templates[0]), leftText);

        var $rightTd = $('<td>');
        var rightText = $box.find('.extracharge_amount').text();
        makeElm($rightTd, $($templates[1]), rightText);

        $totalAmount.before($tr);
    }
});
