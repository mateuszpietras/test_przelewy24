/**
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */
$(function() {
    $("#test_mode").change(
        function() {
            var o_Test_Transaction=$("#test-transaction");

            if($(this).val()==='1') {
                o_Test_Transaction.css("visibility","visible");
            } else {
                o_Test_Transaction.css("visibility","hidden");
            }
        }
    );
});

function przelewy24RefreshTab() {
    var currencyConfig
        = $('[data-type="p24subformconfig"][data-currency="'+$('[type="radio"][name="currency"]:checked').val()+'"]');
    var fieldsets = $('[id^="fieldset_"]');
    fieldsets.hide();
    $('[id^="fieldset_0"]').show();
    var subFormConfigs = $('[data-type="p24subformconfig"]');
    var minIndex = parseInt(currencyConfig.data('min'));
    var maxIndex = fieldsets.length;
    for (var subFormConfigIndex = 0; subFormConfigIndex < subFormConfigs.length; subFormConfigIndex++) {
        var checkedElement = $(subFormConfigs[subFormConfigIndex]);
        var checkedConfig = parseInt(checkedElement.data('min'));
        if (checkedConfig > minIndex && checkedConfig < maxIndex) {
            maxIndex = checkedConfig;
        }
    }
    for (var tabId = minIndex; tabId <= maxIndex-1; tabId++) {
        $('#fieldset_'+tabId+'_'+tabId+', #fieldset_'+tabId).show();
    }
}

function toggleExtraDiscountVisibility(currencySuffix) {
  var newValue = $('#P24_EXTRADISCOUNT_ENABLED'+currencySuffix).val();
  var amountBox = $('#P24_EXTRADISCOUNT_AMOUNT'+currencySuffix).parents('.form-group');
  var percentBox = $('#P24_EXTRADISCOUNT_PERCENT'+currencySuffix).parents('.form-group');
  var extraChargeElement = $('[name="P24_EXTRACHARGE_ENABLED'+currencySuffix+'"]');
  if ('1' === newValue) {
    amountBox.show();
    percentBox.hide();
      extraChargeElement.attr('disabled', 'disabled');
  } else if ('2' === newValue) {
    amountBox.hide();
    percentBox.show();
      extraChargeElement.attr('disabled', 'disabled');
  } else {
    amountBox.hide();
    percentBox.hide();
      extraChargeElement.removeAttr('disabled');
  }
}

function addFormGroupToField(fieldId, text) {
  var element = $(fieldId).parent();
  var inputGroup = $('<div class="input-group col-lg-1"></div>');
  inputGroup.prependTo(element);
  $(fieldId).appendTo(inputGroup);
  inputGroup.append('<span class="input-group-addon">'+text+'</span>');
}

$(document)
    .on('change', '[type="radio"][name="currency"]', function () {
        przelewy24RefreshTab();
    })

    .ready(function() {
        przelewy24RefreshTab();

        $('[name="currency"]').each(function(){
            var currency = $(this).val();
            var currencySuffix = ('PLN' === currency) ? '' : '_'+currency;
            var extraChargeName = 'P24_EXTRACHARGE_ENABLED'+currencySuffix;
            var extraDiscountName = 'P24_EXTRADISCOUNT_ENABLED'+currencySuffix;
            $(document).on('change', '[name="'+extraChargeName+'"]', function () {
                if ($('[name="'+extraChargeName+'"][value="1"]').is(':checked')) {
                    $('#'+extraDiscountName).val(0).attr('disabled', 'disabled').trigger('change');
                } else {
                    $('#'+extraDiscountName).removeAttr('disabled');
                }
            });
            $('[name="'+extraChargeName+'"]').trigger('change');


            $('#'+extraDiscountName)
                .change(function(){
                    toggleExtraDiscountVisibility(currencySuffix);
                })
                .trigger('change')
            ;

            addFormGroupToField('#P24_EXTRADISCOUNT_AMOUNT'+currencySuffix, currency);
            addFormGroupToField('#P24_EXTRACHARGE_AMOUNT'+currencySuffix, currency);
            addFormGroupToField('#P24_EXTRADISCOUNT_PERCENT'+currencySuffix, '%');
            addFormGroupToField('#P24_EXTRACHARGE_PERCENT'+currencySuffix, '%');
        });
    })
;

function setAutoOrder(sufix) {
    if(typeof sufix === 'undefined') {
        sufix = "";
    }
    var moduleLink = $('[data-type="p24config"]').data('modulelink');
    $.ajax(moduleLink, {
        method: 'POST', type: 'POST',
        data : { action: 'setOrder' },
        success : function(data) {
            if(sufix === undefined){
                sufix = "";
            }
            $('.paymethod .selected a.bank-box[data-sufix="suf'+sufix+'"],.paymethod .available a.bank-box[data-sufix="suf'+sufix+'"]').appendTo('.paymethod .tempHolder[data-id="suf'+sufix+'"]');

            var order = JSON.parse(data);
            var counter = 0;
            for (var i in order) {
                counter++;
                if (counter < 4) {
                    $('.paymethod .tempHolder[data-id="suf'+sufix+'"] a.bank-box[data-id=' + order[i] + ']').appendTo('.paymethod .selected[id=selected'+sufix+']');
                } else {
                    $('.paymethod .tempHolder[data-id="suf'+sufix+'"] a.bank-box[data-id=' + order[i] + ']').appendTo('.paymethod .available[id=available'+sufix+']');
                }
            }
            $('.paymethod .tempHolder[data-id="suf'+sufix+'"] a.bank-box[data-sufix="suf'+sufix+'"]').appendTo('.paymethod .available[id=available'+sufix+']');
            $('#clear').appendTo('.paymethod .available[id=available'+sufix+']');
            $('.paymethod .selected[id=selected'+sufix+'],.paymethod .available[id=available'+sufix+']')
                .css({ backgroundColor : "#FEFCD7" })
                .animate({ backgroundColor : "#fff" }, 2000)
            ;
            updatePaymethods(sufix);
        }
    });
}

function updatePaymethods(sufix) {
    if(typeof sufix === 'undefined') {
        sufix = "";
    }
    $('.bank-box.ui-unrotate-helper').removeClass('ui-unrotate-helper');
    var payMethodElement = $('.paymethod .selected[id=selected'+sufix+']');

    var paymentMethods = $('.paymethod .selected[id=selected'+sufix+'] > .bank-box');
    var maxNo = parseInt(payMethodElement.attr('data-max'));
    if (maxNo > 0 && paymentMethods.length > maxNo) {
        for (var payMethodIterator = paymentMethods.length - 1; payMethodIterator >= maxNo; payMethodIterator--) {
            $(paymentMethods[payMethodIterator]).prependTo($('.paymethod .available[id=available'+sufix+']'));
        }
    }

    payMethodElement.each(function() {
        var id = $(this).attr('data-id');
        var payMethodFirstElement = $('#P24_PAYMETHOD_FIRST'+id);
        payMethodFirstElement.val('');
        $(this).children().each(function(){
            payMethodFirstElement.val(payMethodFirstElement.val() + (payMethodFirstElement.val().length ?',':'') + $(this).attr('data-id'));
        });
    });

    $('.paymethod .available[id=available'+sufix+']').each(function() {
        var id = $(this).attr('data-id');
        var payMethodSecondElement = $('#P24_PAYMETHOD_SECOND'+id);
        payMethodSecondElement.val('');
        $(this).children().each(function(){
            if (typeof payMethodSecondElement.val() !== 'undefined') {
                payMethodSecondElement.val(payMethodSecondElement.val() + (payMethodSecondElement.val().length ?',':'') + $(this).attr('data-id'));
            }
        });
    });
}
$(document).ready(function () {
    $('[data-type="p24subformconfig"]').each(function () {
        var suffix = $(this).data('suffix');
        updatePaymethods(suffix);
        $('#P24_PAYMENT_METHOD_LIST' + suffix).change(function(){
            if ($(this).is(":checked")) {
                $('.paymethod:not(:visible)').slideDown(400, function(){ $('body').animate({ scrollTop: $(window).scrollTop() + $('.paymethod').height() })
                });
            } else {
                $('.paymethod:visible').slideUp();
            }
        });

        $.getScript("//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js",
            function() {
                $(".sortable.selected,.sortable.available").sortable({
                    connectWith: ".sortable.selected,.sortable.available",
                    placeholder: "bank-box bank-placeholder",
                    stop: function(){ updatePaymethods(suffix); },
                    revert: true,
                    start: function(){
                        window.setTimeout(function(){
                            $('.bank-box.ui-sortable-helper').on('mouseup', function(){
                                $(this).addClass('ui-unrotate-helper');
                            });
                        });
                    },
                }).disableSelection();

                $("#paymethod_promote_list")
                    .sortable({
                        stop: function() { updatePaymethodPromoted(suffix); },
                        axis: 'y',
                    })
                    .disableSelection()
                ;
            }
        );
        $('.paylistprom').change(function(){
            updatePaymethodPromoted(suffix);
        });
        $('#P24_PAYMETHOD_PROMOTE' + suffix).change(function(){
            if ($(this).is(":checked")) {
                $('#paymethod_promote_list'+suffix+':not(:visible)').slideDown(400, function(){ $('body').animate({ scrollTop: $('#paymethod_promote_list' + suffix).closest('fieldset').offset().top - $('.page-head').height() - 10 }) });
            } else {
                $('#paymethod_promote_list'+suffix+':visible').slideUp();
            }
        });
        var paymethodPromoted = $('#P24_PAYMETHOD_PROMOTED');
        if(paymethodPromoted.length && paymethodPromoted.val()){
            var paymethod_promoted = paymethodPromoted.val().split(',').reverse();
            if (paymethod_promoted.length > 0) {
                for (i in paymethod_promoted) {
                    $('.paylistprom_item_' + paymethod_promoted[i]).prependTo('#paymethod_promote_list');
                }
            }
        }

        function updatePaymethodPromoted(suffix) {
            var paymethod_promoted = [];
            $('.paylistprom[data-sufix=suf]'+suffix+':checked').each(function() {
                paymethod_promoted.push($(this).attr('data-val'));
            });
            $('#P24_PAYMETHOD_PROMOTED' + suffix).val(paymethod_promoted.join(','));
        }
    });
});
