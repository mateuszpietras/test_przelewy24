/**
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */
var configElement = $('[data-type="p24PaymentConfirmation"]');

function formSubmit(description) {
    var form = document.getElementById('przelewy24Form');

    if ($('#przelewy24FormRecuring input[name=p24_cc]').val() > 0) {
        form = document.getElementById('przelewy24FormRecuring');
    }
    if ($('#p24_regulation_accept').length) {
        $(form).find('input[name=p24_regulation_accept]').val($('#p24_regulation_accept').length ? 1 : 0);
    }
    if (typeof description !== 'undefined') {
        $('#orderDescription').val(description);
    }

    simpleFormSubmit();
}

function simpleFormSubmit() {
    var form = document.getElementById('przelewy24Form');
    if ($('#przelewy24FormRecuring').find('input[name=p24_cc]').val()) {
        form = document.getElementById('przelewy24FormRecuring');
    }
    if (!configElement.data('validationrequired')) {
        $.ajax(configElement.data('ajaxurl'), {
            method: 'POST', type: 'POST',
            data: {
                action: 'forgetCart'
            },
            error: function () {
                form.submit();
            },
            success: function (response) {
                form.submit();
            }
        });
    } else {
        form.submit();
    }
}

function proceedPayment() {
    if(configElement.data('validationrequired') === 2 && !configElement.data('orderid')) {
        $('#proceedPaymentLink').closest('a').addClass('disabled').attr('disabled', 1);
        $.ajax(configElement.data('ajaxurl'), {
            method: 'POST', type: 'POST',
            data: {
                action: 'makeOrder',
                cart_id: configElement.data('cartid')
            },
            error: function () {
                formSubmit();
            },
            success: function (response) {
                var data = JSON.parse(response);
                formSubmit(data.description);
            }
        });
    } else {
        formSubmit();
    }
}

if (configElement.data('acceptinshop')) {
    $(document).ready(function () {
        if ($('#p24_regulation_accept').length) {
            $('#p24_regulation_accept').click(function () {
                update_p24_regulation_accept_checkbox();
            });
            update_p24_regulation_accept_checkbox();
        }
    });

    function update_p24_regulation_accept_checkbox() {
        if ($('#p24_regulation_accept:checked').length) {
            $('input[name=p24_cc]').removeAttr('disabled');
            $('#przelewy24FormRecuring,#przelewy24Form').css('opacity', 1);
            if ($('input[name=p24_cc]:checked').length === 0) {
                $('input[name=p24_cc][value="0"]').click().trigger('change');
            }
            $('input[name=p24_cc]:checked:first').click().trigger('change');
            if ($('input[name=p24_cc][value=ajax_card]').length === 0) {
                $('#proceedPaymentLink').closest('a').fadeIn();
            }
            $('#P24_regulation_accept_text').css('display', 'none');
        } else {
            $('input[name=p24_cc]').attr('disabled', true);
            $('#przelewy24FormRecuring,#przelewy24Form').css('opacity', 0.4);
            $('#P24AjaxCardPaymentJs,#P24AjaxCardPaymentCss,#P24FormContainer').remove();
            $('#P24FormArea').hide();
            $('#proceedPaymentLink').closest('a').hide();
            $('#P24_regulation_accept_text').css('display', '');
        }
    }
}

function payInShopSuccess(status) {
    $.ajax(configElement.data('urlstatus'), {
        method: 'POST', type: 'POST',
        data: {
            action: 'trnVerify',
            p24_session_id: configElement.data('sessionid'),
            p24_order_id: status
        },
        error: function () {
            payInShopFailure();
        },
        success: function (response) {
            window.location.href = configElement.data('urlreturn');
        }
    });

}

function payInShopFailure() {
    $('#P24FormArea').html("<span class='info'>"+configElement.data('failureinfo')+"</span>");
    $('#P24FormArea:not(:visible)').slideDown();
    P24_Transaction = undefined;
    window.location =
        configElement.data('urlreturn')
        + (configElement.data('orderid') ? ('?order_id=' + configElement.data('orderid')) : '');
}
var payInShopScriptRequested = false;

function setP24method(method) {
    $('form#przelewy24Form input[name=p24_method]').val(parseInt(method) > 0 ? parseInt(method) : "");
}

function setP24recurringId(id) {
    $('form#przelewy24FormRecuring input[name=p24_cc]').val(parseInt(id) > 0 ? parseInt(id) : "");
}

function onResize() {
    if ($(window).width() <= 640) {
        $('.payMethodList').addClass('mobile');
    } else {
        $('.payMethodList').removeClass('mobile');
    }
}

$(document).ready(function () {
    $('head').append('<link rel="stylesheet" href="'+configElement.data('p24css')+'" type="text/css" />');

    $('input[name=p24_cc]:visible').change(function () {

        setP24recurringId($(this).val());

        if ($(this).val() === 'last_method') {
            $('#przelewy24lastmethod_img').removeClass('inactive');
            setP24method($(this).attr('data-method'));
        } else {
            $('#przelewy24lastmethod_img').addClass('inactive');
            setP24method("");
        }

        if (parseInt($(this).val()) === 0) {
            $('.payMethodList:not(:visible)').slideDown();
            setP24method("");
            if ($('.payMethodList .bank-box.selected').length) {
                setP24method($('.payMethodList .bank-box.selected:first').attr('data-id'));
            }
        } else {
            $('.payMethodList:visible').slideUp();
        }

        if (parseInt($(this).val()) > 0) {
            setP24method("");
        }

        if ($(this).val() === 'ajax_card') {
            setP24method("");
            $('#proceedPaymentLink').closest('a').fadeOut();
            if (typeof P24_Transaction === 'object') {
                $('#P24FormArea').slideDown();
            } else {
                requestJsAjaxCard();
            }
        } else {
            $('#P24FormArea').slideUp();
            if ($(this).is(':not(:disabled)')) {
                $('#proceedPaymentLink:not(:visible)').closest('a').fadeIn();
            }
        }
    });
    if ($('input[name=p24_cc]:checked').length === 0) {
        $('input[name=p24_cc]:visible:first').click();
    }
    $('input[name=p24_cc]:checked').trigger('change');

    $('.bank-box').click(function () {
        $('.bank-box').removeClass('selected').addClass('inactive');
        $(this).addClass('selected').removeClass('inactive');
        setP24method($(this).attr('data-id'));
        setP24recurringId($(this).attr('data-cc'));
    });
    onResize();
});

$(window).resize(function () {
    onResize();
});

function requestJsAjaxCard() {
    var orderId = configElement.data('orderid');
    var request = {
        'action': 'trnRegister',
        'p24_session_id': configElement.data('sessionid')
    };
    if (orderId) {
        request['order_id'] = orderId;
    }
    $.ajax(configElement.data('ajaxurl'), {
        method: 'POST', type: 'POST',
        data: request,
        error: function () {
            payInShopFailure();
        },
        success: function (response) {
            var data = JSON.parse(response);
            var dictionary = JSON.stringify(configElement.data('dictionary'));
            $('#P24FormArea').html("");
            $("<div></div>")
                .attr('id', 'P24FormContainer')
                .attr('data-sign', configElement.data('sign'))
                .attr('data-successCallback', 'payInShopSuccess')
                .attr('data-failureCallback', 'payInShopFailure')
                .attr('data-dictionary', dictionary)
                .addClass('loading')
                .appendTo('#P24FormArea')
                .parent().slideDown()
            ;
            if (document.createStyleSheet) {
                document.createStyleSheet(data.p24cssURL);
            } else {
                $('head').append('<link rel="stylesheet" type="text/css" href="' + data.p24cssURL + '" />');
            }
            if (!payInShopScriptRequested) {
                $.getScript(data.p24jsURL, function () {
                    P24_Transaction.init();
                    $('#P24FormContainer').removeClass('loading');
                    payInShopScriptRequested = false;
                    window.setTimeout(function () {
                        $('#P24FormContainer button').on('click', function () {
                            if (P24_Card.validate()) {
                                $(this).hide().after('<div class="loading"></div>');
                            }
                        });
                    }, 1000);
                });
            }
            payInShopScriptRequested = true;
        }
    });
}

function showPayJsPopup() {
    if (!$('#p24_regulation_accept').length || $('#p24_regulation_accept:checked').length) {
        setP24method("");
        $('#P24FormAreaHolder').appendTo('body');
        $('#proceedPaymentLink').closest('a').fadeOut();

        $('#P24FormAreaHolder').fadeIn();
        if (typeof P24_Transaction !== 'object') {
            requestJsAjaxCard();
        }
    }
}
function hidePayJsPopup() {
    $('#P24FormAreaHolder').fadeOut();
    $('#proceedPaymentLink:not(:visible)').closest('a').fadeIn();
}

function p24_regulation_accept_checked() {
    $('#przelewy24Form input[name=p24_regulation_accept]').val($('#p24_regulation_accept:checked').length);
    $('#przelewy24FormRecuring input[name=p24_regulation_accept]').val($('#p24_regulation_accept:checked').length);
}