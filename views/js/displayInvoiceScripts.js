/**
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */
$(document).ready(function(){

    var invoiceConfigurationElement = $('[data-type="p24invoiceconfiguration"]');
    if (invoiceConfigurationElement.length === 0) {
        return;
    }

    var ajaxUrl = invoiceConfigurationElement.data('url');
    var orderId = invoiceConfigurationElement.data('order');
    var customerPhone = invoiceConfigurationElement.data('phone');
    var confirmationText = invoiceConfigurationElement.data('confirm');
    $('ul#toolbar-nav').append(invoiceConfigurationElement.find('li'));
    invoiceConfigurationElement.remove();

    $('#page-header-p24-ivr').click(function(){
        if (!confirm(confirmationText + ' ' + customerPhone)) {
            return;
        }
        $.ajax(ajaxUrl, {
            method: 'POST', type: 'POST',
            data : { action: 'runIVR', id_order: orderId },
            success : function() {
                window.location.reload();
            }
        });
    });

});