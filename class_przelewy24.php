<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Communication protol version.
 *
 * @var float
 */
define('P24_VERSION', '3.2');
if (!class_exists('Przelewy24Class', false)) {
    /**
     * Class Przelewy24Class
     * Przelewy24 comunication class.
     */
    class Przelewy24Class
    {
        /**
         * Live system URL address.
         *
         * @var string
         */
        private static $hostLive = 'https://secure.przelewy24.pl/';

        /**
         * Sandbox system URL address.
         *
         * @var string
         */
        private static $hostSandbox = 'https://sandbox.przelewy24.pl/';

        /**
         * Use Live (false) or Sandbox (true) enviroment.
         *
         * @var bool
         */
        private $testMode = false;

        /**
         * Merchant Id.
         *
         * @var int
         */
        private $merchantId = 0;

        /**
         * Merchant posId.
         *
         * @var int
         */
        private $posId = 0;

        /**
         * Salt to create a control sum (from P24 panel).
         *
         * @var string
         */
        private $salt = '';

        /**
         * Array of POST data.
         *
         * @var array
         */
        private $postData = array();

        /**
         * Object constructor. Set initial parameters.
         *
         * @param int    $merchantId
         * @param int    $posId
         * @param string $salt
         * @param bool   $testMode
         */
        public function __construct($merchantId, $posId, $salt, $testMode = false)
        {
            $this->posId = (int) trim($posId);
            $this->merchantId = (int) trim($merchantId);
            if (0 === $this->merchantId) {
                $this->merchantId = trim($posId);
            }
            $this->salt = trim($salt);
            $this->testMode = $testMode;
            $this->addValue('p24_merchant_id', $this->merchantId);
            $this->addValue('p24_pos_id', $this->posId);
            $this->addValue('p24_api_version', P24_VERSION);

            return true;
        }

        /**
         * Returns host URL.
         *
         * @return string
         */
        public function getHost()
        {
            if ($this->testMode) {
                return self::$hostSandbox;
            }

            return self::$hostLive;
        }

        /**
         * Returns live host URL.
         *
         * @return string
         */
        public static function getLiveHost()
        {
            return self::$hostLive;
        }

        /**
         * Returns URL for direct request (trnDirect).
         *
         * @return string
         */
        public function trnDirectUrl()
        {
            return $this->getHost().'trnDirect';
        }

        /**
         * Add value do post request.
         *
         * @param string     $name  Argument name
         * @param string|int $value Argument value
         */
        public function addValue($name, $value)
        {
            if ($this->validateField($name, $value)) {
                $this->postData[$name] = $value;
            }
        }

        /**
         * Function is testing a connection with P24 server.
         *
         * @return array Array(INT Error, Array Data), where data is returned by api.
         *
         * @throws Exception
         */
        public function testConnection()
        {
            $crc = md5($this->posId.'|'.$this->salt);
            $arguments = array();
            $arguments['p24_pos_id'] = $this->posId;
            $arguments['p24_sign'] = $crc;

            return $this->callUrl('testConnection', $arguments);
        }

        /**
         * Prepare a transaction request.
         *
         * @param bool $redirect Set true to redirect to Przelewy24 after transaction registration
         *
         * @return array array(INT Error code, STRING Token)
         *
         * @throws Exception
         */
        public function trnRegister($redirect = false)
        {
            if ($missingFields = array_diff_key(
                array_flip(array('p24_session_id','p24_amount','p24_currency')),
                $this->postData
            )) {
                $msg = 'tnrRegister miss: '.join(', ', array_keys($missingFields));
                Przelewy24Logger::addLog(__METHOD__.' '.$msg, 1);

                return array('error' => 1, 'errorMessage' => $msg);
            }

            $crc = md5(
                $this->postData['p24_session_id'].'|'.$this->posId.'|'.
                $this->postData['p24_amount'].'|'.$this->postData['p24_currency'].'|'.$this->salt
            );
            $this->addValue('p24_sign', $crc);
            $result = $this->callUrl('trnRegister', $this->postData);
            if ('0' !== $result['error']) {
                return $result;
            }
            $token = $result['token'];
            if ($redirect) {
                $this->trnRequest($token);
            }

            return array('error' => 0, 'token' => $token, 'sign' => $crc);
        }

        /**
         * Redirects or returns URL to a P24 payment screen.
         *
         * @param string $token    Token
         * @param bool   $redirect If set to true redirects to P24 payment screen.
         *                         If set to false function returns URL to redirect to P24 payment screen
         *
         * @return string URL to P24 payment screen
         */
        public function trnRequest($token, $redirect = true)
        {
            $url = $this->getHost().'trnRequest/'.$token;
            if ($redirect) {
                Tools::redirect($url);
            }

            return $url;
        }

        /**
         * Function verifies response about transaction (received from P24 system).
         *
         * @throws Exception
         *
         * @return array
         */
        public function trnVerify()
        {
            $crc = md5(
                $this->postData['p24_session_id'].'|'.$this->postData['p24_order_id'].'|'.
                $this->postData['p24_amount'].'|'.$this->postData['p24_currency'].'|'.$this->salt
            );
            $this->addValue('p24_sign', $crc);

            return $this->callUrl('trnVerify', $this->postData);
        }

        /**
         * Method makes request to P24 system.
         *
         * @param string $function  Method name
         * @param array  $arguments POST parameters
         *
         * @return array array(INT Error code, ARRAY Result)
         *
         * @throws Exception
         */
        private function callUrl($function, $arguments)
        {
            if (!in_array($function, array('trnRegister', 'trnRequest', 'trnVerify', 'testConnection'))) {
                return array('error' => 201, 'errorMessage' => 'class:Method not exists');
            }
            if ('testConnection' !== $function) {
                $this->checkMandatoryFieldsForAction($arguments, $function);
            }

            $request = array();

            foreach ($arguments as $k => $v) {
                $request[] = $k.'='.urlencode($v);
            }
            $url = $this->getHost().$function;
            $user_agent = 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)';
            if (($ch = curl_init())) {
                if (count($request)) {
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, join('&', $request));
                }
                curl_setopt($ch, CURLOPT_URL, $url);
                if ($this->testMode) {
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                } else {
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                }
                curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                if (($result = curl_exec($ch))) {
                    $info = curl_getinfo($ch);
                    curl_close($ch);
                    if (200 !== $info['http_code']) {
                        return array('error' => 200, 'errorMessage' => 'call:Page load error ('.$info['http_code'].')');
                    }
                    $resultArray = array();
                    $exploded = explode('&', $result);
                    foreach ($exploded as $val) {
                        $valueParts = explode('=', $val);
                        $resultArray[trim($valueParts[0])] = urldecode(trim($valueParts[1]));
                    }

                    return $resultArray;
                }
                curl_close($ch);

                return array('error' => 203, 'errorMessage' => 'call:Curl exec error');
            }

            return array('error' => 202, 'errorMessage' => 'call:Curl init error');
        }

        /**
         * Validate version.
         *
         * @param string $version
         *
         * @return bool
         */
        private function validateVersion(&$version)
        {
            if (preg_match('/^[0-9]+(?:\.[0-9]+)*(?:[\.\-][0-9a-z]+)?$/', $version)) {
                return true;
            }
            $version = '';

            return false;
        }

        /**
         * Validate email.
         *
         * @param string $email
         *
         * @return bool
         */
        private function validateEmail(&$email)
        {
            if (($email = filter_var($email, FILTER_VALIDATE_EMAIL))) {
                return true;
            }
            $email = '';

            return false;
        }

        /**
         * Validate number.
         *
         * @param int      $value
         * @param int|null $min
         * @param int|null $max
         *
         * @return bool
         */
        private function validateNumber(&$value, $min = null, $max = null)
        {
            if (is_numeric($value)) {
                $value = (int) $value;
                if ((null !== $min && $value < $min) || (null !== $max && $value > $max)) {
                    return false;
                }
                return true;
            }
            $value = (null !== $min ? $min : 0);

            return false;
        }

        /**
         * Validate string.
         *
         * @param string $value
         * @param int    $len
         *
         * @return bool
         */
        private function validateString(&$value, $len = 0)
        {
            if (0 === $len ^ Tools::strlen($value) <= $len) {
                return true;
            }
            $value = '';

            return false;
        }

        /**
         * Validates url.
         *
         * @param string $url
         * @param int    $len
         *
         * @return bool
         */
        private function validateUrl(&$url, $len = 0)
        {
            if ((0 === $len ^ Tools::strlen($url) <= $len)
                && preg_match('@^https?://[^\s/$.?#].[^\s]*$@iS', $url)) {
                return true;
            }
            $url = '';

            return false;
        }

        /**
         * Validate enum.
         *
         * @param string $value
         * @param array $haystack
         *
         * @return bool
         */
        private function validateEnum(&$value, $haystack)
        {
            if (in_array(Tools::strtolower($value), $haystack)) {
                return true;
            }
            $value = $haystack[0];

            return false;
        }

        /**
         * Validates value of field.
         *
         * @param string     $field
         * @param string|int &$value
         *
         * @return bool
         */
        public function validateField($field, &$value)
        {
            $ret = false;
            switch ($field) {
                case 'p24_session_id':
                    $ret = $this->validateString($value, 100);
                    break;
                case 'p24_description':
                    $ret = $this->validateString($value, 1024);
                    break;
                case 'p24_address':
                    $ret = $this->validateString($value, 80);
                    break;
                case 'p24_country':
                case 'p24_language':
                    $ret = $this->validateString($value, 2);
                    break;
                case 'p24_client':
                case 'p24_city':
                    $ret = $this->validateString($value, 50);
                    break;
                case 'p24_merchant_id':
                case 'p24_pos_id':
                case 'p24_order_id':
                case 'p24_amount':
                case 'p24_method':
                case 'p24_time_limit':
                case 'p24_channel':
                case 'p24_shipping':
                    $ret = $this->validateNumber($value);
                    break;
                case 'p24_wait_for_result':
                    $ret = $this->validateNumber($value, 0, 1);
                    break;
                case 'p24_api_version':
                    $ret = $this->validateVersion($value);
                    break;
                case 'p24_sign':
                    if (32 === Tools::strlen($value) && ctype_xdigit($value)) {
                        $ret = true;
                    } else {
                        $value = '';
                    }
                    break;
                case 'p24_url_return':
                case 'p24_url_status':
                    $ret = $this->validateUrl($value, 250);
                    break;
                case 'p24_currency':
                    $ret = preg_match('/^[A-Z]{3}$/', $value);
                    if (!$ret) {
                        $value = '';
                    }
                    break;
                case 'p24_email':
                    $ret = $this->validateEmail($value);
                    break;
                case 'p24_encoding':
                    $ret = $this->validateEnum($value, array('iso-8859-2', 'windows-1250', 'urf-8', 'utf8'));
                    break;
                case 'p24_transfer_label':
                    $ret = $this->validateString($value, 20);
                    break;
                case 'p24_phone':
                    $ret = $this->validateString($value, 12);
                    break;
                case 'p24_zip':
                    $ret = $this->validateString($value, 10);
                    break;
                default:
                    if (0 === strpos($field, 'p24_quantity_')
                        || 0 === strpos($field, 'p24_price_')
                        || 0 === strpos($field, 'p24_number_')
                    ) {
                        $ret = $this->validateNumber($value);
                    } elseif (0 === strpos($field, 'p24_name_') ||
                        0 === strpos($field, 'p24_description_')
                    ) {
                        $ret = $this->validateString($value, 127);
                    } else {
                        $value = '';
                    }
                    break;
            }

            return $ret;
        }

        /**
         * Filter value.
         *
         * @param string $field
         * @param string $value
         *
         * @return bool|string
         */
        private function filterValue($field, $value)
        {
            return ($this->validateField($field, $value)) ? addslashes($value) : false;
        }

        /**
         * Check mandatory fields for action.
         *
         * @param array  $fieldsArray
         * @param string $action
         *
         * @throws Exception
         *
         * @return bool
         */
        public function checkMandatoryFieldsForAction($fieldsArray, $action)
        {
            $keys = array_keys($fieldsArray);
            $verification = ('trnVerify' === $action);
            static $mandatory = array('p24_order_id', //verify
                'p24_sign', 'p24_merchant_id', 'p24_pos_id', 'p24_api_version', 'p24_session_id', 'p24_amount', //all
                'p24_currency', 'p24_description', 'p24_country', //register/direct
                'p24_url_return', 'p24_currency', 'p24_email', //register/direct
            );
            for ($i = ($verification ? 0 : 1); $i < ($verification ? 4 : count($mandatory)); ++$i) {
                if (!in_array($mandatory[$i], $keys)) {
                    throw new Exception('Field '.$mandatory[$i].' is required for '.$action.' request!');
                }
            }

            return true;
        }

        /**
         * Parse and validate POST response data from Przelewy24.
         *
         * @return array|bool|null - valid response | false - invalid crc | null - not a Przelewy24 response
         */
        public function parseStatusResponse()
        {
            $sessionId = Tools::getValue('p24_session_id');
            $orderId = Tools::getValue('p24_order_id');
            $merchantId = Tools::getValue('p24_merchant_id');
            $posId = Tools::getValue('p24_pos_id');
            $amount = Tools::getValue('p24_amount');
            $currency = Tools::getValue('p24_currency');
            $method = Tools::getValue('p24_method');
            $sign = Tools::getValue('p24_sign');
            if (false !== $sessionId &&
                false !== $orderId &&
                false !== $merchantId &&
                false !== $posId &&
                false !== $amount &&
                false !== $currency &&
                false !== $method &&
                false !== $sign
            ) {
                $sessionId = $this->filterValue('p24_session_id', $sessionId);
                $merchantId = (int) $this->filterValue('p24_merchant_id', $merchantId);
                $posId = (int) $this->filterValue('p24_pos_id', $posId);
                $orderId = $this->filterValue('p24_order_id', $orderId);
                $amount = $this->filterValue('p24_amount', $amount);
                $currency = $this->filterValue('p24_currency', $currency);
                $method = $this->filterValue('p24_method', $method);
                $sign = $this->filterValue('p24_sign', $sign);
                if (($merchantId !== (int) $this->merchantId && $merchantId !== $this->posId)
                    || $posId !== (int) $this->posId
                    || md5($sessionId.'|'.$orderId.'|'.$amount.'|'.$currency.'|'.$this->salt) !== $sign
                ) {
                    return false;
                }

                return array(
                    'p24_session_id' => $sessionId,
                    'p24_order_id' => $orderId,
                    'p24_amount' => $amount,
                    'p24_currency' => $currency,
                    'p24_method' => $method,
                );
            }

            return null;
        }

        /**
         * Verifies response from przelewy24.
         *
         * @param array|null $data
         *
         * @return bool|null
         *
         * @throws Exception
         */
        public function trnVerifyEx($data = null)
        {
            if (!$parseStatusResponse = $this->parseStatusResponse()) {
                return $parseStatusResponse;
            }
            if (null !== $data && is_array($data)) {
                foreach ($data as $field => $value) {
                    $value = $this->filterValue($field, $value);
                    if ($parseStatusResponse[$field] !== $value) {
                        return false;
                    }
                }
            }
            $this->postData = array_merge($this->postData, $parseStatusResponse);
            $result = $this->trnVerify();

            return 0 === (int) $result['error'];
        }

        /**
         * Return direct sign for p24.
         *
         * @param array $data
         *
         * @return string
         */
        public function trnDirectSign($data)
        {
            return md5(
                $data['p24_session_id'].'|'.$this->posId.'|'.
                $data['p24_amount'].'|'.$data['p24_currency'].'|'.$this->salt
            );
        }
    }
}
