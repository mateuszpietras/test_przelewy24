<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class LastMethodModel.
 */
class LastMethodModel extends ObjectModel
{
    /**
     * Id of customer. Used to connect last payment method with PrestaShop customer
     * (stored in [prefix]_customer table) using its id ([prefix]_customer table.id).
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $customer_id;

    /**
     * Id of last used payment method chosen by customer. List of allowed payment methods list can be accessed
     * by Przelewy24 API.
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $p24_method;

    // Table columns:
    const CUSTOMER_ID = 'customer_id';
    const P24_METHOD = 'p24_method';

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'przelewy24_lastmethod',
        'primary' => 'customer_id',
        'multilang' => false,
        'fields' => array(
            self::CUSTOMER_ID => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            self::P24_METHOD => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        ),
    );

    /**
     * Try create table for this model.
     *
     * @return bool
     */
    public static function tryCreateTable()
    {
        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS`'._DB_PREFIX_.'przelewy24_lastmethod` (
            `customer_id` INT NOT NULL PRIMARY KEY,
            `p24_method` INT NOT NULL
            );');

        return true;
    }
}
