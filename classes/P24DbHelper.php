<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class CustomerSettingsModel. The class provide all advanced accessed to database that cannot be put into models.
 */
class P24DbHelper
{
    /**
     * Add order state.
     *
     * @param string $title
     * @param int    $number
     * @param string $color
     * @param int    $payed
     * @param int    $invoice
     * @param string $template
     */
    public static function addOrderState($title, $number, $color, $payed = 0, $invoice = 0, $template = '')
    {
        $sql = new DbQueryCore();
        $sql->select('id_lang');
        $sql->from('lang');

        $langTable = Db::getInstance()->ExecuteS($sql);

        foreach ($langTable as $langItem) {
            $langId = (int) $langItem['id_lang'];

            $sql = new DbQueryCore();
            $sql->select('id_order_state');
            $sql->from('order_state_lang');
            $sql->where("name = '".pSQL($title)."'");
            $orderStateExists = Db::getInstance()->getRow($sql);

            $sql = new DbQueryCore();
            $sql->select('id_order_state');
            $sql->from('order_state_lang');
            $sql->where("name = '".pSQL($title)."'");
            $sql->where('id_lang = '.$langId);

            $rq = Db::getInstance()->getRow($sql);

            if ($rq && isset($rq['id_order_state']) && (int) $rq['id_order_state'] > 0) {
                Configuration::updateValue('P24_ORDER_STATE_'.$number, (int) $rq['id_order_state']);
            } else {
                if (!$orderStateExists) {
                    Db::getInstance()->insert('order_state', array(
                        'unremovable' => 1,
                        'color' => pSQL($color),
                        'paid' => pSQL($payed),
                        'invoice' => pSQL($invoice),
                    ));

                    $stateid = Db::getInstance()->Insert_ID();
                }
                Db::getInstance()->insert('order_state_lang', array(
                    'id_order_state' => (int) $stateid,
                    'id_lang' => $langId,
                    'name' => pSQL($title),
                    'template' => pSQL($template),
                ));

                Configuration::updateValue('P24_ORDER_STATE_'.$number, $stateid);
            }
        }
    }

    /**
     * Get module parameters (with fixed limit).
     *
     * @param int $limit
     *
     * @return array
     */
    public static function getParameters($limit = 100)
    {
        $queryBuilder = new DbQuery();
        $queryBuilder
            ->select('name')
            ->from('configuration')
            ->where("name LIKE 'P24#_%' ESCAPE '#'")
            ->limit($limit)
        ;

        return Db::getInstance()->executeS($queryBuilder);
    }
}
