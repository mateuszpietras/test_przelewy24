<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class OrderDetailsModel.
 */
class OrderDetailsModel extends ObjectModel
{
    /**
     * Index of przelewy24_amount (automatically incremented)
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $i_id;

    /**
     * Id of PreStashop cart id (stored in [prefix]_cart table)
     *
     * The name is misleading due to historical reasons. Changing it would be too dangerous
     * or would break backward compatibility.
     *
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $i_id_order;

    /**
     * Session id
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var string
     */
    public $s_sid;

    /**
     * Payment method. List of allowed payment methods list can be accessed by Przelewy24 API.
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $p24_method;

    /**
     * Total order amount. For PLN currency stored in Grosze (1 Grosz is 0.01 PLN)
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $i_amount;

    // Table columns:
    const P24_METHOD = 'p24_method';
    const ID = 'i_id';
    const ORDER_ID = 'i_id_order';
    const AMOUNT = 'i_amount';
    const SESSION_ID = 's_sid';

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'przelewy24_amount',
        'primary' => 'i_id',
        'multilang' => false,
        'fields' => array(
            self::ID => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            self::ORDER_ID => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            self::SESSION_ID => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 32),
            self::P24_METHOD => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            self::AMOUNT => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        ),
    );

    /**
     * Get most used payment methods.
     *
     * @return array
     */
    public static function getMostUsedPaymentMethods()
    {
        $sql = new DbQueryCore();
        $sql->select(self::P24_METHOD.', count(*) as count');
        $sql->from(self::$definition['table']);
        $sql->where(self::P24_METHOD.' > 0');
        $sql->groupBy(self::P24_METHOD);
        $sql->orderBy('count DESC');

        return Db::getInstance()->executeS($sql);
    }

    /**
     * Find by session id.
     *
     * @param int $sessionId
     *
     * @return array
     */
    public static function findBySessionId($sessionId)
    {
        $sql = new DbQueryCore();
        $sql->select('*');
        $sql->from(self::$definition['table']);
        $sql->where(self::SESSION_ID." = '".pSQL($sessionId)."'");

        return Db::getInstance()->getRow($sql);
    }

    /**
     * Construct by sid.
     *
     * @param int $sid
     *
     * @return null|OrderDetailsModel
     *
     * @throws PrestaShopException
     */
    public static function constructBySid($sid)
    {
        $sid = pSQL($sid);
        $sql = new DbQuery();
        $sql
            ->select(self::$definition['primary'])
            ->from('przelewy24_amount')
            ->where("s_sid='$sid'")
        ;
        $id = Db::getInstance()->getValue($sql);

        return $id ? new self($id) : null;
    }

    /**
     * Find by order id.
     *
     * @param int $orderId
     *
     * @return array
     */
    public static function findByOrderId($orderId)
    {
        if (_PS_VERSION_ < 1.6) {
            $sql = new DbQueryCore();
            $sql->select('*');
            $sql->from(OrderDetailsModel::$definition['table']);
            $sql->where(OrderDetailsModel::ORDER_ID." = '".bqSQL($orderId)."'");

            $row = Db::getInstance()->getRow($sql);
            return $row && isset($row[OrderDetailsModel::ID]) ?
                new OrderDetailsModel($row[OrderDetailsModel::ID]) : null;
        }
        $amounts = new PrestaShopCollection('OrderDetailsModel');
        $amounts->where(OrderDetailsModel::ORDER_ID, '=', $orderId);

        return $amounts->getFirst();
    }


    /**
     * Get refund data from db.
     *
     * @param int $orderId
     *
     * @return array|bool
     */
    public static function getRefundData($orderId)
    {
        $orderId = (int) $orderId;
        $sql = new DbQuery();
        $sql
            ->select('o.id_order as orderId')
            ->select('o.id_cart as cartId')
            ->select('pa.s_sid')
            ->select("concat(o.id_cart,'|',pa.s_sid) as sessionId")
            ->select('pa.i_amount as amount')
            ->select('op.transaction_id as p24_OrderId')
            ->select('op.id_order_payment as orderPaymentId')
            ->from('przelewy24_amount', 'pa')
            ->innerJoin('orders', 'o', 'pa.i_id_order = o.id_cart')
            ->innerJoin('order_payment', 'op', 'o.reference = op.order_reference')
            ->where("o.id_order = $orderId")
            ->where('pa.p24_method is not null')
        ;
        $results = DB::getInstance()->executeS($sql);
        if ($results) {
            $result = reset($results);
            if ($result) {
                return array(
                    'sessionId' => $result['sessionId'],
                    'amount' => $result['amount'],
                    'p24OrderId' => $result['p24_OrderId'],
                );
            }
        }

        return false;
    }

    /**
     * Try create table for this model.
     *
     * @return bool
     */
    public static function tryCreateTable()
    {
        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS`'._DB_PREFIX_.'przelewy24_amount` (
            `i_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
            `s_sid`	char(32) NOT NULL,
            `i_id_order` INT UNSIGNED NOT NULL ,
            `i_amount` INT UNSIGNED NOT NULL
            );');

        /* backwards compatibility */
        $hasFieldP24Method = Db::getInstance()->getRow("SELECT EXISTS(
            SELECT * FROM information_schema.columns
            WHERE table_schema = database()
            AND COLUMN_NAME = 'p24_method'
            AND table_name = '"._DB_PREFIX_."przelewy24_amount') AS table_exists");
        if (!$hasFieldP24Method['table_exists']) {
            Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'przelewy24_amount ADD COLUMN p24_method INT;');
        }

        return true;
    }
}
