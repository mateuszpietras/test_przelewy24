<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Interface Przelewy24Consts.
 */
interface Przelewy24Consts
{
    const CONFIRMATION_EMAIL_HTML_KEY = 'template_html';
    const EMAIL_PARAMS_TEMPLATE_NAME = 'template';
    const EMAIL_PARAMS_EXTRA_VARS = 'extra_template_vars';
    const EMAIL_PARAMS_STD_VARS = 'template_vars';
    const EMAIL_TEMPLATE_NAME_ORDER = 'order_conf';
    const EMAIL_TEMPLATE_VAR_EXTRACHARGE = '{p24_extracharge}';
    const EMAIL_TEMPLATE_VAR_ORDER_NAME = '{order_name}';
}
