<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24Autoloader
 */
class Przelewy24Autoloader
{
    /**
     * Register autoloader.
     */
    public function register()
    {
        spl_autoload_register(array($this, 'getLoader'));
    }

    /**
     * Get directories where autoloaded files are stored.
     * Preg filter will append self::getRootDirectory prefix to all elements in an array.
     *
     * @return string[]
     */
    public static function getMainDirectories()
    {
        return preg_filter('/^/', self::getRootDirectory(), array(
            'classes',
            'helper',
            'shared-libraries',
            'controllers',
        ));
    }

    /**
     * Returns przelewy24 root module directory.
     *
     * @return string
     */
    public static function getRootDirectory()
    {
        return _PS_MODULE_DIR_.DIRECTORY_SEPARATOR.'przelewy24'.DIRECTORY_SEPARATOR;
    }

    /**
     * Get paths for file loading.
     *
     * @return array
     */
    public static function getPaths()
    {
        $pathsToCheck = array();

        foreach (self::getMainDirectories() as $directory) {
            /**
             * Preg filter appends parent directory path prefix to all subdirectories names.
             */
            $pathsToCheck = array_merge(
                $pathsToCheck,
                array($directory), // Adding current directory to paths.
                array_filter(glob($directory.'/*'), 'is_dir')
            );
        }

        return $pathsToCheck;
    }

    /**
     * Przelewy24 autoload function.
     *
     * @param string $className
     */
    public function getLoader($className)
    {
        foreach (self::getPaths() as $path) {
            $filename = $path.DIRECTORY_SEPARATOR.$className.'.php';
            if (file_exists($filename)) {
                require_once $filename;
                break;
            }
        }
    }
}

$p24Autoloader = new Przelewy24Autoloader();
$p24Autoloader->register();
