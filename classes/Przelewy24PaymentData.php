<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

class Przelewy24PaymentData
{
    /**
     * @var Cart
     */
    private $cart;

    /**
     * Cached order for the cart.
     *
     * This variable should be used by dedicated setter.
     * It is lazy loaded.
     *
     * @var array|null
     */
    private $firstOrder;

    /**
     * Cached currency for the cart.
     *
     * This variable should be used by dedicated setter.
     * It is lazy loaded.
     *
     * @var null|Currency
     */
    private $currency;

    /**
     * Przelewy24PaymentData constructor.
     *
     * @param Cart $cart
     */
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    /**
     * Get cart.
     *
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Check if cart is in database.
     *
     * @return bool
     */
    public function isCartInDb()
    {
        return (bool)$this->cart->id;
    }

    /**
     * Check if order match module.
     *
     * @param string $moduleName
     * @return bool
     */
    public function isModuleOfOrderMatched($moduleName)
    {
        $order = $this->getFirstOrder();
        if ($order) {
            return $order['module'] === $moduleName;
        } else {
            return false;
        }
    }

    /**
     * Check if order has already been placed
     *
     * @return bool result
     */
    public function orderExists()
    {
        $sql = 'SELECT count(*) FROM `' . _DB_PREFIX_ . 'orders` WHERE `id_cart` = ' . (int)$this->cart->id;
        $result = (bool)Db::getInstance()->getValue($sql);
        return $result;
    }


    /**
     * Get the first order bind to the selected cart.
     *
     * @return null|array Order details
     */
    private function getFirstOrder()
    {
        if (!$this->firstOrder) {
            $sql = 'SELECT *
                FROM `' . _DB_PREFIX_ . 'orders`
                WHERE `id_cart` = ' . (int)$this->cart->id
                . Shop::addSqlRestriction()
                . ' ORDER BY `id_order` ASC';
            $data = Db::getInstance()->getRow($sql);
            $this->firstOrder = $data ?: null;
        }
        return $this->firstOrder;
    }

    /**
     * Get order id.
     *
     * @return null|int
     */
    public function getFirstOrderId()
    {
        $order = $this->getFirstOrder();
        if ($order && $order['id_order']) {
            return (int)$order['id_order'];
        } else {
            return null;
        }
    }

    /**
     * The reference is the same for all orders from the single cart.
     *
     * @return null|string
     */
    public function getOrderReference()
    {
        $order = $this->getFirstOrder();
        if ($order && $order['reference']) {
            return $order['reference'];
        } else {
            return null;
        }
    }

    /**
     * Get all orders for the single cart.
     *
     * Returns null if there are no orders.
     *
     * @return array
     */
    public function getAllOrders()
    {
        if (_PS_VERSION_ < 1.6) {
            return $this->getAllOrdersByRawSql();
        }
        $orders = new PrestaShopCollection('Order');
        $orders->where('id_cart', '=', $this->cart->id);

        return $orders->getAll();
    }

    /**
     * Returns all order by raw sql - used by PrestaShop without PrestaShopCollection support.
     *
     * @return array
     */
    private function getAllOrdersByRawSql()
    {
        $sql = new DbQueryCore();
        $sql->select(Order::$definition['primary']);
        $sql->from(Order::$definition['table']);
        $sql->where('id_cart = '.bqSQL($this->cart->id));
        $orders = array();
        foreach (Db::getInstance()->executeS($sql) as $row) {
            $orders[] = new Order($row[Order::$definition['primary']]);
        }

        return $orders;
    }

    /**
     * Get payments.
     *
     * @return null|PrestaShopCollection
     */
    public function getPayments()
    {
        /* Any order from the same cart is fine. */
        $orderId = $this->getFirstOrderId();
        if ($orderId) {
            $order = new Order($orderId);
            $payments = $order->getOrderPaymentCollection();
        } else {
            $payments = null;
        }
        return $payments;
    }

    /**
     * Return currency for the cart.
     *
     * @return Currency
     */
    public function getCurrency()
    {
        if (!$this->currency) {
            $this->currency = new Currency($this->cart->id_currency);
        }
        return $this->currency;
    }

    /**
     * Get extracharge from database.
     *
     * @return null|Extracharge
     */
    public function getExtrachargeFromDatabase()
    {
        if ($this->orderExists()) {
            /* For now bind it to the first order in group. */
            return Extracharge::getExtrachargeByOrderId($this->getFirstOrderId());
        } else {
            return null;
        }
    }

    /**
     * Get p24 rounded amount.
     *
     * @param float $amount
     * @return int
     */
    public function formatAmount($amount)
    {
        $currency = $this->getCurrency();
        if ('0' === (string)$currency->decimals && Configuration::hasKey('PS_PRICE_ROUND_MODE')) {
            switch (Configuration::get('PS_PRICE_ROUND_MODE')) {
                case 0:
                    $amount = ceil($amount);
                    break;
                case 1:
                    $amount = floor($amount);
                    break;
                case 2:
                    $amount = round($amount);
                    break;
                /* No default. If the parameter is different, do not round to naturals. */
            }
        }

        return (int)round($amount * 100);
    }

    /**
     * Get extra charge amount.
     *
     * @return float
     */
    public function computeExtrachargeAmount()
    {
        $currency = $this->getCurrency();
        $currencySuffix = ('PLN' === $currency->iso_code) ? '' : '_' . $currency->iso_code;
        $amount = $this->cart->getOrderTotal(true, Cart::BOTH);
        $amount = $this->formatAmount($amount);

        return Przelewy24::getExtracharge($amount, $currencySuffix);
    }

    /**
     * Add extra charge.
     *
     * @return bool
     */
    public function addExtracharge()
    {
        if ($this->getExtrachargeFromDatabase()) {
            return false;
        }

        /* This is amount for all orders from the cart. */
        $extrachargeAmount = $this->computeExtrachargeAmount();

        if ($extrachargeAmount) {
            $orderId = $this->getFirstOrderId();
            $order = new Order($orderId);

            $order->total_paid += ($extrachargeAmount / 100);
            $order->total_paid_tax_incl += ($extrachargeAmount / 100);
            $order->total_paid_tax_excl += ($extrachargeAmount / 100);

            try {
                $order->update();

                $extracharge = new Extracharge();
                $extracharge->id_order = (int)$order->id;
                $extracharge->extracharge_amount = ($extrachargeAmount / 100);
                $extracharge->add();

                return true;
            } catch (Exception $e) {
                Przelewy24Logger::addLog(__CLASS__ . ' ' . __METHOD__ . ' ' . $e->getMessage(), 1);
            }
        }

        return false;
    }

    /**
     * Return total paid with extracharge.
     *
     * @return float
     */
    public function getTotalAmountWithExtraCharge()
    {
        if ($this->orderExists()) {
            /* Extracharge should be included. */
            $orders = $this->getAllOrders();
            $totalPaid = 0.0;
            foreach ($orders as $order) {
                $totalPaid = round($totalPaid + $order->total_paid, 2);
            }
        } else {
            $extrachargeAmount = $this->computeExtrachargeAmount() / 100;
            $cartTotal = $this->cart->getOrderTotal(true, Cart::BOTH);
            $totalPaid = round($extrachargeAmount + $cartTotal, 2);
        }
        return $totalPaid;
    }

    /**
     * Return shipping costs.
     *
     * @return float
     */
    public function getShippingCosts()
    {
        if ($this->orderExists()) {
            $orders = $this->getAllOrders();
            $shippingCosts = 0.0;
            foreach ($orders as $order) {
                $shippingCosts = round($shippingCosts + $order->total_shipping_tax_incl, 2);
            }
        } else {
            $shippingCostsRaw = $this->cart->getPackageShippingCost((int)$this->cart->id_carrier);
            $shippingCosts = round($shippingCostsRaw, 2);
        }
        return $shippingCosts;
    }

    /**
     * Get sum of discounts.
     *
     * @return float
     */
    public function getDiscounts()
    {
        if ($this->orderExists()) {
            $orders = $this->getAllOrders();
            $totalDiscounts = 0.0;
            foreach ($orders as $order) {
                $totalDiscounts = round($totalDiscounts + $order->total_discounts_tax_incl, 2);
            }
        } else {
            $totalDiscountsRaw = $this->cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS);
            $totalDiscounts = round($totalDiscountsRaw, 2);
        }
        return $totalDiscounts;
    }

    /**
     * Get products.
     *
     * @return array
     */
    public function getProducts()
    {
        if ($this->orderExists()) {
            $orders = $this->getAllOrders();
            $products = array();
            foreach ($orders as $order) {
                $products = $products + $order->getProducts();
            }
        } else {
            $products = $this->cart->getProducts();
        }
        return $products;
    }

    /**
     * Try to get extracharge amount for order in question.
     *
     * Return 0 if order is from different cart.
     *
     * @param $reference
     * @return float
     */
    public function getExtrachargeForOrderReference($reference)
    {
        /* This regular expression mach first order of the set, or the single one. */
        $rex = '/^([^\\#]+)(\\#1)?$/';
        if (preg_match($rex, $reference, $m)) {
            $order = $this->getFirstOrder();
            if ($order['reference'] === $m[1]) {
                $extraCharge = $this->getExtrachargeFromDatabase();
                return $extraCharge->extracharge_amount;
            }
        }
        return 0.0;
    }

    /**
     * Fixt extracharge in temporal order representations.
     *
     * In few steps there are temporal order objects without extracharge.
     * This method fix them.
     *
     * @param Order $tmpOrder
     */
    public function fixExtrachargeInTmpOrder(Order $tmpOrder)
    {
        $orderId = (int)$tmpOrder->id;
        if (!$orderId) {
            throw new LogicException("Cannot fix orders without id.");
        }
        $order = new Order($orderId);
        $tmpOrder->total_paid = $order->total_paid;
        $tmpOrder->total_paid_tax_incl = $order->total_paid_tax_incl;
        $tmpOrder->total_paid_tax_excl = $order->total_paid_tax_excl;
    }
}
