<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Extracharge.
 */
class Extracharge extends ObjectModel
{
    /**
     * Autoincremented index of table przelewy24_extracharges.
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $id_extracharge;

    /**
     * Id of PrestaShop order (stored in table [prefix]_orders).
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $id_order;

    /**
     * Amount of extra charge.
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var float
     */
    public $extracharge_amount;


    // Table columns:
    const ID = 'id_extracharge';
    const ORDER_ID = 'id_order';
    const AMOUNT = 'extracharge_amount';

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'przelewy24_extracharges',
        'primary' => 'id_extracharge',
        'multilang' => false,
        'fields' => array(
            self::ID => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            self::ORDER_ID => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            self::AMOUNT => array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
        ),
    );

    /**
     * Get extracharge by order id.
     *
     * @param int $idOrder
     *
     * @return bool|Extracharge
     */
    public static function getExtrachargeByOrderId($idOrder)
    {
        $sql = new DbQueryCore();
        $sql->select(self::ID);
        $sql->from(Extracharge::$definition['table']);
        $sql->where(self::ORDER_ID . ' = ' . (int)$idOrder);
        $extracharge = Db::getInstance()->getRow($sql);

        if (!$extracharge || !isset($extracharge[self::ID])) {
            return false;
        }

        return new Extracharge((int)$extracharge[self::ID]);
    }

    public function getExtracharge()
    {
        return (float)$this->extracharge_amount;
    }


    /**
     * Try create table for this model.
     *
     * @return bool
     */
    public static function tryCreateTable()
    {
        Db::getInstance()->Execute(
            'CREATE TABLE IF NOT EXISTS`' . _DB_PREFIX_ . 'przelewy24_extracharges` (
            `id_extracharge` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
            `id_order` INT NOT NULL,
            `extracharge_amount` DECIMAL(20,6) NOT NULL
            );'
        );

        return true;
    }
}
