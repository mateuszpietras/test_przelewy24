<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

require_once 'Przelewy24Autoloader.php';

/**
 * Class Przelewy24Soap.
 */
class Przelewy24SoapCardService extends Przelewy24SoapAbstract
{
    const SOAP_SERVICE_DEFAULT = 1;
    const SOAP_SERVICE_CHARGE_CARD = 2;

    /**
     * Soap client.
     *
     * @var SoapClient
     */
    protected $soapClient = null;

    /**
     * Suffix for selected currency. Empty for PLN.
     *
     * @var string
     */
    protected $currencySuffix = '';

    public function __construct($currencySuffix = '')
    {
        if (!$this->isSoapExtensionInstalled()) {
            Przelewy24Logger::addLog(__METHOD__.' '.self::ERROR_EXTENSION_NOT_INSTALLED, 1);
            throw new RuntimeException(self::ERROR_EXTENSION_NOT_INSTALLED);
        }

        $this->currencySuffix = $currencySuffix;

        $url = $this->getWsdlCC();

        try {
            $this->soapClient = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE, 'exceptions' => true));
        } catch (SoapFault $e) {
            Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);

            throw new RuntimeException(self::ERROR_UNABLE_CONNECTION);
        } catch (Exception $e) {
            Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);

            throw new RuntimeException(self::ERROR_UNKNOWN_SOAP);
        }
    }

    /**
     * Check credit card recurrence.
     *
     * @return bool
     */
    public function ccCheckRecurrency()
    {
        try {
            $res = $this->soapClient->checkRecurrency(
                Configuration::get(P24Configuration::P24_SHOP_ID.$this->currencySuffix),
                Configuration::get(P24Configuration::P24_SALT.$this->currencySuffix)
            );

            return (bool)$res;
        } catch (Exception $e) {
            Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);

            return false;
        }
    }

    /**
     * Get transaction reference.
     *
     * @param $currencySuffix
     * @param $oid
     * @return object
     */
    public function getTransactionReference($currencySuffix, $oid)
    {
        return $this->soapClient->GetTransactionReference(
            Configuration::get(P24Configuration::P24_SHOP_ID.$currencySuffix),
            Configuration::get(P24Configuration::P24_API_KEY.$currencySuffix),
            $oid
        );
    }

    /**
     * Check card reference id.
     *
     * @param $currencySuffix
     * @param $refId
     * @return object
     */
    public function checkCard($currencySuffix, $refId)
    {
        return $this->soapClient->CheckCard(
            Configuration::get(P24Configuration::P24_SHOP_ID.$currencySuffix),
            Configuration::get(P24Configuration::P24_API_KEY.$currencySuffix),
            $refId
        );
    }

    /**
     * Charger card.
     *
     * @param $currencySuffix
     * @param $ref
     * @param $amount
     * @param $currency
     * @return object
     */
    public function chargeCard($currencySuffix, $ref, $amount, $currency)
    {
        return $this->soapClient->ChargeCard(
            Configuration::get(P24Configuration::P24_SHOP_ID.$currencySuffix),
            Configuration::get(P24Configuration::P24_API_KEY.$currencySuffix),
            $ref,
            $amount,
            $currency,
            Tools::getValue('p24_email'),
            Tools::getValue('p24_session_id'),
            Tools::getValue('p24_client'),
            Tools::getValue('p24_description')
        );
    }

    /**
     * Get wsdl credit card service.
     *
     * @return string
     */
    private function getWsdlCC()
    {
        $mode = (Configuration::get(P24Configuration::P24_TEST_MODE)) ? 'sandbox' : 'secure';
        $url = 'https://'.$mode.'.przelewy24.pl/external/wsdl/charge_card_service.php?wsdl';

        return $url;
    }

    /**
     * Is SOAP extension loaded.
     *
     * @return bool
     */
    private function isSoapExtensionInstalled()
    {
        return extension_loaded('soap');
    }
}
