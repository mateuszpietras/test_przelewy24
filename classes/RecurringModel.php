<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class RecurringModel.
 */
class RecurringModel extends ObjectModel
{
    /**
     * Automatically incremented id.
     *
     * @var int
     */
    public $id;

    /**
     * Shop id. Used to connect credit card with PrestaShop shop ([prefix]_shop).
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $website_id;

    /**
     * Id of customer. Used to connect credit card with PrestaShop customer ([prefix]_customer).
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $customer_id;

    /**
     * Reference id of this credit card. Used in communication with Przelewy24 API to identify this credit card.
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $reference_id;

    /**
     * Date of card expiration in format MMYY (where MM is number of month, starting with 1, with leading 0
     * and YY are 2 last digits of card expiration year).
     *
     * @var string
     */
    public $expires;

    /**
     * Card mask - will be displayed in place of true card number.
     *
     * @var string
     */
    public $mask;

    /**
     * Type of card (for example: visa, mastercard)
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var string
     */
    public $card_type;

    /**
     * Date of remembering this card.
     *
     * @var DateTime
     */
    public $timestamp;

    // Table columns:
    const ID = 'id';
    const WEBSITE_ID = 'website_id';
    const CUSTOMER_ID = 'customer_id';
    const REFERENCE_ID = 'reference_id';
    const EXPIRES = 'expires';
    const MASK = 'mask';
    const CARD_TYPE = 'card_type';
    const TIMESTAMP = 'timestamp';

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'p24_recuring',
        'primary' => 'id',
        'multilang' => false,
        'fields' => array(
            self::ID => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            self::WEBSITE_ID => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            self::CUSTOMER_ID => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            self::REFERENCE_ID => array(
                'type' => self::TYPE_STRING,
                'validate' => 'isString',
                'size' => 35,
            ),
            self::EXPIRES => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 4),
            self::MASK => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 32),
            self::CARD_TYPE => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 20),
            self::TIMESTAMP => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        ),
    );

    /**
     * Load by customer id.
     *
     * @param int $customerId
     *
     * @return bool|RecurringModel
     */
    public static function loadByCustomerId($customerId)
    {
        $sql = new DbQueryCore();
        $sql->select(self::ID);
        $sql->from(self::$definition['table']);
        $sql->where(self::CUSTOMER_ID.' = '.(int) $customerId);

        $row = Db::getInstance()->getRow($sql);

        if (!$row || !isset($row[self::ID])) {
            return false;
        }

        return new RecurringModel((int) $row[self::ID]);
    }

    /**
     * Creates unique entry for credit card.
     *
     * @param string $mask
     * @param string $cardType
     * @param string $expires
     * @param int $customerId
     * @param string $referenceId
     * @param int $websiteId
     *
     * @return RecurringModel
     */
    public static function constructByUnique($mask, $cardType, $expires, $customerId, $referenceId, $websiteId)
    {
        $sql = new DbQueryCore();
        $sql
            ->select(self::$definition['primary'])
            ->from(self::$definition['table'])
            ->where(self::MASK." = '". pSQL($mask)."'")
            ->where(self::CARD_TYPE." = '". pSQL($cardType)."'")
            ->where(self::EXPIRES." = '".pSQL($expires)."'")
            ->where(self::CUSTOMER_ID.' = '.(int) $customerId)
            ->where(self::WEBSITE_ID.' = '.(int) $websiteId)
            ->where(self::REFERENCE_ID." = '".pSQL($referenceId)."'")
        ;
        $id = Db::getInstance()->getValue($sql);
        if ($id) {
            $ret = new self($id);
        } else {
            $ret = new self();
            $ret->{self::MASK} = $mask;
            $ret->{self::CARD_TYPE} = $cardType;
            $ret->{self::EXPIRES} = $expires;
            $ret->{self::CUSTOMER_ID} = $customerId;
            $ret->{self::WEBSITE_ID} = $websiteId;
            $ret->{self::REFERENCE_ID} = $referenceId;
            $ret->save();
        }

        return $ret;
    }

    /**
     * Get customer cards.
     *
     * @param int $customerId
     *
     * @return array
     */
    public static function getCustomerCards($customerId)
    {
        $sql = new DbQueryCore();
        $sql->select('*');
        $sql->from(self::$definition['table']);
        $sql->where(self::CUSTOMER_ID.' = '.(int) $customerId);
        $sql->where(self::EXPIRES.' >= '.date('ym'));

        return Db::getInstance()->executeS($sql);
    }

    /**
     * Inserts element into database.
     *
     * @param bool $autoDate
     * @param bool $nullValues
     *
     * @return bool
     */
    public function add($autoDate = true, $nullValues = false)
    {
        try {
            return parent::add($autoDate, $nullValues);
        } catch (PrestaShopDatabaseException $exception) {
            return false;
        }
    }

    /**
     * Delete expired cards.
     */
    public static function deleteExpiredCards()
    {
        Db::getInstance()->delete(self::$definition['table'], self::EXPIRES.' < '.date('ym'));
    }

    /**
     * Try create table for this model.
     *
     * @return bool
     */
    public static function tryCreateTable()
    {
        Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'p24_recuring` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `website_id` INT NOT NULL,
                `customer_id` INT NOT NULL,
                `reference_id` VARCHAR(35) NOT NULL,
                `expires` VARCHAR(4) NOT NULL,
                `mask` VARCHAR (32) NOT NULL,
                `card_type` VARCHAR (20) NOT NULL,
                `timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
                UNIQUE KEY `UNIQUE_FIELDS` (`mask`,`card_type`,`expires`,`customer_id`,`website_id`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;'
        );

        return true;
    }
}
