<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class CustomerSettingsModel.
 */
class CustomerSettingsModel extends ObjectModel
{
    /**
     * Id of customer. Used to connect setting with PrestaShop customer
     * (stored in [prefix]_customer table) using its id ([prefix]_customer table.id).
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $customer_id;

    /**
     * Holds information if credit card should be forgotten for this customer by default  (0 - false, 1 - true).
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $cc_forget;

    /**
     * Did user accept Przelewy24 terms (0 - false, 1 - true).
     * PrestaShop (Adapter/Adapter_EntityMapper) enforces this parameter to have the same name as column
     * in database (przelewy24_customersettings.customer_id). This is why we left underscore in its name.
     *
     * @var int
     */
    public $accepted_terms;

    // Table columns:
    const CUSTOMER_ID = 'customer_id';
    const FORGET = 'cc_forget';
    const ACCEPTED_TERMS = 'accepted_terms';

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'przelewy24_customersettings',
        'primary' => self::CUSTOMER_ID,
        'multilang' => false,
        'fields' => array(
            self::CUSTOMER_ID => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            self::FORGET => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            self::ACCEPTED_TERMS => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        ),
    );

    /**
     * Should card be remembered.
     *
     * @return bool
     */
    public function shouldCardBeRemembered()
    {
        return '0' === $this->cc_forget || !$this->cc_forget;
    }

    /**
     * Try create table for this model.
     *
     * @return bool
     */
    public static function tryCreateTable()
    {
        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS`'._DB_PREFIX_.'przelewy24_customersettings` (
            `customer_id` INT NOT NULL PRIMARY KEY,
            `cc_forget` INT DEFAULT 0,
            `accepted_terms` INT DEFAULT 0
            );');

        return true;
    }
}
