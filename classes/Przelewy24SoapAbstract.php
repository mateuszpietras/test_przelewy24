<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

require_once 'Przelewy24Autoloader.php';

/**
 * Class Przelewy24SoapAbstract.
 */
abstract class Przelewy24SoapAbstract
{
    const ERROR_EXTENSION_NOT_INSTALLED = 'SOAP extension is not installed';
    const ERROR_PLUGIN_NOT_CONFIGURED = 'Plugin is not configured';
    const ERROR_WSDL_NOT_REACHABLE = 'Can\'t fetch WSDL file';
    const ERROR_UNABLE_CONNECTION = 'Unable to establish connection with WSDL';
    const ERROR_UNKNOWN_SOAP = 'Unknown SOAP error';
}
