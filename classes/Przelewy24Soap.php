<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

require_once 'Przelewy24Autoloader.php';

/**
 * Class Przelewy24Soap.
 */
class Przelewy24Soap extends Przelewy24SoapAbstract
{

    /**
     * Soap client.
     *
     * @var SoapClient
     */
    protected $soapClient = null;

    /**
     * Suffix for selected currency. Empty for PLN.
     *
     * @var string
     */
    protected $currencySuffix = '';

    /**
     * Merchant id
     *
     * @var string
     */
    protected $p24MerchantId;

    /**
     * Merchant id with currency
     *
     * @var string
     */
    protected $p24MerchantIdWithCurrency;

    /**
     * Api key
     *
     * @var string
     */
    protected $p24ApiKey;

    /**
     * Api key with currency
     *
     * @var string
     */
    protected $p24ApiKeyWithCurrency;

    /**
     * Shop id with currency
     *
     * @var string
     */
    protected $p24ShopIdWithCurrency;

    /**
     * Przelewy24Soap constructor.
     *
     * @param string $currencySuffix
     */
    public function __construct($currencySuffix = '')
    {
        if (!$this->isSoapExtensionInstalled()) {
            Przelewy24Logger::addLog(__METHOD__.' '.self::ERROR_EXTENSION_NOT_INSTALLED, 1);
            throw new RuntimeException(self::ERROR_EXTENSION_NOT_INSTALLED);
        }

        $this->currencySuffix = $currencySuffix;
        $this->p24MerchantId = Configuration::get(P24Configuration::P24_MERCHANT_ID);
        $this->p24ApiKey = Configuration::get(P24Configuration::P24_API_KEY);
        $this->p24MerchantIdWithCurrency = Configuration::get(P24Configuration::P24_MERCHANT_ID.$this->currencySuffix);
        $this->p24ApiKeyWithCurrency = Configuration::get(P24Configuration::P24_API_KEY.$this->currencySuffix);
        $this->p24ShopIdWithCurrency = Configuration::get(P24Configuration::P24_SHOP_ID.$this->currencySuffix);

        if (!$this->isPluginConfigured()) {
            Przelewy24Logger::addLog(__METHOD__.' '.self::ERROR_PLUGIN_NOT_CONFIGURED, 1);
            throw new RuntimeException(self::ERROR_PLUGIN_NOT_CONFIGURED);
        }

        $url = $this->getWSurl();

        if (!$this->isWsdlReachable($url)) {
            Przelewy24Logger::addLog(__METHOD__.' '.self::ERROR_WSDL_NOT_REACHABLE.': '.$url, 1);
            throw new RuntimeException(self::ERROR_WSDL_NOT_REACHABLE.' '.$url);
        }

        try {
            $this->soapClient = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE, 'exceptions' => true));
        } catch (SoapFault $e) {
            Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);

            throw new RuntimeException(self::ERROR_UNABLE_CONNECTION);
        } catch (Exception $e) {
            Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);

            throw new RuntimeException(self::ERROR_UNKNOWN_SOAP);
        }
    }

    /**
     * Refund transaction.
     *
     * @param array $refunds
     *
     * @return stdClass|string
     */
    public function refundTransaction($refunds)
    {
        try {
            $response = $this->soapClient->refundTransaction(
                $this->p24MerchantId,
                $this->p24ApiKey,
                time(),
                $refunds
            );

            return $response;
        } catch (Exception $e) {
            Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);

            return $e->getMessage();
        }
    }

    /**
     * Check if payment completed using WS and return data.
     *
     * @param string $sessionId
     *
     * @return bool
     */
    public function checkIfPaymentCompletedUsingWSAndReturnData($sessionId)
    {
        try {
            $result = $this->soapClient->GetTransactionBySessionId(
                $this->p24MerchantId,
                $this->p24ApiKey,
                $sessionId
            );

            if ($result->error->errorCode > 0 || 2 !== (int)$result->result->status) {
                return false;
            }

            return $result;
        } catch (Exception $e) {
            Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);

            return false;
        }
    }

    /**
     * Get refund info.
     *
     * @param $orderId
     * @return bool
     */
    public function getRefundInfo($orderId)
    {
        $result = $this->soapClient->GetRefundInfo(
            $this->p24MerchantId,
            $this->p24ApiKey,
            $orderId
        );

        if ($result->result) {
            return $result->result;
        }

        return false;
    }

    /**
     * Returns payment methods ordered by usage frequency.
     *
     * @param bool $only24at7
     * @param string $currency
     *
     * @return array|bool
     */
    public function availablePaymentMethods($only24at7 = true, $currency = 'PLN')
    {
        try {
            $res = $this->soapClient->paymentMethods(
                $this->p24ShopIdWithCurrency,
                $this->p24ApiKeyWithCurrency,
                Context::getContext()->language->iso_code
            );

            if (0 === (int)$res->error->errorCode) {
                if ($only24at7) {
                    foreach ($res->result as $key => $item) {
                        if (!$item->status) {
                            unset($res->result[$key]);
                        }
                    }
                }
                if ('PLN' !== $currency) {
                    foreach ($res->result as $key => $item) {
                        if (!in_array($item->id, Przelewy24::getChannelsNonPln())) {
                            unset($res->result[$key]);
                        }
                    }
                }

                return $res->result;
            }
        } catch (Exception $e) {
            Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);
        }

        return false;
    }

    /**
     * Function check refund in soap function:
     * - GetTransactionBySessionId
     * - RefundTransaction
     *
     * @return bool
     */
    public function checkRefundFunction()
    {
        $errorSoap = false;
        try {
            if ($this->hasMethod('GetTransactionBySessionId') && $this->hasMethod('RefundTransaction')) {
                $errorSoap = true;
            }
        } catch (Exception $error) {
            Przelewy24Logger::addLog(__METHOD__.' '.$error->getMessage(), 1);
            $errorSoap = false;
        }

        return $errorSoap;
    }

    /**
     * Check ivr present.
     *
     * @return bool
     */
    public function checkIvrPresent()
    {
        $hasIvr = false;
        try {
            $hasIvr = $this->hasMethod('TransactionMotoCallBackRegister');
        } catch (Exception $e) {
            Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);
        }

        return $hasIvr;
    }

    /**
     * Run IVR call.
     *
     * @param $currencySuffix
     * @param $clientPhone
     * @param $amount
     * @param $currency
     * @param $cartId
     * @param $sSid
     * @param $customerEmail
     * @param $firstname
     * @param $lastname
     * @param $urlStatus
     * @return object
     */
    public function runIvr(
        $currencySuffix,
        $clientPhone,
        $amount,
        $currency,
        $cartId,
        $sSid,
        $customerEmail,
        $firstname,
        $lastname,
        $urlStatus
    ) {
        return $this->soapClient->__call(
            'TransactionMotoCallBackRegister',
            array(
                'login' => Configuration::get(P24Configuration::P24_MERCHANT_ID.$currencySuffix),
                'pass' => Configuration::get(P24Configuration::P24_API_KEY.$currencySuffix),
                'details' => array(
                    'clientPhone' => $clientPhone,
                    'amount' => $amount,
                    'currency' => $currency,
                    'paymentId' => '#'.Tools::getValue('id_order'),
                    'description' => 'zamówienie numer '.Tools::getValue('id_order'),
                    'sessionId' => $cartId.'|'.$sSid,
                    'clientEmail' => $customerEmail,
                    'merchantEmail' => Configuration::get('PS_SHOP_EMAIL'),
                    'client' => $firstname.' '.$lastname,
                    'urlStatus' => $urlStatus,
                    'typeOfResponse' => 'post',
                    'sendEmail' => 0,
                    'time' => 0,
                    'additionalInfo' => '',
                ),
            )
        );
    }

    /**
     * Api test access.
     *
     * @return bool
     */
    public function hasApiTestAccess()
    {
        try {
            return (bool)$this->soapClient->TestAccess(
                $this->p24ShopIdWithCurrency,
                $this->p24ApiKeyWithCurrency
            );
        } catch (Exception $e) {
            Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);
        }

        return false;
    }

    /**
     * Check if soap method exists.
     *
     * @param $method
     * @return bool
     */
    public function hasMethod($method)
    {
        $list = $this->soapClient->__getFunctions();
        if (is_array($list)) {
            foreach ($list as $line) {
                $typeAnName = explode(' ', $line, 2);
                if (2 === count($typeAnName) && 0 === strpos($typeAnName[1], $method)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Remove wsdl cache.
     *
     * @return bool
     */
    public static function removeWsdlCache()
    {
        $dir = ini_get('soap.wsdl_cache_dir');
        $files = glob($dir.'/wsdl*');
        $success = true;
        if ($files) {
            foreach ($files as $file) {
                try {
                    if (is_writable($file)) {
                        unlink($file);
                    } else {
                        $success = false;
                    }
                } catch (Exception $e) {
                    $success = false;
                    Przelewy24Logger::addLog(__METHOD__.' '.$e->getMessage(), 1);
                }
            }
        }

        return $success;
    }

    /**
     * Is SOAP extension loaded.
     *
     * @return bool
     */
    public static function isSoapExtensionInstalled()
    {
        return extension_loaded('soap');
    }

    /**
     * Is plugin configured.
     *
     * @return bool
     */
    private function isPluginConfigured()
    {
        return (bool)$this->p24MerchantIdWithCurrency;
    }

    /**
     * Is plugin configured.
     *
     * @param $url
     * @return bool
     */
    private function isWsdlReachable($url)
    {
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
        curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);

        return $httpCode >= 200 && $httpCode < 300;
    }

    /**
     * Get wsdl url.
     *
     * @return string
     */
    private function getWSurl()
    {
        $mode = (Configuration::get(P24Configuration::P24_TEST_MODE)) ? 'sandbox' : 'secure';
        $url = 'https://'.$mode.'.przelewy24.pl/external/'.$this->p24MerchantIdWithCurrency.'.wsdl';

        return $url;
    }
}
