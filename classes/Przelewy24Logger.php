<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24Logger.
 */
class Przelewy24Logger
{

    /**
     * In PS1.6 PrestaShopLogger was added in place of Logger class. This method will decide which logger should be
     * called depending of current PS version and will use it.
     *
     * Add a log item to the database and send a mail if configured for this $severity.
     *
     * @param string $message the log message
     * @param int $severity
     * @param int $errorCode
     * @param string $objectType
     * @param int $objectId
     * @param bool $allowDuplicate if set to true, can log several time the same information (not recommended).
     * @param int $idEmployee employee id.
     *
     * @return bool true if succeed.
     */
    public static function addLog(
        $message,
        $severity = 1,
        $errorCode = null,
        $objectType = null,
        $objectId = null,
        $allowDuplicate = false,
        $idEmployee = null
    ) {
        return _PS_VERSION_ >= 1.6 ? PrestaShopLogger::addLog(
            $message,
            $severity,
            $errorCode,
            $objectType,
            $objectId,
            $allowDuplicate,
            $idEmployee
        ) : Logger::addLog(
            $message,
            $severity,
            $errorCode,
            $objectType,
            $objectId,
            $allowDuplicate,
            $idEmployee
        );
    }
}
