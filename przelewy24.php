<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

if (!defined('_PS_VERSION_')) {
    die('Nieznana wersja Prestashop/Unknown Prestashop version.');
}

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'Przelewy24Autoloader.php';
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'class_przelewy24.php';

/**
 * Class Przelewy24
 */
class Przelewy24 extends PaymentModule
{
    const MODULE_KEY = '5904b0e92c361acd8b5e76bb45b60b2b';
    const PLUGIN_VERSION = '3.4.28';
    const MAX_PAGES_TO_PROCESS_IN_ONE_RESET_REQUEST = 100;

    /**
     * Are currencies supported.
     *
     * @var bool
     */
    public $currencies = true;

    /**
     * Type of field to switch between currencies.
     *
     * @var string
     */
    public $currencies_mode = 'checkbox';

    /**
     * Minimal amount for single installment.
     *
     * @var int
     */
    private static $minInstallmentValue = 300;

    /**
     * Przelewy24 constructor.
     */
    public function __construct()
    {
        $this->name = 'przelewy24';
        $this->tab = 'payments_gateways';
        $this->version = '3.4.28';
        $this->is_eu_compatible = 1;
        $this->currencies = true;
        $this->author = 'Przelewy24';
        $this->module_key = self::MODULE_KEY;
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';
        $this->bootstrap = true;
        $this->p24_css = Przelewy24Class::getLiveHost() . 'skrypty/ecommerce_plugin.css.php';
        $this->discount_code_prefix = 'P24_';
        parent::__construct();
        $this->displayName = $this->l('Przelewy24');
        if (_PS_VERSION_ < 1.5) {
            $this->description = $this->l('This extension supports newer version of PrestaShop.');
            parent::uninstall();
        } else {
            $this->description = $this->l('Przelewy24.pl Payment Service');
        }
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (null === Configuration::get(P24Configuration::P24_ORDER_STATE_1)) {
            Configuration::updateValue(P24Configuration::P24_ORDER_STATE_1, 1);
        }
        if (null === Configuration::get(P24Configuration::P24_ORDER_STATE_2)) {
            Configuration::updateValue(P24Configuration::P24_ORDER_STATE_2, 2);
        }

        if (null === Configuration::get(P24Configuration::P24_INSTALLMENT_SHOW)) {
            Configuration::updateValue(P24Configuration::P24_INSTALLMENT_SHOW, 0);
        }

        if (null === Configuration::get(P24Configuration::P24_ORDER_TITLE_ID)) {
            Configuration::updateValue(P24Configuration::P24_ORDER_TITLE_ID, 0);
        }

        if (null === Configuration::get(P24Configuration::P24_SHOW_SUMMARY)) {
            Configuration::updateValue(P24Configuration::P24_SHOW_SUMMARY, 0);
        }

        if (null === Configuration::get(P24Configuration::P24_DEBUG)) {
            Configuration::updateValue(P24Configuration::P24_DEBUG, 0);
        }

        if (!Configuration::hasKey(P24Configuration::P24_VERIFYORDER)) {
            Configuration::updateValue(P24Configuration::P24_VERIFYORDER, 1);
        }

        if ('1' !== Configuration::get(P24Configuration::P24_CONFIGURED)) {
            $this->warning = $this->l('Module is not configured.');
        }

        if (null === Configuration::get(P24Configuration::P24_WAIT_FOR_RESULT)) {
            Configuration::updateValue(P24Configuration::P24_WAIT_FOR_RESULT, 0);
        }

        if (Configuration::get(P24Configuration::P24_PLUGIN_VERSION) !== $this->version
            && isset(Context::getContext()->controller->admin_webpath)) {
            $this->install();
        }
    }

    /**
     * Get min installment value
     *
     * @return int
     */
    public static function getMinInstallmentValue()
    {
        return self::$minInstallmentValue;
    }

    /**
     * Validate credential.
     *
     * @param int $merchant
     * @param int $pos
     * @param string $salt
     * @param bool $test
     * @return bool|null
     */
    public function validateCredentials($merchant, $pos, $salt, $test)
    {
        Configuration::updateValue(P24Configuration::P24_CONFIGURED, 0);
        try {
            $P24C = new Przelewy24Class($merchant, $pos, $salt, $test);
            $result = $P24C->testConnection();
        } catch (Exception $e) {
            Przelewy24Logger::addLog(__CLASS__ . ' ' . __METHOD__ . ' ' . $e->getMessage(), 1);
        }
        if (!empty($merchant) && !empty($pos) && !empty($salt)) {
            $hasError = array_key_exists('error', $result) && 0 !== (int)$result['error'];
            if (!$hasError) {
                Configuration::updateValue(P24Configuration::P24_CONFIGURED, 1);

                return true;
            }

            return false;
        }

        return null;
    }

    /**
     * Get currency code.
     *
     * @param int $currencyId
     * @return string
     */
    public function getCurrencyCode($currencyId)
    {
        $currency = $this->getActualCurrencyById($currencyId);

        return $currency['iso_code'];
    }

    /**
     * Get actual currency by id
     *
     * @param int $id
     * @return array
     */
    private function getActualCurrencyById($id)
    {
        static $currency = null;
        if (null === $currency) {
            $currencies = $this->getCurrency($id);
            foreach ($currencies as $c) {
                if ((int)$c['id_currency'] === (int)$id) {
                    $currency = $c;
                    break;
                }
            }
        }

        return $currency;
    }


    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        $helper = new Przelewy24HelperForm($this);

        if (Tools::isSubmit('currency')) {
            Przelewy24Soap::removeWsdlCache();
            Przelewy24Logger::addLog(__CLASS__ . ' ' . __METHOD__ . ' Configuration has been changed', 1);

            $helper->markAsSubmitted();
            $saveHelper = new Przelewy24HelperSaveForm($helper->getParameters(), $this->name);
            $saveHelper->save();
        }

        return $helper->generate();
    }

    /**
     * Display template.
     *
     * @param string $template
     * @param string $templatesDirectory
     * @param string $extension
     *
     * @return string
     */
    public function displayTemplate($template, $templatesDirectory = 'views/templates/admin', $extension = 'tpl')
    {
        return $this->display(__FILE__, $templatesDirectory . '/' . $template . '.' . $extension);
    }

    /**
     * Removes one page of P24 parameters. Each currency has its own set of parameters, and PrestaShop can accept
     * unlimited number of currencies. Pagination was provided in order to prevent possible performance issues.
     *
     * @return bool|null is next iteration required (true|false)? In case of error method will return null
     */
    private function removeParametersPage()
    {
        if (!$parameters = P24DbHelper::getParameters()) {
            return null;
        }
        if (!count($parameters)) {
            return false;
        }
        foreach ($parameters as $parameter) {
            if (!is_array($parameter)
                || !isset($parameter['name'])
                || !Configuration::deleteByName($parameter['name'])
            ) {
                return null;
            }
        }

        return true;
    }

    /**
     * Get module parameters (with fixed limit).
     *
     * @param int $limit
     *
     * @return array
     */
    public function getParameters($limit = 100)
    {
        $queryBuilder = new DbQuery();
        $queryBuilder
            ->select('name')
            ->from('configuration')
            ->where("name LIKE 'P24#_%' ESCAPE '#'")
            ->limit($limit);

        return Db::getInstance()->executeS($queryBuilder);
    }

    /**
     * Resets configuration table.
     *
     * @return bool
     */
    public function reset()
    {
        $nextIterationRequired = null;
        $currentPage = 0;
        while ($nextIterationRequired = $this->removeParametersPage()
            && $currentPage < self::MAX_PAGES_TO_PROCESS_IN_ONE_RESET_REQUEST) {
            ++$currentPage;
        }

        return false === $nextIterationRequired;
    }

    /**
     * Install.
     *
     * @return bool
     */
    public function install()
    {
        if (_PS_VERSION_ >= 1.5) {
            if (!parent::install()) {
                return false;
            }

            P24DbHelper::addOrderState($this->l('Awaiting Przelewy24 payment'), '1', 'Lightblue');
            P24DbHelper::addOrderState(
                $this->l('Payment Przelewy24 accepted'),
                '2',
                'Limegreen',
                1,
                1,
                'payment'
            );

            // Set default settings in installation
            foreach (CurrencyCore::getCurrencies() as $currency) {
                $currencySuffix = 'PLN' === $currency['iso_code'] ? '' : '_' . $currency['iso_code'];
                if (!Configuration::get(P24Configuration::P24_PAYMETHOD_GRAPHICS . $currencySuffix)) {
                    Configuration::updateValue(P24Configuration::P24_PAYMETHOD_GRAPHICS . $currencySuffix, 1);
                }
                if (!Configuration::get(P24Configuration::P24_PAYMETHOD_FIRST . $currencySuffix)) {
                    Configuration::updateValue(
                        P24Configuration::P24_PAYMETHOD_FIRST . $currencySuffix,
                        '25,31,112,20,65'
                    );
                }
                if (!Configuration::get(P24Configuration::P24_DEFAULT_REMEMBER_CARD . $currencySuffix)) {
                    Configuration::updateValue(P24Configuration::P24_DEFAULT_REMEMBER_CARD . $currencySuffix, 1);
                }
            }

            Configuration::updateValue(P24Configuration::P24_PLUGIN_VERSION, $this->version);

            if (!$this->registerHook('displayPayment')
                || !$this->registerHook('displayOrderDetail')
                || !$this->registerHook('displayRightColumnProduct')
                || !$this->registerHook('displayCustomerAccount')
                || !$this->registerHook('displayHeader')
                || !$this->registerHook('displayInvoice')
                || !$this->registerHook('displayShoppingCartFooter')
                || !$this->registerHook('displayShoppingCart')
                || !$this->registerHook('displayBackOfficeHeader')
                || !$this->registerHook('displayAdminOrder')
                || !$this->registerHook('displayAdminOrderTabOrder')
                || !$this->registerHook('displayPDFInvoice')
                || !$this->registerHook('actionValidateOrder')
                || !$this->registerHook('actionEmailAddAfterContent')
                || !$this->registerHook('actionGetExtraMailTemplateVars')
                || !$this->registerHook('displayFooter')
                || !OrderDetailsModel::tryCreateTable()
                || !LastMethodModel::tryCreateTable()
                || !CustomerSettingsModel::tryCreateTable()
                || !Extracharge::tryCreateTable()
                || !RecurringModel::tryCreateTable()
            ) {
                return false;
            }

            return true;
        } else {
            parent::uninstall();

            return false;
        }
    }


    /**
     * Add to email html template: 'email_extracharge.tpl' if order have extra charge.
     *
     * IMPORTANT original email template has been cut in first '<tr class="conf_body">'.
     *
     * @param array $params
     */
    public function hookActionEmailAddAfterContent($params)
    {
        if ($params[Przelewy24Consts::EMAIL_PARAMS_TEMPLATE_NAME] !== Przelewy24Consts::EMAIL_TEMPLATE_NAME_ORDER) {
            return;
        }

        $cart = $this->context->cart;

        if ($cart) {
            $paymentData = new Przelewy24PaymentData($cart);
        } else {
            return;
        }

        if (!$paymentData->isModuleOfOrderMatched($this->name)) {
            return;
        }

        $emailHead = strstr($params[Przelewy24Consts::CONFIRMATION_EMAIL_HTML_KEY], '<tr class="conf_body">', true);
        $emailFoot = strstr($params[Przelewy24Consts::CONFIRMATION_EMAIL_HTML_KEY], '<tr class="conf_body">');

        if ($emailHead) {
            $params[Przelewy24Consts::CONFIRMATION_EMAIL_HTML_KEY] =
                $emailHead . Przelewy24Consts::EMAIL_TEMPLATE_VAR_EXTRACHARGE . $emailFoot;
        }
    }

    /**
     * Add params to mail template.
     *
     * @param $params
     */
    public function hookActionGetExtraMailTemplateVars($params)
    {
        /* By default it should be empty string. */
        $params[Przelewy24Consts::EMAIL_PARAMS_EXTRA_VARS][Przelewy24Consts::EMAIL_TEMPLATE_VAR_EXTRACHARGE] = '';

        $cart = $this->context->cart;
        if ($cart) {
            $paymentData = new Przelewy24PaymentData($cart);
        } else {
            return;
        }

        if (!$paymentData->isModuleOfOrderMatched($this->name)) {
            return;
        }

        $reference = $params[Przelewy24Consts::EMAIL_PARAMS_STD_VARS][Przelewy24Consts::EMAIL_TEMPLATE_VAR_ORDER_NAME];
        $amount = $paymentData->getExtrachargeForOrderReference($reference);
        if ($amount) {
            $params[Przelewy24Consts::EMAIL_PARAMS_EXTRA_VARS][Przelewy24Consts::EMAIL_TEMPLATE_VAR_EXTRACHARGE] =
                $this->renderExtraChargeDataInMail($amount);
        }
    }

    /**
     * Prepares html string containing extracharge data, which has to be send in email to user.
     *
     * @param float $extracharge
     * @return string
     */
    private function renderExtraChargeDataInMail($extracharge)
    {
        /* The $extracharge is expected to be rounded. */
        if (0.0 === (float)$extracharge) {
            return '';
        }
        $this->context->smarty->assign(array(
            'extracharge' =>  Tools::displayPrice($extracharge, $this->context->currency, false),
            'extracharge_text' => $this->l('Extra charged was added to this order by Przelewy24: '),
        ));

        return $this->display(__FILE__, 'email_extracharge.tpl');
    }

    /**
     * Hook display payment.
     */
    public function hookDisplayPayment()
    {
        $currency = $this->getCurrencyCode($this->context->cart->id_currency);
        $currencySuffix = ('PLN' === $currency || empty($currency)) ? '' : '_' . $currency;

        if (empty($currency)) {
            return;
        }
        //$amount = $order->total_products_wt * 100; // total products price tax included
        $amount = $this->context->cart->getOrderTotal(true, Cart::BOTH);
        $protocol = Przelewy24::getHttpProtocol();
        $this->context->smarty->assign(
            'p24_url',
            $protocol . htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8') . __PS_BASE_URI__
        );
        $this->context->smarty->assign('ps_version', _PS_VERSION_);

        $p24UrlPayment = $this->context->link->getModuleLink(
            'przelewy24',
            'paymentConfirmation',
            array(),
            '1' === Configuration::get('PS_SSL_ENABLED')
        );

        $channelsIds = array();
        $channelsList = array();

        // installment with suffix
        if ($amount >= self::getMinInstallmentValue()
            && Configuration::get(P24Configuration::P24_INSTALLMENT_SHOW . $currencySuffix) > 0) {
            $channelsIds = $this->getChannelsRaty();
        }

        if ('1' === Configuration::get(P24Configuration::P24_PAYMETHOD_PROMOTE . $currencySuffix)) {
            $paymethodPromoted = Configuration::get(P24Configuration::P24_PAYMETHOD_PROMOTED . $currencySuffix);
            $paymethodPromotedAr = explode(',', $paymethodPromoted);
            if (is_array($paymethodPromotedAr)) {
                foreach ($paymethodPromotedAr as $id) {
                    if ($id > 0) {
                        $channelsIds[] = $id;
                    }
                }
            }
        }

        if (count($channelsIds) > 0) {
            $paymentListRes = $this->availablePaymentMethods(
                '1' !== Configuration::get(P24Configuration::P24_PAYSLOW_ENABLED . $currencySuffix),
                $currency
            );
            $paymentList = array();
            if (is_array($paymentListRes)) {
                $thereIs218 = false;
                foreach ($paymentListRes as $item) {
                    $paymentList[$item->id] = $item;
                    if (218 === (int)$item->id) {
                        $thereIs218 = true;
                    }
                }
                if ($thereIs218) {
                    unset($paymentList[142], $paymentList[145]);
                }
            }
            $querySeparator = false !== strpos($p24UrlPayment, '?') ? '&' : '?';
            foreach ($channelsIds as $id) {
                if (isset($paymentList[$id])) {
                    $channelsList[] = array(
                        'name' => $paymentList[$id]->name,
                        'url' => "{$p24UrlPayment}{$querySeparator}payment_method={$id}",
                        'desc' => Configuration::get(P24Configuration::P24_CUSTOM_DESCRIPTION . $currencySuffix),
                    );
                }
            }
        }

        $channelsList[] = array(
            'name' => 'Przelewy24',
            'logo' => _MODULE_DIR_ . 'przelewy24/views/img/logo_small.png',
            'url' => $p24UrlPayment,
            'desc' => Configuration::get(P24Configuration::P24_CUSTOM_DESCRIPTION . $currencySuffix),
        );

        $this->context->smarty->assign('p24_channels_list', $channelsList);
        $this->context->smarty->assign(
            'p24_gate_logo',
            (int)Configuration::get(P24Configuration::P24_GATE_LOGO . $currencySuffix)
        );
        $this->context->smarty->assign(
            'p24_gate_class',
            Configuration::get(P24Configuration::P24_GATE_CLASS . $currencySuffix)
        );
        $this->context->smarty->assign(
            'p24_chevron_right',
            (int)Configuration::get(P24Configuration::P24_CHEVRON_RIGHT . $currencySuffix)
        );
        $this->context->controller->addCSS(Przelewy24Class::getLiveHost() . 'skrypty/ecommerce_plugin.css.php');
        $this->context->controller->addJS(__PS_BASE_URI__ . 'modules/przelewy24/views/js/p24_order.js');

        return $this->display(__FILE__, 'payment.tpl');
    }

    /**
     * Gets content of requested url.
     *
     * @param string $url
     *
     * @return string
     */
    public static function requestGet($url)
    {
        $isCurl = function_exists('curl_init')
            && function_exists('curl_setopt')
            && function_exists('curl_exec')
            && function_exists('curl_close');

        if ($isCurl) {
            $userAgent = 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)';
            $curlConnection = curl_init();
            curl_setopt($curlConnection, CURLOPT_URL, $url);
            curl_setopt($curlConnection, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curlConnection, CURLOPT_USERAGENT, $userAgent);
            curl_setopt($curlConnection, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlConnection, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curlConnection);
            curl_close($curlConnection);

            return $result;
        }

        return '';
    }

    /**
     * Hook display right column product.
     *
     * @return Smarty_Internal_Data
     */
    public function hookDisplayRightColumnProduct()
    {
        if ('2' === Configuration::get(P24Configuration::P24_INSTALLMENT_SHOW)) {
            $productID = Tools::getValue('id_product');

            if ($productID) {
                $product = new Product((int)$productID, true, $this->context->cookie->id_lang);
                $amount = $product->getPrice(true, null, 2, null, false, true, 1);
                $context = Context::getContext();
                $idCurrency = (int)(Validate::isLoadedObject($context->currency) ?
                    $context->currency->id : Configuration::get('PS_CURRENCY_DEFAULT'));
                $currencies = $this->getCurrency($idCurrency);
                foreach ($currencies as $c) {
                    if ((int)$c['id_currency'] === $idCurrency) {
                        $currency = $c['iso_code'];
                    }
                }
                if ($amount >= self::getMinInstallmentValue() && 'PLN' === $currency) {
                    $amountInt = $amount * 100;
                    $liveUrl = Przelewy24Class::getLiveHost();
                    $result = self::requestGet($liveUrl . 'kalkulator_raty.php?ammount=' . $amountInt);
                    $resultTab = explode('<br>', $result);
                    $this->context->smarty->assign(
                        array(
                            'liveUrl' => $liveUrl,
                            'part_count' => $resultTab[0],
                            'part_cost' => $resultTab[1],
                            'product_ammount' => $amount,
                            'p24_ajax_url' => $this->context->link->getModuleLink(
                                'przelewy24',
                                'paymentAjax',
                                array(),
                                '1' === Configuration::get('PS_SSL_ENABLED')
                            ),
                        )
                    );

                    return $this->display(__FILE__, 'installmentRightColumnProduct.tpl');
                }
            }
        }
    }

    /**
     * Hook display order detail.
     *
     * @param array $params
     *
     * @return Smarty_Internal_Data
     */
    public function hookDisplayOrderDetail($params)
    {
        /** @var OrderCore $order */
        $order = $params['order'];
        $state = new OrderState($order->current_state);

        $currency = new Currency($order->id_currency);
        /* For particular Order, not the first one from the set. */
        $extracharge = Extracharge::getExtrachargeByOrderId((int)$order->id);

        $this->context->smarty->assign(
            array(
                'extracharge' => '',
                'extracharge_text' => '',
            )
        );

        if ($extracharge && $extracharge->extracharge_amount > 0) {
            $this->context->smarty->assign(
                array(
                    'extracharge' => number_format(
                        round($extracharge->extracharge_amount, 2),
                        2,
                        ',',
                        '.'
                    ) . ' ' . $currency->sign,
                    'extracharge_text' => $this->l('Extracharge Przelewy24'),
                )
            );
        }

        $customer = new Customer((int)($order->id_customer));
        if ('przelewy24' === $order->module
            && !($state->paid || $order->current_state === Configuration::get(P24Configuration::P24_ORDER_STATE_2))) {
            $this->context->smarty->assign(
                'p24_retryPaymentUrl',
                $this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentConfirmation',
                    array('order_id' => (int)$order->id, 'token' => $customer->secure_key)
                )
            );

            return $this->display(__FILE__, 'renewPaymentOrderDetail.tpl');
        }

        return $this->display(__FILE__, 'user_order_extracharge.tpl');
    }

    /**
     * Hook display customer account.
     *
     * @return Smarty_Internal_Data|null
     */
    public function hookDisplayCustomerAccount()
    {
        if ('1' === Configuration::get(P24Configuration::P24_ONECLICK_ENABLED)) {
            $protocol = Przelewy24::getHttpProtocol();
            $shortUrl = 'module/przelewy24/myStoredCards';
            $longUrl = 'index.php?fc=module&module=przelewy24&controller=myStoredCards';
            $this->context->smarty->assign(
                'base_url',
                $protocol . htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8')
                . __PS_BASE_URI__
            );
            $this->context->smarty->assign(
                'end_url',
                0 === (int)Configuration::get('PS_REWRITING_SETTINGS') ? $longUrl : $shortUrl
            );

            return $this->display(__FILE__, 'myStoredCards.tpl');
        }

        return null;
    }

    /**
     * Hook display header.
     */
    public function hookDisplayHeader()
    {
        $this->removeDiscount();
    }

    /**
     * Hook display footer.
     *
     * @return Smarty_Internal_Data|null
     */
    public function hookDisplayFooter()
    {
        $this->removeDiscount();
    }

    /**
     * Hook display invoice.
     *
     * @param array $params
     *
     * @return Smarty_Internal_Data|null
     */
    public function hookDisplayInvoice($params)
    {
        if ('1' === Configuration::get(P24Configuration::P24_IVR_ENABLED)) {
            $orderId = (int)$params['id_order'];
            $order = new Order($orderId);
            $state = new OrderState($order->current_state);
            $address = new Address((int)$order->id_address_delivery);
            $clientPhone = (!empty($address->phone_mobile) ? $address->phone_mobile : $address->phone);

            if ($state->paid) {
                return null;
            }
            $this->smarty->assign(
                array(
                    'clientPhone' => $clientPhone,
                    'orderId' => $orderId,
                    'ajaxUrl' => $this->context->link->getModuleLink(
                        'przelewy24',
                        'paymentAjax',
                        array(),
                        '1' === Configuration::get('PS_SSL_ENABLED')
                    ),
                )
            );

            return $this->display(__FILE__, 'displayInvoice.tpl');
        }
    }

    /**
     * Get http protocol.
     *
     * @return string
     */
    public static function getHttpProtocol()
    {
        return (isset($_SERVER['HTTPS']) && 'off' !== $_SERVER['HTTPS']) ? 'https://' : 'http://';
    }

    /**
     * Get extracharge.
     *
     * Amount in grosz.
     *
     * @param float $amount
     * @param string $currencySuffix
     *
     * @return float
     */
    public static function getExtracharge($amount, $currencySuffix)
    {
        $extrachargeAmount = 0;
        if (('1' === Configuration::get(P24Configuration::P24_EXTRACHARGE_ENABLED . $currencySuffix)) &&
            ($amount > 0)) {
            $incAmountSettings = (float)(str_replace(
                ',',
                '.',
                Configuration::get(P24Configuration::P24_EXTRACHARGE_AMOUNT . $currencySuffix)
            ));
            $incPercentSettings = (float)(str_replace(
                ',',
                '.',
                Configuration::get(P24Configuration::P24_EXTRACHARGE_PERCENT . $currencySuffix)
            ));

            $incAmount = round($incAmountSettings > 0 ? $incAmountSettings * 100 : 0);
            $incPercent = round($incPercentSettings > 0 ? $incPercentSettings / 100 * $amount : 0);

            $extrachargeAmount = max($incAmount, $incPercent);
        }

        return round($extrachargeAmount);
    }

    /**
     * Cart has discount.
     *
     * @param Cart|null $cart
     *
     * @return int|null
     */
    public function cartHasDiscount($cart = null)
    {
        if (empty($cart)) {
            $cart = Context::getContext()->cart;
        }
        if ($cart) {
            foreach ($cart->getCartRules() as $item) {
                if (Tools::substr($item['code'], 0, 4) === $this->discount_code_prefix) {
                    return $item['id_cart_rule'];
                }
            }
        }

        return null;
    }

    /**
     * Remove discount.
     *
     * @param Cart $cart
     */
    public function removeDiscount($cart = null)
    {
        if (false === strpos($_SERVER['REQUEST_URI'], $this->name)) {
            if (empty($cart)) {
                $cart = Context::getContext()->cart;
            }

            $currency = $this->getCurrencyCode($cart->id_currency);
            $currencySuffix = ('PLN' === $currency || empty($currency)) ? '' : '_' . $currency;

            if (Configuration::get(P24Configuration::P24_EXTRADISCOUNT_ENABLED . $currencySuffix) > 0) {
                $discountId = $this->cartHasDiscount($cart);
                if ($discountId) {
                    $cart->removeCartRule($discountId);
                    $cart_rule = new CartRule((int)$discountId);
                    $cart_rule->delete();
                }
            }
        }
    }

    /**
     * Add to card discount coupon.
     *
     * @param Cart $cart
     * @param string $currency
     *
     * @return bool
     **/
    public function addDiscount($cart = null, $currency = 'PLN')
    {
        $currencySuffix = ('PLN' === $currency || empty($currency)) ? '' : '_' . $currency;

        if (Configuration::get(P24Configuration::P24_EXTRADISCOUNT_ENABLED . $currencySuffix) > 0) {
            if (empty($cart)) {
                $cart = Context::getContext()->cart;
            }
            if ($cart) {
                $discountId = $this->cartHasDiscount($cart);
                if (null === $discountId) {
                    $cartRule = new CartRule();
                    $array = array();
                    $languages = Language::getLanguages();
                    foreach ($languages as $language) {
                        $array[$language['id_lang']] = 'Rabat za użycie Przelewy24';
                    }

                    $cartRule->name = $array;
                    $cartRule->code = Tools::strtoupper(uniqid($this->discount_code_prefix));
                    $cartRule->description = 'Rabat za użycie Przelewy24';
                    $cartRule->partial_use = 1;
                    $cartRule->reduction_currency = (int)Configuration::get('PS_CURRENCY_DEFAULT');
                    $cartRule->active = 1;
                    $cartRule->id_customer = $cart->id_customer;
                    $cartRule->date_from = date('Y-m-d 00:00:00');
                    $cartRule->date_to = date(
                        'Y-m-d h:i:s',
                        mktime(
                            0,
                            0,
                            0,
                            date('m'),
                            date('d') + 1,
                            date('Y')
                        )
                    );
                    $cartRule->minimum_amount = '1';
                    $cartRule->minimum_amount_currency = '1';
                    $cartRule->quantity = '1';
                    $cartRule->quantity_per_user = '1';
                    $cartRule->product_restriction = '1';
                    $cartRule->priority = '1';

                    $cartRule->reduction_tax = true;
                    $cartRule->reduction_product = 0;
                    $cartRule->date_add = 'NOW()';
                    $cartRule->date_upd = 'NOW()';

                    $cartRule->reduction_amount
                        = '1' === Configuration::get(P24Configuration::P24_EXTRADISCOUNT_ENABLED . $currencySuffix)
                        ? Configuration::get(P24Configuration::P24_EXTRADISCOUNT_AMOUNT . $currencySuffix) : 0;
                    $cartRule->reduction_percent
                        = '2' === Configuration::get(P24Configuration::P24_EXTRADISCOUNT_ENABLED . $currencySuffix)
                        ? Configuration::get(P24Configuration::P24_EXTRADISCOUNT_PERCENT . $currencySuffix) : 0;

                    $cartRule->add();

                    $cart->addCartRule($cartRule->id);

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check ivr present.
     *
     * @param string $currencySuffix
     *
     * @return bool
     */
    public function checkIvrPresent($currencySuffix)
    {
        return $this->getSoapConnection($currencySuffix)->checkIvrPresent();
    }

    /**
     * Check credit card recurrence.
     *
     * @param string $currencySuffix
     *
     * @return bool
     */
    public function ccCheckRecurrency($currencySuffix)
    {
        $przelewy24SoapCardService = new Przelewy24SoapCardService($currencySuffix);

        return $przelewy24SoapCardService->ccCheckRecurrency();
    }

    /**
     * Api test access.
     *
     * @param string $currencySuffix
     *
     * @return bool
     */
    public function apiTestAccess($currencySuffix)
    {
        try {
            return $this->getSoapConnection($currencySuffix)->hasApiTestAccess();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Returns payment methods ordered by usage frequency.
     *
     * @param bool $only24at7
     * @param string $currency
     *
     * @return array|bool
     */
    public function availablePaymentMethods($only24at7 = true, $currency = 'PLN')
    {
        $currencySuffix = ('PLN' === $currency || empty($currency)) ? '' : '_' . $currency;

        return $this->getSoapConnection($currencySuffix)->availablePaymentMethods($only24at7, $currency);
    }

    /**
     * Get channels non pln.
     *
     * @return array
     */
    public static function getChannelsNonPln()
    {
        return array(66, 92, 124, 140, 145, 152, 218);
    }

    /**
     * Get channels raty.
     *
     * @return array
     */
    public static function getChannelsRaty()
    {
        return array(72, 129, 136);
    }

    /**
     * Hook display pdf invoice.
     *
     * @param array $params
     *
     * @return string|null
     */
    public function hookDisplayPDFInvoice($params)
    {
        $orderInvoice = $params['object'];
        if (!($orderInvoice instanceof OrderInvoice)) {
            return null;
        }

        $idOrder = (int)$orderInvoice->id_order;
        $order = new Order($idOrder);
        $currency = new Currency((int)$order->id_currency);
        $extracharge = Extracharge::getExtrachargeByOrderId($idOrder);
        if ($extracharge) {
            return $this->l('Extracharged was added to this order by Przelewy24: ') . "<br>"
                . number_format(
                    round($extracharge->extracharge_amount, 2),
                    2,
                    ',',
                    '.'
                ) . ' ' . $currency->sign;
        }

        return null;
    }

    /**
     * Hook action validate order.
     *
     * @param array $params
     */
    public function hookActionValidateOrder($params)
    {
        $tmpOrder = $params['order'];
        if ($tmpOrder->module !== $this->name) {
            /* Other module. Nothing to do. */
            return;
        }
        $paymentData = new Przelewy24PaymentData($params['cart']);
        $paymentData->addExtracharge();
        $paymentData->fixExtrachargeInTmpOrder($tmpOrder);
    }

    /**
     * Add additional script for back office.
     */
    public function hookDisplayBackOfficeHeader()
    {
        $this->context->controller->addJS($this->_path.'views/js/admin.js');
    }

    /**
     * Render box with data to display extracharge.
     *
     * The box is hidden. It is used as a data holder. The actual rendering is done by JavaScript.
     * This is limitation of hook system of PrestaShop.
     *
     * @param array $params
     * @return null|string The rendered output if any.
     */
    public function hookDisplayAdminOrder($params)
    {
        $orderId = (int)$params['id_order'];
        $extracharge = Extracharge::getExtrachargeByOrderId($orderId);
        $extrachargeAmount = (float)$extracharge->extracharge_amount;
        if ($extrachargeAmount > 0) {
            $order = new Order($orderId);
            $currencyId = (int)$order->id_currency;
            $this->context->smarty->assign(
                array(
                    'przelewy24_extracharge_amount' => $extrachargeAmount,
                    'przelewy24_extracharge_currency_id' => $currencyId,
                    'przelewy24_extracharge_text' => $this->l('Extracharge Przelewy24'),
                )
            );
            return $this->display(__FILE__, 'admin_order.tpl');
        }

        return null;
    }

    /**
     * Hook (additional data to be displayed in admin order tab).
     *
     * @param array $params
     *
     * @return Smarty_Internal_Data
     *
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayAdminOrderTabOrder($params)
    {
        $order = $params['order'];
        $orderId = (int)$order->id;

        if (!Przelewy24Soap::isSoapExtensionInstalled() || !$orderId) {
            return null;
        }

        $dataToRefund = $this->checkIfRefundIsPossibleAndReturnDataToRefund($orderId);
        $dataToRefund['refundError'] = '';
        if (!$dataToRefund) {
            return false;
        }

        if (Tools::isSubmit('submitRefund')) {
            $amountToRefund = (float)Tools::getValue('amountToRefund') * 100;
            $mess = $this->refundProcess($dataToRefund['sessionId'], $dataToRefund['p24OrderId'], $amountToRefund);

            if (is_object($mess)) {
                $messError = array();
                foreach ($mess->result as $r) {
                    $messError[] = $r->error;
                }
                if (!empty($messError[0])) {
                    $dataToRefund['refundError'] = $messError;
                }
            } else {
                $dataToRefund['refundError'] = $this->l('Refund error');
            }
        }

        $dataToRefund['error'] = $this->checkRefundFunction();

        $dataToRefund['refunds'] = false;
        if (array_key_exists('p24OrderId', $dataToRefund)) {
            $refunds = $this->getRefunds($dataToRefund['p24OrderId']);
        } else {
            $refunds = false;
        }
        $dataToRefund['currency'] = 'zł';


        $currency = new Currency($order->id_currency);
        if (isset($currency->iso_code)) {
            $dataToRefund['currency'] = $currency->iso_code;
        }

        $dataToRefund['getRefundInfo'] = $this->checkGetRefundInfo();

        if ($refunds && $dataToRefund['getRefundInfo']) {
            $dataToRefund['amount'] -= $refunds['maxToRefund'];
            $dataToRefund['refunds'] = $refunds['refunds'];
        }
        $this->assignToTransactionRefundView($dataToRefund);

        return $this->display(__FILE__, 'transactionRefund.tpl');
    }

    /**
     * Function check refound in soap function:
     * - GetTransactionBySessionId
     * - RefundTransaction
     *
     * @return array
     */
    public function checkRefundFunction()
    {
        try {
            $hasRefundFunction = $this->getSoapConnection()->checkRefundFunction();
        } catch (Exception $e) {
            $hasRefundFunction = false;
        }

        return array('error' => $hasRefundFunction);
    }

    /**
     * Function check in soap function:
     * - GetRefundInfo.
     *
     * @return bool
     */
    public function checkGetRefundInfo()
    {
        try {
            return $this->getSoapConnection()->hasMethod('GetRefundInfo');
        } catch (Exception $e) {
            Przelewy24Logger::addLog(__CLASS__ . ' ' . __METHOD__ . ' ' . $e->getMessage(), 1);

            return false;
        }
    }

    /**
     * Checks if refund is possible and (in case it is) returns data needed to execute it.
     *
     * @param $orderId
     *
     * @return bool|array
     *
     * @throws PrestaShopDatabaseException
     */
    private function checkIfRefundIsPossibleAndReturnDataToRefund($orderId)
    {
        $refund = OrderDetailsModel::getRefundData($orderId);
        if (!$refund) {
            return false;
        }

        $dataFromP24 = $this->getSoapConnection()->checkIfPaymentCompletedUsingWSAndReturnData($refund['sessionId']);
        if (!$dataFromP24) {
            return false;
        }

        $return = array(
            'sessionId' => $refund['sessionId'],
            'amount' => $dataFromP24->result->amount,
            'p24OrderId' => $dataFromP24->result->orderId,
        );

        return $return;
    }

    /**
     * Get refunds.
     *
     * This is for the concrete order in admin panel.
     * Few orders may share the same cart.
     *
     * @param int $orderId
     *
     * @return array|false
     */
    private function getRefunds($orderId)
    {
        try {
            $return = false;
            $result = $this->getSoapConnection()->getRefundInfo($orderId);

            if ($result) {
                $return = array();
                $return['maxToRefund'] = 0;
                foreach ($result as $key => $value) {
                    $return['refunds'][$key]['amount_refunded'] = $value->amount;
                    $date = new DateTime($value->date);
                    $return['refunds'][$key]['created'] = $date->format('Y-m-d H:i:s');
                    $return['refunds'][$key]['status'] = $this->getStatusMessage($value->status);
                    $return['refunds'][$key]['currency'] = 'zł';
                    if (1 === (int)$value->status || 3 === (int)$value->status) {
                        $return['maxToRefund'] += $value->amount;
                    }
                }
            }

            return $return;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Assign to transaction refund view.
     *
     * @param array $dataToRefund
     *
     * @return Smarty_Internal_Data
     */
    private function assignToTransactionRefundView($dataToRefund)
    {
        $result = $this->context->smarty->assign(
            array(
                'amount' => array_key_exists('amount', $dataToRefund) ? $dataToRefund['amount'] : null,
                'refunds' => array_key_exists('refunds', $dataToRefund) ? $dataToRefund['refunds'] : null,
                'error' => array_key_exists('error', $dataToRefund) ? $dataToRefund['error'] : null,
                'getRefundInfo' =>
                    array_key_exists('getRefundInfo', $dataToRefund) ? $dataToRefund['getRefundInfo'] : null,
                'refundError' => array_key_exists('refundError', $dataToRefund) ? $dataToRefund['refundError'] : null,
                'currency' => array_key_exists('currency', $dataToRefund) ? $dataToRefund['currency'] : null,
            )
        );

        return $result;
    }

    /**
     * Get status message.
     *
     * @param string $status
     *
     * @return string
     */
    private function getStatusMessage($status)
    {
        switch ($status) {
            case 0:
                $statusMessage = $this->l('Refund error');
                break;
            case 1:
                $statusMessage = $this->l('Refund done');
                break;
            case 3:
                $statusMessage = $this->l('Awaiting for refund');
                break;
            case 4:
                $statusMessage = $this->l('Refund rejected');
                break;
            default:
                $statusMessage = $this->l('Unknown status of refund');
        }

        return $statusMessage;
    }

    /**
     * Refund process.
     *
     * @param string $sessionId
     * @param int $p24OrderId
     * @param int $amountToRefund
     *
     * @return stdClass|string
     */
    private function refundProcess($sessionId, $p24OrderId, $amountToRefund)
    {
        $refunds = array(
            0 => array(
                'sessionId' => $sessionId,
                'orderId' => $p24OrderId,
                'amount' => $amountToRefund,
            ),
        );

        $result = $this->getSoapConnection()->refundTransaction($refunds);

        return $result;
    }

    /**
     * Validate order.
     *
     * @param int $idCart
     * @param int $idOrderState
     * @param float $amountPaid
     * @param string $paymentMethod
     * @param string $message
     * @param array $extraVars
     * @param string $currencySpecial
     * @param bool $dontTouchAmount
     * @param bool $secureKey
     * @param Shop|null $shop
     *
     * @return bool
     *
     * @throws PrestaShopException
     */
    public function validateOrder(
        $idCart,
        $idOrderState,
        $amountPaid,
        $paymentMethod = 'Unknown',
        $message = null,
        $extraVars = array(),
        $currencySpecial = null,
        $dontTouchAmount = false,
        $secureKey = false,
        Shop $shop = null
    ) {
        $this->context->cart = new Cart((int)$idCart);

        return parent::validateOrder(
            $idCart,
            $idOrderState,
            $amountPaid,
            $paymentMethod,
            $message,
            $extraVars,
            $currencySpecial,
            $dontTouchAmount,
            $secureKey,
            $shop
        );
    }

    /**
     * Get SOAP connection object
     *
     * @param string $currencySuffix
     * @return Przelewy24Soap
     */
    private function getSoapConnection($currencySuffix = '')
    {
        return new Przelewy24Soap($currencySuffix);
    }
}
