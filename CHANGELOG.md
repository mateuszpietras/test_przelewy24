# Change Log

## [3.4.28] - 2020-02-13 ##
- Escaping translations and numeric values (extracharge context) in templates
- Proper (accepted by PS validator) way of setting version

## [3.4.27] - 2020-02-13 ##
- Making multiple warehouses work properly on PS1.5

## [3.4.26] - 2020-01-24 ##
- Refactor code to allow orders from multiple warehouses

## [3.4.25] - 2019-12-31 ##
- Fix extra discount

## [3.4.24] - 2019-12-19 ##
- better conditions for displaying refunds communicates
- fix point and comma problem for extra discount
- accept P24 rules in shop for guest users

## [3.4.23] - 2019-12-16 ##
- Add ajax form for payment method 218.
- Fix display of extra charge
- Other fixes

## [3.4.22] - 2019-10-22 ##
- extra charge fixes. 

## [3.4.21] - 2019-07-16 ##
- VAT for extra charge. 

## [3.4.20] - 2019-07-16 ##
- Move communication with SOAP to a dedicated class. 
- Remove of unnecessary notice messages in debug mode.

## [3.4.19] - 2019-06-12 ##
- Usage of models instead of RAW SQLs in application.

## [3.4.18] - 2019-05-13 ##
- Adding usage of PrestaShop form builder.
- Usage of PrestaShopLogger instead of deprecated Logger.
- Loading images from external sources.

## [3.4.17] - 2019-03-18 ##
- Extra charge will not be added for non-p24 payment methods.
- Making extension compatible with rockpos extension.
- Usage of triple equals instead of double equals.
- Loading images from external sources. Using remote przelewy24 css file to display images of promoted payment methods.
- Handling catched and ignored excepction.
- Ajax methods moved to separate methods. Usage of build-in PrestaShop ajaxDie method instead of custom exits.

## [3.4.16] - 2019-02-06 ##
- Translating refund statuses.

## [3.4.15] - 2019-02-05 ##
- Removing ZenCard and Google Analytics.

## [3.4.14] - 2018-10-22 ##
- HotFix after 3.4.13 (installment button was missing).

## [3.4.13] - 2018-10-22 ##
- Separating html, js and css structure from php files
  (for now only in configuration page, payment confirmation page and invoice).
- Few methods were refactored / rewritten to look better.
- Noticed malfunctions were fixed (not consequent form validation, wrong usage of jQuery, insufficient   
  loops etc).
- Making code compatible with PSR2 coding standards.
- Making code compatible with Symfony coding standards.
- Upgrading code to be accepted by PrestaShop Validator, which is:
   - fulfilling PS requirements (fixing configuration files),
   - fixing code structure,
   - making code compatible with earlier version of PHP,
   - fixing code errors (mainly parts of code capable of throwing notice),
   - optimising code,
   - adding missing translations,
   - adding license information to files,
   - fixing security issues (unescaped variable usage in template files)
- Adding missing translations (there were few strings in polish in code, out of translation system).
- Translations are no longer removed on AdminPanel translations update.
- mBank icon is displayed properly now for "mBank Raty".
- Rebuilding reset flow - now it removes only Przelewy24 configuration, without reinstalling whole plugin.
- Unification of variables naming convention - now we are using camelCase in php file and camel_case in template files.
  It is not always possible, so there will be few exception (inherited variables, model parameters).
- Updating phpDocs (adding missing phpDocs, editing / extending existing ones).

## [3.4.12] - 2018-08-27 ##
- Fixed link to "myStoredCards".
- Fixed terms checkbox.
- Fixed discount.

## [3.4.11] - 2018-08-06 ##
- Checking if the refund webservice method are available.

## [3.4.10] - 2018-07-13 ##
- Fix different amounts after editing order before paying. Take amount to pay from order if it exists, from cart if not.

## [3.4.9] - 2018-07-05 ##
- Fixed payment returns section does not show up in orders 

## [3.4.8] - 2018-06-27 ##
- Fixed compatible for OnePageCheckout
- Correcting the calculation of the amount sent to Przelewy24
- Change label name
- Fixed double pay to one cart

## [3.4.7] - 2018-06-01 ##
- Change return message

## [3.4.6] - 2017-02-20 ##
- OneClick fix.
- Promoted Metod fix.

## [3.4.5] - 2017-01-08 ##
- Change of configuration panel. 

## [3.4.4] - 2017-12-19 ##
- Technical fix.
- Change of status message.

## [3.4.3] - 2017-12-11 ##
- Fixed creating tables during installation.
- Security fix.

## [3.4.2] - 2017-11-29 ##
- Fixed creating tables during installation.

## [3.4.1] - 2017-11-17 ##
- Added accepted_terms column.
- Fixed jQuery undefined error.

## [3.4.0] - 2017-11-09 ##
- Security fix.
- Disabled zencard.
- Clean up.

## [3.3.22] - 2017-07-06 ##
- Added currency restriction

## [3.3.21] - 2017-07-06 ##
- Added missing polish translations (extra charge, extra discount)

## [3.3.20] - 2017-06-29 ##
- Fixed zencard requests.

## [3.3.19] - 2017-06-28 ##
- Installments description.

## [3.3.18b] - 2017-03-28 ##
- Added payment card registration from panel.

## [3.3.18a] - 2017-03-28 ##
- Fixed id_currency call.

## [3.3.17] - 2017-03-27 ##
- Added security token to paymentConfirmation.

## [3.3.16] - 2017-01-12 ##
- Fixed translations (Bug #2049)

## [3.3.15] - 2017-01-09 ##
- Fixed value in write context issue (Bug #2003)

## [3.3.14] - 2017-01-03 ##
- Added display of selected payment method in confirmation (Bug #1981)

## [3.3.13] - 2016-12-15 ##
- Updated Hungary translations.

## [3.3.12] - 2016-12-13 ##
- Fixed discount with different currencies. 

## [3.3.11] - 2016-12-07 ##

- Added Hungary language.
- Added missing translations.

## [3.3.10] - 2016-12-07 ##

- ZenCard - cash on delivery.

## [3.3.9] - 2016-12-06 ##

- Fixed price zc on delivery payment.
- Fixed delete card link.

## [3.3.8] - 2016-11-25 ##

- Fixed extra-charge when update order status

## [3.3.7a] - 2016-11-10 ##

- ZenCard - fixed total price including delivery costs

## [3.3.7] - 2016-11-10 ##

- ZenCard - has coupon field.
- Fixed ZenCard amount on checkout.

## [3.3.6b] - 2016-11-09 ##

- Added `p24_shipping` to ajax payment.

## [3.3.6a] - 2016-11-08 ##

- Prepare products cart for p24 in ajax payment.

## [3.3.6] - 2016-11-04 ##

- Fixed double discount ZC in front.
- Fixed discount price on invoice.

## [3.3.5] - 2016-10-27 ##

- ZenCard - use function withZencard().

## [3.3.4] - 2016-10-21 ##

- Create free order when cart amount is 0.
- Fixed tax in extra discount.
- Moved ZenCard sdk to shared-libraries.

## [3.3.3b] - 2016-10-19 ##

- Removed all functions in empty().

## [3.3.3a] - 2016-10-17 ##

- Removed function in empty(). 

## [3.3.3] - 2016-10-13 ##

- Added class `Przelewy24Product`.
- Prepare products cart for p24. 

## [3.3.2l] - 2016-10-10 ##

- Confirmation - updated descriptions/translations.

## [3.3.2k] - 2016-10-05 

- Changed functionality of ZenCard coupons to a new version.
- Added _zencard field and withZencard method to the Transaction Class.

## [3.3.2j] - 2016-10-03

- Fixed base_url on myStoredCards page.

## [3.3.2i] - 2016-10-03

- Added ZenCard transaction logger.

## [3.3.2h] - 2016-09-29 

- Fixed cart display on confirm page.

## [3.3.2g] - 2016-09-28 

- Added a ZenCard amount to PDFInvoice.
- When ZenCard's discount amount equals to 0 a discount is not being created for an order.

## [] - 2016-09-27

- Updated PL translations. Added missing descriptions. 

## [3.3.2f] - 2016-09-26

- Add zencard info to orderCartRule.

## [3.3.2e] - 2016-09-23

- Added info about extracharge_amount in paymentConfirmation.tpl. 

## [3.3.2d] - 2016-09-23

- Fixed redirecting to success status page after card payment.

## [3.3.2c] - 2016-09-21 

- Fixed creating my stored cards page url.

## [3.3.2b] - 2016-09-20

- Fixed Zencard_Util number format amount.

## [3.3.2a] - 2016-09-20 

- Fixed refreshing a ZenCard coupon after updating a cart.

## [3.3.2] - 2016-09-15

- Fixed verify ZenCard process.

## [3.3.1r] - 2016-09-08

- Fixed minimum amount for discount.

## [3.3.1p] - 2016-09-08

- Fixed cart reference in hookActionCartSave.
- Delete cart rules after change discount in cart.

## [3.3.1l] - 2016-09-07 

- Added `p24_shipping` field to Przelewy24 form.

## [3.3.1j] - 2016-09-06

- Changed way of adding extracharge to order.
- New table `przelewy24_extracharges` is created during installation.
- New model `Extracharge` added.
- Client doesn't need to choose a virtual product in the module to add a extracharge
to order any more. In the `actionValidateOrder` hook the extracharge is added to a order
prices.
- Removing extracharge from order if the payment method has beed changed to other in the
admin panel.
- Adding extracharge to order if the payment method has beed changed to Przelewy24 in the
admin panel.
- Extracharges are stored in the database in the `przelewy24_extracharges` table. It ensures
a proper removing extracharge from order even after changing this value by client in the
module.
- Added info about extracharge to the order's details view in the admin panel.
- Added info about extracharge to the order's pdf.








