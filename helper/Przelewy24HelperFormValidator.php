<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24HelperFormValidator responsible for specific validation.
 * It adds flash messages available via method getFlash.
 */
class Przelewy24HelperFormValidator extends Helper
{

    /**
     * @var bool
     */
    public $isCreditCardRecurrenceCorrect;

    /**
     * @var string
     */
    private $currentSuffix;

    /**
     * @var string
     */
    private $currentCurrencyCode;

    /**
     * Array of flash messages.
     *
     * @var array
     */
    private $flash = array();

    /**
     * Is api key correct for currently active currency (current currency code).
     *
     * @var bool
     */
    private $isApiKeyCorrect = false;

    /**
     * @var bool
     */
    private $isSubmitted = false;

    /**
     * FormHelperValidator constructor.
     *
     * @param Przelewy24 $module
     * @param string $currentSuffix
     * @param string $currentCurrencyCode
     * @param bool $isSubmitted
     */
    public function __construct($module, $currentSuffix, $currentCurrencyCode, $isSubmitted)
    {
        $this->module = $module;
        $this->currentSuffix = $currentSuffix;
        $this->currentCurrencyCode = $currentCurrencyCode;
        $this->isSubmitted = $isSubmitted;
        // Required for translations
        Module::$classInModule[get_class($this)] = $this->module->name;
    }

    /**
     * Gets array of flash messages.
     *
     * @return array
     */
    public function getFlash()
    {
        return $this->flash;
    }

    /**
     * Return validity of api key.
     *
     * @return bool
     */
    public function isApiKeyCorrect()
    {
        return $this->isApiKeyCorrect;
    }

    /**
     * Validates current currency data and sets validation results in parameters and flash messages.
     * Validate messages should be visible only if form is submitted.
     */
    public function validateCurrency()
    {
        if ($this->isCrcKeyValid() && $this->isSubmitted) {
            $this->addFlash(
                'danger',
                $this->currentSuffix,
                $this->l('Wrong CRC Key for this Merchant/Shop ID and module mode!')
            );
        }
        if ($this->isApiKeyValid() && $this->isSubmitted) {
            $this->addFlash(
                'danger',
                $this->currentSuffix,
                $this->l('Wrong API Key for this Merchant/Shop ID and module mode!')
            );
        }
        $this->isCreditCardRecurrenceCorrect = $this->module->ccCheckRecurrency($this->currentSuffix);
    }

    /**
     * Validates global data and sets validation results in parameters and flash messages.
     */
    public function handleRefundFunctionFlash()
    {
        if (count($this->flash) === 0 && $this->isSubmitted) {
            $this->addFlash(
                'success',
                '',
                $this->l('Updated')
            );
        }
        if (!$this->module->checkRefundFunction()) {
            $this->addFlash(
                'info',
                '',
                $this->l('The function of generating returns from the store requires')
            );
        }
    }

    /**
     * Validates Crc key by currency.
     *
     * @return bool
     */
    private function isCrcKeyValid()
    {
        return !$this->module->validateCredentials(
            Configuration::get(P24Configuration::P24_MERCHANT_ID.$this->currentSuffix),
            Configuration::get(P24Configuration::P24_SHOP_ID.$this->currentSuffix),
            Configuration::get(P24Configuration::P24_SALT.$this->currentSuffix),
            Configuration::get(P24Configuration::P24_TEST_MODE.$this->currentSuffix)
        ) && trim(Tools::getValue('currency')) === $this->currentCurrencyCode;
    }

    /**
     * Checks validity of api key setting it.
     *
     * @return bool
     */
    private function isApiKeyValid()
    {
        return !($this->isApiKeyCorrect = $this->module->apiTestAccess($this->currentSuffix))
            && Configuration::get(P24Configuration::P24_API_KEY.$this->currentSuffix);
    }

    /**
     * Adds flash message.
     *
     * @param string $type
     * @param string $currency
     * @param string $text
     */
    private function addFlash($type, $currency, $text)
    {
        $this->flash[] = array(
            'type' => $type,
            'currency' => $currency,
            'text' => $text,
        );
    }
}
