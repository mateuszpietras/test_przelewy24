<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

require_once dirname(__FILE__) . '/Przelewy24HelperFormValidator.php';

/**
 * Class Przelewy24HelperForm.
 */
class Przelewy24HelperForm extends HelperFormCore
{
    const ALIOR_BANK_CURRENCY = 'PLN';

    const MAX_NUMBER_OF_PAYMENT_METHODS = 5;
    const BOOLEAN_TYPE = 'boolean';
    const DEFAULT_TYPE = 'default';
    const INTEGER_TYPE = 'integer';
    const FLOAT_TYPE = 'float';
    const MULTICURRENCY_TYPE = 'multicurrency';
    const STRING_TYPE = 'string';

    /**
     * Inherited from PrestaShop class HelperCore.
     *
     * @var Przelewy24
     */
    public $module;

    /**
     * Default payment image.
     *
     * @var string
     */
    private $paymentImage = '../img/admin/payment.gif';

    /**
     * Current currency code.
     *
     * @var string
     */
    private $currentCurrencyCode = 'PLN';

    /**
     * Current suffix (will be added to form field names).
     *
     * @var string
     */
    private $currentSuffix = '';

    /**
     * Is credit card recurrence correct for currently active currency (current currency code).
     *
     * @var bool
     */
    private $isCreditCardRecurrenceCorrect = false;

    /**
     * Helper class for specyfic validation.
     *
     * @var Przelewy24HelperFormValidator
     */
    private $validatorHelper = null;

    /**
     * Parameters supported by this extension.
     *
     * @var array
     */
    private $parameters = array(
        P24Configuration::P24_MERCHANT_ID => self::INTEGER_TYPE,
        P24Configuration::P24_SHOP_ID => self::INTEGER_TYPE,
        P24Configuration::P24_SALT => self::STRING_TYPE,
        P24Configuration::P24_TEST_MODE => self::DEFAULT_TYPE,
        P24Configuration::P24_ONECLICK_ENABLED => self::BOOLEAN_TYPE,
        P24Configuration::P24_DEFAULT_REMEMBER_CARD => self::BOOLEAN_TYPE,
        P24Configuration::P24_API_KEY => self::DEFAULT_TYPE,
        P24Configuration::P24_BASIC_AUTH => self::DEFAULT_TYPE,
        P24Configuration::P24_PAYINSHOP_ENABLED => self::BOOLEAN_TYPE,
        P24Configuration::P24_ACCEPTINSHOP_ENABLED => self::BOOLEAN_TYPE,
        P24Configuration::P24_PAYSLOW_ENABLED => self::BOOLEAN_TYPE,
        P24Configuration::P24_CUSTOM_TIMELIMIT => self::INTEGER_TYPE,
        P24Configuration::P24_IVR_ENABLED => self::BOOLEAN_TYPE,
        P24Configuration::P24_CUSTOM_DESCRIPTION => self::DEFAULT_TYPE,
        P24Configuration::P24_PAYMETHOD_LIST => self::BOOLEAN_TYPE,
        P24Configuration::P24_PAYMETHOD_GRAPHICS => self::BOOLEAN_TYPE,
        P24Configuration::P24_PAYMETHOD_FIRST => self::DEFAULT_TYPE,
        P24Configuration::P24_PAYMETHOD_SECOND => self::DEFAULT_TYPE,
        P24Configuration::P24_PAYMETHOD_PROMOTE => self::BOOLEAN_TYPE,
        P24Configuration::P24_DISPLAY_GATEWAY => self::INTEGER_TYPE,
        P24Configuration::P24_PAYMETHOD_PROMOTED => self::DEFAULT_TYPE,
        P24Configuration::P24_PAYMENTS_PROMOTE_LIST => self::DEFAULT_TYPE,
        P24Configuration::P24_GATE_LOGO => self::DEFAULT_TYPE,
        P24Configuration::P24_CHEVRON_RIGHT => self::BOOLEAN_TYPE,
        P24Configuration::P24_GATE_CLASS => self::DEFAULT_TYPE,
        P24Configuration::P24_EXTRADISCOUNT_ENABLED => self::INTEGER_TYPE,
        P24Configuration::P24_EXTRADISCOUNT_AMOUNT => self::FLOAT_TYPE,
        P24Configuration::P24_EXTRADISCOUNT_PERCENT => self::FLOAT_TYPE,
        P24Configuration::P24_INSTALLMENT_SHOW => self::DEFAULT_TYPE,
        P24Configuration::P24_ORDER_TITLE_ID => self::DEFAULT_TYPE,
        P24Configuration::P24_SHOW_SUMMARY => self::BOOLEAN_TYPE,
        P24Configuration::P24_DEBUG => self::BOOLEAN_TYPE,
        P24Configuration::P24_WAIT_FOR_RESULT => self::BOOLEAN_TYPE,
        P24Configuration::P24_VERIFYORDER => self::DEFAULT_TYPE,
        P24Configuration::P24_TEST_MODE_TRANSACTION => self::MULTICURRENCY_TYPE,
        P24Configuration::P24_ORDER_STATE_1 => self::MULTICURRENCY_TYPE,
        P24Configuration::P24_ORDER_STATE_2 => self::MULTICURRENCY_TYPE,
        P24Configuration::P24_EXTRACHARGE_PERCENT => self::FLOAT_TYPE,
        P24Configuration::P24_EXTRACHARGE_AMOUNT => self::FLOAT_TYPE,
        P24Configuration::P24_EXTRACHARGE_ENABLED => self::BOOLEAN_TYPE,
    );

    /**
     * Was form submitted.
     * @var bool
     */
    private $isSubmitted;

    /**
     * List of available payment methods.
     * @var array
     */
    private $paymentMethodsList;

    /**
     * Przelewy24HelperForm constructor.
     *
     * @param Przelewy24 $module
     */
    public function __construct(Przelewy24 $module)
    {
        // Get default language
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Inherited parameters
        $this->module = $module;
        $this->name_controller = $this->module->name;
        $this->token = Tools::getAdminTokenLite('AdminModules');
        $this->currentIndex = AdminController::$currentIndex . '&configure=' . $this->module->name;

        // Required for translations
        Module::$classInModule[get_class($this)] = $this->module->name;

        // Language
        $this->default_form_language = $defaultLang;
        $this->allow_employee_form_lang = $defaultLang;

        // Title and toolbar
        $this->title = $this->module->displayName;
        $this->show_toolbar = true;        // false -> remove toolbar
        $this->toolbar_scroll = true;      // true - > Toolbar is always visible on the top of the screen.
        $this->submit_action = 'submit' . $this->module->name;
        $this->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex .
                        '&configure=' . $this->module->name .
                        '&save' . $this->module->name .
                        '&token=' . Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list'),
            ),
        );

        parent::__construct();
    }

    /**
     * Returns html string with generated form.
     *
     * @return string
     */
    public function generate()
    {
        $this->fields_form = array(
            array(
                'form' => $this->getCurrencyForm(),
            ),
        );
        $defaultCurrency = new CurrencyCore(Configuration::get('PS_CURRENCY_DEFAULT'));
        $this->fields_value['currency'] = Tools::getValue('currency') ?: $defaultCurrency->iso_code;

        foreach (CurrencyCore::getCurrencies() as $currency) {
            $this->setCurrencyContext($currency['iso_code']);
            $this->initFormValidator($this->isSubmitted());
            $this->validatorHelper->validateCurrency();
            $minIndex = count($this->fields_form);
            $this->addFieldSets(
                array(
                    $this->getPrzelewy24SettingsForm(),
                    $this->getKeysForm(),
                    $this->getOneClickPaymentForm(),
                    $this->getPaymentSettingsForm(),
                    $this->getPaymentMethodsForm(),
                    $this->getPaymentGatewaysForm(),
                    $this->getExtraChargeForm(),
                    $this->getExtraDiscountForm(),
                    $this->getSaveButton($minIndex),
                )
            );
            foreach ($this->parameters as $parameter => $type) {
                $parameterName = $type === self::MULTICURRENCY_TYPE ? $parameter : $parameter . $this->currentSuffix;
                $this->fields_value[$parameter . $this->currentSuffix] = Configuration::get($parameterName);
                if (P24Configuration::P24_CUSTOM_TIMELIMIT === $parameter &&
                    !$this->fields_value[$parameter . $this->currentSuffix]) {
                    $this->fields_value[$parameter . $this->currentSuffix] = 15;
                }
            }
            if ($currency['iso_code'] === $this->fields_value['currency']) {
                $this->validatorHelper->handleRefundFunctionFlash();

                $this->context->smarty->assign(
                    array(
                        'flashMessages' => $this->validatorHelper->getFlash(),
                    )
                );
            }
        }

        $this->loadScripts();

        return $this->module->displayTemplate('notifications') . parent::generate();
    }

    /**
     * Marks form as submitted.
     */
    public function markAsSubmitted()
    {
        $this->isSubmitted = true;
    }

    /**
     * Check if is submited.
     *
     * @return bool
     */
    public function isSubmitted()
    {
        return $this->isSubmitted;
    }

    /**
     * Provide public access for parameters. It is for pre php 7 compatibility.
     *
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Loads JS and CSS used in this form.
     */
    private function loadScripts()
    {
        $this->context->controller->addCSS(
            $this->module->getPathUri() . 'views/css/formStyle.css',
            'all'
        );
        if (_PS_VERSION_ >= 1.6) {
            $this->context->controller->addCSS(
                $this->module->p24_css,
                'all'
            );
        } else {
            $this->context->controller->css_files[$this->module->p24_css] = 'all';
        }
        $this->context->controller->addJS(
            $this->module->getPathUri() . 'views/js/formScripts.js',
            'all'
        );
    }

    /**
     * Adds field set to current form.
     * Fields_form property is inherited from Prestashop HelperForm class.
     *
     * @param array $fieldSets
     * @return Przelewy24HelperForm
     */
    private function addFieldSets($fieldSets)
    {
        foreach ($fieldSets as $fieldSet) {
            if (null === $fieldSet) {
                continue;
            }
            $this->fields_form[] = array('form' => $fieldSet);
        }

        return $this;
    }

    /**
     * Get currency form.
     *
     * @return array
     */
    private function getCurrencyForm()
    {
        $values = array();
        foreach (CurrencyCore::getCurrencies() as $currency) {
            $values[] = array(
                'id' => $currency['iso_code'],
                'value' => $currency['iso_code'],
                'label' => $currency['iso_code'],
                'p' => _PS_VERSION_ < 1.6 ? ' ' : null,
            );
        }
        $this->context->smarty->assign(
            array(
                'moduleLink' => $this->context->link->getModuleLink(
                    'przelewy24',
                    'paymentAjax',
                    array(),
                    '1' === Configuration::get('PS_SSL_ENABLED')
                ),
            )
        );

        return array(
            'legend' => array(
                'title' => $this->l('Select currency for which you want to configure your merchant'),
                'image' => $this->paymentImage,
            ),
            'input' => array(
                array(
                    'type' => 'radio',
                    'label' => '',
                    'desc' => $this->l('For each currency in your store you need to configure module Przelewy24.'),
                    'name' => 'currency',
                    'required' => true,
                    'values' => $values,
                ),
                array(
                    'type' => 'html',
                    'name' => $this->module->displayTemplate('form_config'),
                ),
            ),
        );
    }

    /**
     * Get boolean field.
     * Switch field was added in PrestaShop 1.6 . In order to make this script work with PrestaShop 1.5
     * different type of field (select) has to be used for boolean fields.
     *
     * @param string $label
     * @param string $name
     * @param string|null $description
     * @param bool $condition
     *
     * @return array
     */
    private function getBooleanField($label, $name, $description = null, $condition = true)
    {
        return _PS_VERSION_ >= 1.6 ? array(
            'condition' => $condition,
            'type' => 'switch',
            'label' => $label,
            'name' => $name . $this->currentSuffix,
            'desc' => $description,
            'values' => array(
                array(
                    'id' => 'active_on',
                    'value' => 1,
                ),
                array(
                    'id' => 'active_off',
                    'value' => 0,
                ),
            ),
        ) : array(
            'condition' => $condition,
            'type' => 'radio',
            'label' => $label,
            'class' => 'p24-label-short',
            'name' => $name . $this->currentSuffix,
            'desc' => $description,
            'br' => true,
            'is_bool' => true,
            'values' => array(
                array(
                    'id' => $name . $this->currentSuffix . 'active_on',
                    'value' => 1,
                    'label' => $this->l('Yes'),
                    'p' => ' ',
                ),
                array(
                    'id' => $name . $this->currentSuffix . 'active_off',
                    'value' => 0,
                    'label' => $this->l('No'),
                ),
            ),
        );
    }

    /**
     * Get przelewy24 main form (with connection configuration).
     *
     * @return array
     */
    private function getPrzelewy24SettingsForm()
    {
        $orderStatesList = OrderState::getOrderStates($this->context->cookie->id_lang);

        return array(
            'legend' => array(
                'title' => $this->l('Payment Settings'),
                'image' => $this->paymentImage,
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Merchant ID'),
                    'name' => P24Configuration::P24_MERCHANT_ID . $this->currentSuffix,
                    'desc' => $this->l('Where is ID'),
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Shop ID'),
                    'name' => P24Configuration::P24_SHOP_ID . $this->currentSuffix,
                    'desc' => $this->l('Where is ID'),
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('CRC Key'),
                    'name' => P24Configuration::P24_SALT . $this->currentSuffix,
                    'desc' => $this->l('Where is CRC'),
                    'required' => true,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Order id in title'),
                    'name' => P24Configuration::P24_ORDER_TITLE_ID . $this->currentSuffix,
                    'options' => array(
                        'query' => array(
                            array(
                                'id_option' => 0,
                                'name' => $this->l('Order id in database (e.g. 1, 2, 3)'),
                            ),
                            array(
                                'id_option' => 1,
                                'name' => $this->l('Masked order id (e.g. QYTUVLHOW)'),
                            ),
                        ),
                        'id' => 'id_option',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status before completing payment'),
                    'name' => P24Configuration::P24_ORDER_STATE_1 . $this->currentSuffix,
                    'options' => array(
                        'query' => $orderStatesList,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status after completing payment'),
                    'name' => P24Configuration::P24_ORDER_STATE_2 . $this->currentSuffix,
                    'options' => array(
                        'query' => $orderStatesList,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Module mode'),
                    'name' => P24Configuration::P24_TEST_MODE . $this->currentSuffix,
                    'options' =>
                        array(
                            'query' => array(
                                array(
                                    'id' => 1,
                                    'name' => $this->l('test'),
                                ),
                                array(
                                    'id' => 0,
                                    'name' => $this->l('normal'),
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name',
                        ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Order validation (change cart to order):'),
                    'name' => P24Configuration::P24_VERIFYORDER . $this->currentSuffix,
                    'options' => array(
                        'query' => array(
                            array(
                                'id' => 1,
                                'name' => $this->l('order confirmation'),
                            ),
                            array(
                                'id' => 0,
                                'name' => $this->l('transaction verification'),
                            ),
                            array(
                                'id' => 2,
                                'name' => $this->l('after click Confirm button'),
                            ),
                        ),
                        'id' => 'id',
                        'name' => 'name',
                    ),
                ),
                $this->getBooleanField(
                    $this->l('Wait for the result of the transaction'),
                    P24Configuration::P24_WAIT_FOR_RESULT,
                    $this->l('setting shared between currencies')
                ),
                $this->getBooleanField(
                    $this->l('Debug mode'),
                    P24Configuration::P24_DEBUG,
                    $this->l('setting shared between currencies')
                ),
            ),
        );
    }

    /**
     * Get keys form.
     *
     * @return array
     */
    private function getKeysForm()
    {
        return array(
            'legend' => array(
                'title' => $this->l('Keys'),
                'image' => $this->paymentImage,
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('API Key'),
                    'name' => P24Configuration::P24_API_KEY . $this->currentSuffix,
                    'required' => false,
                    'class' => Configuration::get(P24Configuration::P24_API_KEY . $this->currentSuffix) ?
                        ($this->validatorHelper->isApiKeyCorrect() ? 'badge-ok' : 'badge-no') : null,
                    'desc' => $this->l('Where is API'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Basic auth'),
                    'name' => P24Configuration::P24_BASIC_AUTH . $this->currentSuffix,
                    'required' => false,
                    'placeholder' => 'login:password',
                    'desc' => $this->l('Fill in this field if Your shop is protected with basic-auth mechanism'),
                ),
            ),
        );
    }

    /**
     * Get one click payment form.
     *
     * @return array|null
     */
    private function getOneClickPaymentForm()
    {
        $oneClickFields = array();

        if ($this->validatorHelper->isApiKeyCorrect()) {
            array_push(
                $oneClickFields,
                $this->getBooleanField(
                    $this->l('Oneclick payments'),
                    P24Configuration::P24_ONECLICK_ENABLED,
                    $this->l('Allows you to order products with on-click')
                ),
                $this->getBooleanField(
                    $this->l('Default memorize the card?'),
                    P24Configuration::P24_DEFAULT_REMEMBER_CARD,
                    $this->l(
                        'Default option "Memorize payment cards, which I pay" on the confirmation page is selected?'
                    ),
                    (bool)Configuration::get(P24Configuration::P24_ONECLICK_ENABLED . $this->currentSuffix)
                )
            );

            if (!$this->paymentMethodsList) {
                $this->paymentMethodsList = $this->module->availablePaymentMethods(false, $this->currentCurrencyCode);
            }

            $thereAreCardPayments = false;
            foreach ($this->paymentMethodsList as $item) {
                if (in_array((int)$item->id, array(142, 145, 218))) {
                    $thereAreCardPayments = true;
                    break;
                }
            }

            if ($thereAreCardPayments) {
                array_push(
                    $oneClickFields,
                    $this->getBooleanField(
                        $this->l('Pay in shop'),
                        P24Configuration::P24_PAYINSHOP_ENABLED,
                        $this->l('Pay in shop description')
                    )
                );
            }
        }

        return count($oneClickFields) > 0 ? array(
                'legend' => array(
                    'title' => $this->l('Oneclick payments'),
                    'image' => $this->paymentImage,
                ),
                'input' => $oneClickFields,
            ) : null;
    }

    /**
     * Get payment settings form.
     *
     * @return array
     */
    private function getPaymentSettingsForm()
    {
        return array(
            'legend' => array(
                'title' => $this->l('Payment Settings'),
                'image' => $this->paymentImage,
            ),
            'input' => array(
                $this->getBooleanField(
                    $this->l('Show accept button in shop'),
                    P24Configuration::P24_ACCEPTINSHOP_ENABLED,
                    $this->l('Show accept button in shop description')
                ),
                array(
                    'condition' => self::ALIOR_BANK_CURRENCY === $this->currentCurrencyCode,
                    'type' => 'select',
                    'label' => $this->l('Installment Settings'),
                    'name' => P24Configuration::P24_INSTALLMENT_SHOW . $this->currentSuffix,
                    'description' => 'Alior Bank only',
                    'options' => array(
                        'query' => array(
                            array(
                                'id_option' => 2,
                                'name' => $this->l('button (payment page) and information (product page)'),
                            ),
                            array(
                                'id_option' => 1,
                                'name' => $this->l('button (payment page) only'),
                            ),
                            array(
                                'id_option' => 0,
                                'name' => $this->l('hide all'),
                            ),
                        ),
                        'id' => 'id_option',
                        'name' => 'name',
                    ),
                ),
                $this->getBooleanField(
                    $this->l('Allow also for not immediate payments'),
                    P24Configuration::P24_PAYSLOW_ENABLED,
                    $this->l('This setting increases the number of available payment methods')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Adjust the time limit for completion of the transaction'),
                    'name' => 'P24_CUSTOM_TIMELIMIT' . $this->currentSuffix,
                    'desc' => $this->l('Value 0 means no time limit'),
                ),
                $this->getBooleanField(
                    $this->l('Show IVR button at Order Details page'),
                    P24Configuration::P24_IVR_ENABLED,
                    $this->l('This button allows to call back to the Customer'),
                    $this->validatorHelper->isApiKeyCorrect() && $this->module->checkIvrPresent($this->currentSuffix)
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Custom module description'),
                    'name' => P24Configuration::P24_CUSTOM_DESCRIPTION . $this->currentSuffix,
                ),
            ),
        );
    }

    /**
     * Get payment methods form.
     *
     * @return array|null
     */
    private function getPaymentMethodsForm()
    {
        if (!$this->validatorHelper->isApiKeyCorrect()) {
            return null;
        }
        $result = array(
            'legend' => array(
                'title' => $this->l('Payment methods'),
                'image' => $this->paymentImage,
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => P24Configuration::P24_PAYMETHOD_FIRST . $this->currentSuffix,
                ),
                array(
                    'type' => 'hidden',
                    'name' => P24Configuration::P24_PAYMETHOD_SECOND . $this->currentSuffix,
                ),
                $this->getBooleanField(
                    $this->l('Show available payment methods in shop'),
                    P24Configuration::P24_PAYMETHOD_LIST,
                    $this->l('Customer can chose payment method on confirmation page')
                ),
            ),
        );

        if (!Configuration::get(P24Configuration::P24_PAYMETHOD_LIST . $this->currentSuffix)) {
            return $result;
        }
        $result['input'][] = $this->getBooleanField(
            $this->l('Use graphics list of payment methods'),
            P24Configuration::P24_PAYMETHOD_GRAPHICS,
            $this->l('Customer can chose payment method on confirmation page')
        );
        $this->context->smarty->assign(array('currencySuffix' => $this->currentSuffix));
        $result['input'][] = array(
            'type' => 'html',
            'name' => $this->module->displayTemplate('auto_order_button'),
        );
        $additionalSubformData = array(
            'suffix' => $this->currentSuffix,
            'maxNumberOfPaymentMethods' => self::MAX_NUMBER_OF_PAYMENT_METHODS,
        );
        if ($this->validatorHelper->isApiKeyCorrect()) {
            if (!$this->paymentMethodsList) {
                $this->paymentMethodsList = array();
            }
            $paymentListRemote = $this->module->availablePaymentMethods(false, $this->currentCurrencyCode);
            $this->paymentMethodsList = array_merge($this->paymentMethodsList, $paymentListRemote);
            unset($paymentListRemote);

            $paymentList = array();
            $thereIs218 = false;
            foreach ($this->paymentMethodsList as $item) {
                $paymentList[$item->id] = $item;
                if (218 === (int)$item->id) {
                    $thereIs218 = true;
                }
            }
            if ($thereIs218) {
                unset($paymentList[142], $paymentList[145]);
            }

            $p24PayMethodFirst = explode(
                ',',
                Configuration::get(P24Configuration::P24_PAYMETHOD_FIRST . $this->currentSuffix)
            );
            $p24PayMethodSecond = explode(
                ',',
                Configuration::get(P24Configuration::P24_PAYMETHOD_SECOND . $this->currentSuffix)
            );
            $additionalSubformData['paymentMethodsSelected'] = array();
            $additionalSubformData['paymentMethodsAvailable'] = array();
            foreach ($p24PayMethodFirst as $id) {
                if (array_key_exists($id, $paymentList)) {
                    $paymentMethod = $paymentList[$id];
                    $additionalSubformData['paymentMethodsSelected'][] = array(
                        'id' => $paymentMethod->id,
                        'name' => $paymentMethod->name,
                    );
                    unset($paymentList[$id]);
                }
            }
            foreach ($p24PayMethodSecond as $id) {
                if (array_key_exists($id, $paymentList)) {
                    $paymentMethod = $paymentList[$id];
                    $additionalSubformData['paymentMethodsAvailable'][] = array(
                        'id' => $paymentMethod->id,
                        'name' => $paymentMethod->name,
                    );
                    unset($paymentList[$id]);
                }
            }
            foreach ($paymentList as $id => $bank) {
                $additionalSubformData['paymentMethodsAvailable'][] = array(
                    'id' => $bank->id,
                    'name' => $bank->name,
                );
            }
            $paymethodPromoted = Configuration::get(P24Configuration::P24_PAYMETHOD_PROMOTED . $this->currentSuffix);
            $paymethodPromotedAr = explode(',', $paymethodPromoted);
            $additionalSubformData['paymentListToPromote'] = array();
            if ($this->paymentMethodsList) {
                foreach ($this->paymentMethodsList as $item) {
                    // Hardcoded values, moved from Przelewy24 class, perhaps there is a better way to get those
                    // ids, but I do not know of any at the moment.
                    // excluding credit cards, installments and prepay
                    if (!in_array($item->id, array(142, 145, 218, 2000))) {
                        $item->checked = '';
                        if (in_array($item->id, $paymethodPromotedAr)) {
                            $item->checked = ' checked="checked"';
                        }
                        $additionalSubformData['paymentListToPromote'][] = array(
                            'id' => $item->id,
                            'name' => $item->name,
                            'checked' => in_array($item->id, $paymethodPromotedAr),
                        );
                    }
                }
            }
        }

        $this->context->smarty->assign($additionalSubformData);
        $result['input'][] = $this->getHtmlField(
            $this->l('Payment methods'),
            $this->module->displayTemplate('sortable_payments')
        );

        return $result;
    }

    /**
     * Get html field.
     *
     * @param string $label
     * @param string $html
     *
     * @return array
     */
    private function getHtmlField($label, $html)
    {
        if (_PS_VERSION_ >= 1.6) {
            return array(
                'type' => 'html',
                'label' => $label,
                'name' => $html,
            );
        }
        $name = 'custom_field_' . uniqid();
        $this->fields_value[$name] = $html;

        return array('type' => 'free', 'label' => $label, 'name' => $name);
    }

    /**
     * Get payment gateways form.
     *
     * @return array
     */
    private function getPaymentGatewaysForm()
    {
        $this->context->smarty->assign(
            array(
                'isPayMethodPromoteChecked' => Configuration::get(
                    P24Configuration::P24_PAYMETHOD_PROMOTE . $this->currentSuffix
                ),
                'suffix' => $this->currentSuffix,
            )
        );

        return $this->validatorHelper->isApiKeyCorrect() ? array(
            'legend' => array(
                'title' => $this->l('Payment gateways list'),
                'image' => $this->paymentImage,
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => P24Configuration::P24_PAYMENTS_PROMOTE_LIST . $this->currentSuffix,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('How to display logo on a list of payment gateways'),
                    'desc' => $this->l('Can improve display with some particular templates or modules'),
                    'name' => P24Configuration::P24_DISPLAY_GATEWAY . $this->currentSuffix,
                    'options' => array(
                        'query' => array(
                            array(
                                'id_option' => 0,
                                'name' => $this->l('Display as background (default)'),
                            ),
                            array(
                                'id_option' => 1,
                                'name' => $this->l('Display as separate image'),
                            ),
                        ),
                        'id' => 'id_option',
                        'name' => 'name',
                    ),
                ),
                $this->getBooleanField(
                    $this->l('Show right arrow on a list of payment gateways'),
                    P24Configuration::P24_CHEVRON_RIGHT
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Additional CSS Class for payment gateway'),
                    'name' => P24Configuration::P24_GATE_CLASS . $this->currentSuffix,
                ),
                array(
                    'type' => 'hidden',
                    'name' => P24Configuration::P24_PAYMETHOD_PROMOTED . $this->currentSuffix,
                ),
                $this->getBooleanField(
                    $this->l('Promote some payment methods'),
                    P24Configuration::P24_PAYMETHOD_PROMOTE,
                    $this->l('Selected items will be shown next to other payments gateways')
                ),
                $this->getHtmlField('', $this->module->displayTemplate('sortable_promote_payments')),
            ),
        ) : null;
    }

    /**
     * Get extra charge form.
     *
     * @return array
     */
    private function getExtraChargeForm()
    {
        return array(
            'legend' => array(
                'title' => $this->l('Extra charge settings'),
                'image' => $this->paymentImage,
            ),
            'input' => array(
                $this->getBooleanField(
                    $this->l('Enable extra charge'),
                    P24Configuration::P24_EXTRACHARGE_ENABLED
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Increase payment (amount)'),
                    'name' => P24Configuration::P24_EXTRACHARGE_AMOUNT . $this->currentSuffix,
                    'class' => 'input',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Increase payment (percent)'),
                    'name' => P24Configuration::P24_EXTRACHARGE_PERCENT . $this->currentSuffix,
                    'class' => 'input',
                    'desc' => $this->l('Extra charge description'),
                    'required' => false,
                )
            ),
        );
    }

    /**
     * Get extra discount form.
     *
     * @return array
     */
    private function getExtraDiscountForm()
    {
        return array(
            'legend' => array(
                'title' => $this->l('Extra discount settings'),
                'image' => $this->paymentImage,
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Enable extra discount'),
                    'name' => P24Configuration::P24_EXTRADISCOUNT_ENABLED . $this->currentSuffix,
                    'options' => array(
                        'query' => array(
                            array(
                                'id_option' => 0,
                                'name' => $this->l('Discount disabled'),
                            ),
                            array(
                                'id_option' => 1,
                                'name' => $this->l('Discount amount'),
                            ),
                            array(
                                'id_option' => 2,
                                'name' => $this->l('Discount percent'),
                            ),
                        ),
                        'id' => 'id_option',
                        'name' => 'name',
                    ),
                    'desc' => $this->l('It allows for an additional discount if the customer uses Przelewy24'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Discount amount'),
                    'name' => P24Configuration::P24_EXTRADISCOUNT_AMOUNT . $this->currentSuffix,
                    'class' => 'input',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Discount percent'),
                    'name' => P24Configuration::P24_EXTRADISCOUNT_PERCENT . $this->currentSuffix,
                    'class' => 'input',
                    'required' => false,
                ),
            ),
        );
    }

    /**
     * Get save button.
     *
     * @param int $minIndex
     * @return array
     */
    private function getSaveButton($minIndex)
    {
        $this->context->smarty->assign(
            array(
                'minIndex' => $minIndex,
                'currencyCode' => $this->currentCurrencyCode,
                'currencySuffix' => $this->currentSuffix,
            )
        );

        return array(
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
            'input' => array($this->getHtmlField('', $this->module->displayTemplate('currency_config'))),
        );
    }

    /**
     * Set currency context. From now on generated fields will be related to given currency.
     *
     * @param string $isoCode
     */
    private function setCurrencyContext($isoCode)
    {
        $this->currentCurrencyCode = $isoCode;
        $this->currentSuffix = 'PLN' === $this->currentCurrencyCode ? '' : '_' . $this->currentCurrencyCode;
    }

    /**
     * Lazy loading of ValidatorHelper class. Whenever php 7 is lowest supported it would be good to use DI approach
     * to decouple it from form class. Make sure to use it after module, currentSuffix and currentCurrencyCode is set.
     *
     * @param bool $isSubmitted
     *
     * @return Przelewy24HelperFormValidator
     */
    private function initFormValidator($isSubmitted)
    {
        $this->validatorHelper = new Przelewy24HelperFormValidator(
            $this->module,
            $this->currentSuffix,
            $this->currentCurrencyCode,
            $isSubmitted
        );

        return $this->validatorHelper;
    }
}
