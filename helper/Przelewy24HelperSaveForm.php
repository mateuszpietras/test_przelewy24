<?php
/**
 * @author Przelewy24
 * @copyright Przelewy24
 * @license https://www.gnu.org/licenses/lgpl-3.0.en.html
 */

/**
 * Class Przelewy24HelperSaveForm responsible for saving data
 */
class Przelewy24HelperSaveForm
{

    /**
     * @var bool $isSubmitted
     */
    private $isSubmitted;

    /**
     * @var array $parameters
     */
    private $parameters;

    /**
     * FormSaveHelper constructor.
     *
     * @param array $parameters
     * @param $moduleName
     */
    public function __construct(array $parameters, $moduleName)
    {
        $this->parameters = $parameters;
        // Required for translations
        Module::$classInModule[get_class($this)] = $moduleName;
    }

    /**
     * Check if is submitted
     *
     * @return bool
     */
    public function isSubmitted()
    {
        return $this->isSubmitted;
    }

    /**
     * Saves form data.
     */
    public function save()
    {
        $needCartRules = false;

        Configuration::updateValue('P24_COMMISSION', 0);
        $currencySuffix = $this->getCurrencySuffix();
        foreach ($this->parameters as $parameter => $type) {
            $parameterWithSuffix = $parameter.$currencySuffix;

            if (Przelewy24HelperForm::MULTICURRENCY_TYPE === $type) {
                Configuration::updateValue(
                    $parameter, // updating parameter with no currency suffix.
                    Tools::getValue($parameterWithSuffix)
                );
                continue;
            }
            $value = $this->getValue($parameterWithSuffix, $type);

            $fieldsWithFloats = array(
                P24Configuration::P24_EXTRACHARGE_PERCENT,
                P24Configuration::P24_EXTRACHARGE_AMOUNT,
                P24Configuration::P24_EXTRADISCOUNT_PERCENT,
                P24Configuration::P24_EXTRADISCOUNT_AMOUNT,
            );

            if (in_array($parameter, $fieldsWithFloats)) {
                $value = (float)(str_replace(',', '.', $value));
            }

            if (P24Configuration::P24_EXTRADISCOUNT_ENABLED === $parameter) {
                /* The extra discount use a specific feature of PrestaShop we have to activate. */
                $needCartRules = (bool)$value;
            }

            Configuration::updateValue(
                $parameterWithSuffix,
                $value
            );
        }
        // Prevents applying discount and extra charge at the same time.
        if (Configuration::get(P24Configuration::P24_EXTRACHARGE_ENABLED.$currencySuffix)
            && Configuration::get(P24Configuration::P24_EXTRADISCOUNT_ENABLED.$currencySuffix)) {
            Configuration::updateValue(P24Configuration::P24_EXTRADISCOUNT_ENABLED.$currencySuffix, 0);
        }

        if ($needCartRules) {
            /*
             * The PrestaShop automatically activate this feature on first use.
             * There is a problem with cache though. It is stored in static variables inside functions.
             * If we use cart rules for the first time and create a new order in the same request,
             * the order will read unchanged value of cart rules. It is not possible to override.
             * Set it in this place instead.
             */
            Configuration::updateValue('PS_CART_RULE_FEATURE_ACTIVE', '1');
        }
    }

    /**
     * Extracts value from parameter.
     *
     * @param $parameter
     * @param $type
     * @return int|string
     */
    private function getValue($parameter, $type)
    {
        $value = Tools::getValue($parameter);
        if (Przelewy24HelperForm::BOOLEAN_TYPE === $type || Przelewy24HelperForm::INTEGER_TYPE === $type) {
            $value = (int)$value;
        } elseif (Przelewy24HelperForm::STRING_TYPE === $type) {
            $value = trim($value);
        }

        return $value;
    }

    /**
     * Extracts currency suffix.
     *
     * @return string
     */
    private function getCurrencySuffix()
    {
        $currencyCode = trim(Tools::getValue('currency'));
        $currencySuffix = 'PLN' === $currencyCode ? '' : '_'.$currencyCode;

        return $currencySuffix;
    }
}
